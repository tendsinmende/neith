use crate::Interface;
use crate::com::MutVal;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;

use std::time::Instant;
use super::Value;

pub struct Label{
    value: Value<String>,
    text: Box<crate::base::Text>,
}

impl Label{
    pub fn new(text: &str) -> Self{
	Label{
	    value: Value::Own(text.to_string()),
	    text: Box::new(crate::base::Text::new().with_text(text)),
	}
    }

    pub fn with_mut_val(mut self, val: MutVal<String>) -> Self{
	self.value = Value::MutVal(val);
	self.text.set_text(self.get_text());
	self
    }

    pub fn get_text(&self) -> String{
	match &self.value{
	    Value::Own(t) => t.clone(),
	    Value::MutVal(ref val) => val.get().clone()
	}
    }

    pub fn with_text(mut self, text: crate::base::Text) -> Self{
	self.text = Box::new(text);
	self
    }
    
    pub fn set_text(&mut self, text: String){
	match &mut self.value{
	    Value::MutVal(v) => *v.get_mut() = text,
	    Value::Own(v) => *v = text,
	}
    }
    
    pub fn with_size(mut self, size: Unit) -> Self{
	self.text.layout.text_size = size;
	self
    }
}

impl Widget for Label{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, _event: &Event, _location: &Vector2<Unit>){
	//Do nothing since we dont want to react to any input
    }

    ///Returns current occupied size
    fn area(&self) -> Area{
	self.text.area()
    }
    
    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{
	if self.get_text() != self.text.get_text(){
	    self.text.set_text(self.get_text());
	}
	
	self.text.get_calls(host_extent, draw_time, interface, level)
    }
}
