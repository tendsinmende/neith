use neith::primitive::Prim;

use crate::vulkan_prelude::*;
use std::sync::Arc;
use crate::interface_pass::InterfacePass;


use crate::post_progress_pass::PostProgress;


///GPU side definition of a vertex
#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Vertex{
    pub pos: [f32;4],
    pub uv: [f32; 2],
}

impl Vertex{
    ///Returns the vertex state of `Vertex` which can be used to build a graphical pipeline which uses
    ///these vertices as input.
    pub fn get_default_vertex_state() -> VertexInputState {
	//This one descibes how vertices are bound to the shader.
	VertexInputState {
            vertex_binding_descriptions: vec![
		vk::VertexInputBindingDescription::builder()
		    .binding(0)
		    .stride(std::mem::size_of::<Vertex>() as u32)
		    .input_rate(vk::VertexInputRate::VERTEX)
		    .build()
	    ],
            vertex_attribute_descriptions: vec![
		//Description of the Pos attribute
		vk::VertexInputAttributeDescription::builder()
                    .location(0)
                    .binding(0)
                    .format(vk::Format::R32G32B32A32_SFLOAT)
                    .offset(offset_of!(Vertex, pos) as u32)
                    .build(),
		//Description of the uv attribute
		vk::VertexInputAttributeDescription::builder()
                    .location(1)
                    .binding(0)
                    .format(vk::Format::R32G32_SFLOAT)
                    .offset(offset_of!(Vertex, uv) as u32)
                    .build(),
            ],
	}
    }
}

pub(crate) struct CommandBuilder{
    device: Arc<Device>,
    
    command_pool: Arc<CommandBufferPool>,

    pub(crate) interface_pass: InterfacePass,
    pub(crate) post_progress_pass: PostProgress,
}


impl CommandBuilder{
    pub fn new(
	device: Arc<Device>,
	graphics_queue: Arc<Queue>,
	drawing_area: (u32, u32),
	num_frames: usize,
	swapchain_format: vk::Format,
    ) -> Self{

	let (width, height) = drawing_area;
	
	let command_pool = CommandBufferPool::new(
            device.clone(),
            graphics_queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        ).expect("Failed to create command pool!");

	let interface_pass = InterfacePass::new(
	    device.clone(),
	    graphics_queue.clone(),
	    (width, height),
	    num_frames,
	    swapchain_format
	);
	//Create pp pass based on the created images
	let post_progress_pass = PostProgress::new(
	    device.clone(),
	    interface_pass.frame_data[0].color_format,
	    (width, height),
	    num_frames,
	    interface_pass.get_images()
	);
	
	CommandBuilder{
	    interface_pass,
	    post_progress_pass,
	    device,
	    command_pool,
	}
    }

    pub fn resize(&mut self, frame_count: u32, new_extent: (u32, u32)){
	self.interface_pass.resize(new_extent, frame_count);
	self.post_progress_pass = PostProgress::new(
	    self.device.clone(),
	    self.interface_pass.frame_data[0].color_format,
	    new_extent,
	    frame_count as usize,
	    self.interface_pass.get_images()
	);
    }

    ///Builds a new command buffer, that when executed draws a 
    pub fn render(
	&mut self,
	prims: Vec<Prim>,
	index: usize,
	swapchain_image: Arc<dyn AbstractImage + Send + Sync>,
    ) -> Arc<CommandBuffer>{
	let interface_pass_buffer = self.command_pool.alloc(1, false)
	    .expect("Failed to allocate command buffer for interface pass!")
	    .pop()
	    .expect("Could not pop command buffer from vec");

	interface_pass_buffer.begin_recording(
	    true, //One time submit yes
            false, //No continue
            false, //No simultaneouse
            None
	).expect("Failed to start interface pass command buffer!");

	self.interface_pass.draw_interface(
	    interface_pass_buffer.clone(),
	    index,
	    prims,
	);

	//Execute
	self.post_progress_pass.execute(interface_pass_buffer.clone(), self.interface_pass.get_image(index), index);

	//After drawing everything, copy the final image to the swapchain image
	//First transfer it to transfer_dst_optimal, and the src image to src_optimal
	interface_pass_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![
		swapchain_image.new_image_barrier(
                    Some(vk::ImageLayout::PRESENT_SRC_KHR),
                    Some(vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		),
		self.post_progress_pass.get_final_image(index).new_image_barrier(
                    Some(vk::ImageLayout::GENERAL),
                    Some(vk::ImageLayout::TRANSFER_SRC_OPTIMAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		)
	    ]
        ).expect("Failed to add pipeline barrier while copying");

	//Now blit the final image, since the swapchain image might have some strange format or whatever
	let src_size = self.post_progress_pass.get_final_image(index).extent();
	interface_pass_buffer.cmd_blit_image(
	    self.post_progress_pass.get_final_image(index),
            vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
            swapchain_image.clone(), //Since we wait for the image anyways, we can do this
            vk::ImageLayout::TRANSFER_DST_OPTIMAL,
	    vec![
		vk::ImageBlit::builder()
		    .src_subresource(vk::ImageSubresourceLayers{
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    })
		    .src_offsets([
			vk::Offset3D{x:0, y:0, z:0},
			vk::Offset3D{x: src_size.width as i32, y: src_size.height as i32, z: 1}
		    ])
		    .dst_subresource(vk::ImageSubresourceLayers{
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        mip_level: 0,
                        base_array_layer: 0,
                        layer_count: 1,
                    })
		    .dst_offsets([
			vk::Offset3D{x:0, y:0, z:0},
			vk::Offset3D{x: src_size.width as i32, y: src_size.height as i32, z: 1}
		    ]).build()
	    ],
	    vk::Filter::LINEAR
	).expect("Failed to blit to swapchain image!");
	//Now transfer the swapchain image back to present layout
	interface_pass_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::TRANSFER,
            vk::PipelineStageFlags::TOP_OF_PIPE,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![
		swapchain_image.new_image_barrier(
                    Some(vk::ImageLayout::TRANSFER_DST_OPTIMAL),
                    Some(vk::ImageLayout::PRESENT_SRC_KHR),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		)
	    ]
        ).expect("Failed to add pipeline barrier while copying");
	
	interface_pass_buffer.end_recording().expect("Failed to end command buffer!");
	interface_pass_buffer
    }
}
