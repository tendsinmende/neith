use crate::vulkan_prelude::*;
use crate::command_builder::Vertex;
use std::sync::Arc;

///The default vertex buffer push block
#[repr(C)]
#[derive(Clone,Copy,Debug)]
pub struct TextPush {
    pub transform: [[f32; 4]; 4],
    pub color: [f32;4],
    pub power: f32,
    pub level: f32,
    pub pad1: [f32;2],
}


fn default_vertex_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../bin/spirv/text_vert.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read text shader!")
}

fn default_frag_shader() -> Vec<u32>{
    let bytes = include_bytes!("../../bin/spirv/text_frag.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read text shader!")
}

///Draws the given vertex buffer and flood fills it with some color
pub struct TextPipeline{
    pipeline: Arc<dyn Pipeline + Send + Sync>
}

impl TextPipeline{
    pub fn new(device: Arc<Device>, render_pass: Arc<RenderPass>, msaa_flag: vk::SampleCountFlags) -> Self{


	//Create the pipeline that handles the quad drawing.
        //Load Shaders
        let vertex_shader = ShaderModule::new_from_code(
            device.clone(),
            default_vertex_shader()
        ).expect("Failed to load vertex shader: text");
        
        let fragment_shader = ShaderModule::new_from_code(
	    device.clone(),
	    default_frag_shader()
        ).expect("Failed to load fragment shader: text");
        

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        // setup Graphicspipeline
        //This one descibes how vertices are bound to the shader.
        let vertex_state = Vertex::get_default_vertex_state();
        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };

        //raster_info
        let raster_info = RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //multisample_state
        let multisample_state = MultisampleState {
            sample_count: msaa_flag,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //depth_stencil_state
        let depth_stencil_state = DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::GREATER_OR_EQUAL,
            stencil_test: StencilTestState::NoTest,
            depth_bounds: None,
        };
        //Blend with everything we get
        let color_blend_state = ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            Some(vk::LogicOp::COPY),
            vec![
                //Our Color attachment, we don't blend, but discard in the shader if the alpha value is below 1.0
                ColorBlendAttachmentState::NoBlending,
		ColorBlendAttachmentState::NoBlending,
            ],
        );

        //Create pipeline
        let pipeline_state = PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            ViewportState::new(ViewportMode::Dynamic(1)), //We have no viewport state since we set the viewport at runtime
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );

        //Create a fake push constant to have some range
        let fake_push = PushConstant::new(
            TextPush {
                transform: [[0.0; 4]; 4],
                level: 1.0,
                color: [1.0; 4],
		power: 0.0,
                pad1: [1.0; 2]
            },
            vk::ShaderStageFlags::ALL,
        );

        //Setup the persistent descriptor sets as well as the final pipeline layout, all of the primitives will use
        let pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            vec![],
            vec![*fake_push.range()],
        ).expect("Failed to create GraphicsPipelineLayout for text pipeline");

        let pipeline = GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass,
            0, //Use subpass 0
        ).expect("Failed to create Graphics pipeline");
	
	TextPipeline{
	    pipeline
	}
    }

    ///Draw the supplied buffer pairs of (vertex_buffer, index_buffer)
    /// # Note on the index buffer
    ///The first is the index buffer length, the second is the actual buffer
    pub fn draw(
	&self,
	index_buffer: (usize, Arc<Buffer>),
	vertex_buffer: Arc<Buffer>,
	draw_calls: &[(TextPush, vk::Rect2D)],
	command_buffer: Arc<CommandBuffer>,
    ){
	//First bind our pipeline then, from call to call bind the vertex and index buffer and draw.
	command_buffer
            .cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind Rect pipeline!");

        command_buffer
            .cmd_bind_index_buffer(
                index_buffer.1.clone(),
                0, //No offset into the buffer
                vk::IndexType::UINT32,
            )
            .expect("Failed to bind Rect Index Buffer!");

        command_buffer
            .cmd_bind_vertex_buffers(
		0,
		vec![(vertex_buffer.clone(), 0)]
	    )
            .expect("Failed to bind vertex buffer");
	
	for (call, scissors) in draw_calls.into_iter(){	    
	    //Setup the scissors for this call
            command_buffer
                .cmd_set_scissors(vec![*scissors])
                .expect("Failed to set scissors!");

	    command_buffer.cmd_push_constants(
		self.pipeline.layout(),
		&PushConstant::new(
		    *call,
                    vk::ShaderStageFlags::ALL,
		)
	    ).expect("Failed to push new consts for surface");

            command_buffer
                .cmd_draw_indexed(index_buffer.0 as u32, 1, 0, 0, 1)
                .expect("Failed to draw rect!");
	}
    }
}

