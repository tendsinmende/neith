use std::time::Instant;
use std::cmp::Ordering;

use neith::Interface;
use neith::config::Color;
use neith::event::Event;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::io::Value;
use neith::com::MutVal;
use neith;

use neith::primitive::Lines;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;
use splines::*;

use crate::graph::Graph;

pub const MAX_RES: f32 = 0.000125;
pub const PIXEL_PER_SEGMENTS: Unit = 1.0;

//TODO make generic over any plotable type?
pub struct LineGraph{
    resolution: f32,
    data: Value<Vec<(f32, f32)>>,
    cached_spline: Spline<f32, f32>,
    is_up_to_date: bool,
    area: Area,
    interpolation: bool,
    color: Option<Color>,
}

impl LineGraph{
    pub fn new() -> Self{
	LineGraph{
	    resolution: 1.0,
	    data: Value::Own(Vec::new()),
	    area: Area::empty(),
	    cached_spline: Spline::from_vec(vec![]),
	    is_up_to_date: false,
	    interpolation: false,
	    color: None,
	}
    }

    pub fn with_resolution(mut self, resolution: f32) -> Self{
	self.resolution = resolution.max(MAX_RES);
	self
    }

    pub fn with_data(mut self, data: Vec<(f32, f32)>) -> Self{
	self.data = Value::Own(data);
	self.is_up_to_date = false;
	self
    }

    pub fn with_mut_val(mut self, data: MutVal<Vec<(f32, f32)>>) -> Self{
	self.data = Value::MutVal(data);
	self.is_up_to_date = false;
	self
    }
    
    pub fn with_color(mut self, color: Color) -> Self{
	self.color = Some(color);
	self
    }
    
    //Sorts inner data based on the x value
    fn sort(&mut self){
	self.data.on_data_mut(|d| d.sort_by(|(ax,_ay), (bx, _by)| ax.partial_cmp(bx).unwrap_or(Ordering::Less)));
    }
    
    pub fn on_data<F>(&mut self, f: F) where F: Fn(&mut [(f32, f32)]){
	self.data.on_data_mut(|mut d| f(&mut d));
	self.sort();
	self.is_up_to_date = false;
    }

    pub fn set_data(&mut self, d: Vec<(f32, f32)>){
	match &mut self.data{
	    Value::Own(ref mut t) => *t = d,
	    Value::MutVal(t) => *t.get_mut() = d,
	}
	self.sort();
	self.is_up_to_date = false;
    }

    pub fn set_color(&mut self, color: Color){
	self.color = Some(color);
    }

    ///When set, interpolates the points which makes the graph smoother, but might distort the actual data set.
    pub fn with_interpolation(mut self) -> Self{
	self.interpolation = true;
	self
    }

    fn update_spline(&mut self){
	if self.data.on_data::<_, usize>(|d| d.len()) == 0{
	    return;
	}


	let (max_x, min_x, max_y, min_y) = self.data.on_data::<_, (f32, f32, f32, f32)>(|d|d.iter().fold(
	    (std::f32::MIN, std::f32::MAX, std::f32::MIN, std::f32::MAX),
	    |(max_x, min_x, max_y, min_y), (px, py)| {
		(max_x.max(*px), min_x.min(*px), max_y.max(*py), min_y.min(*py))
	    }
	    
	));
	let x_extent = max_x - min_x;
	let y_extent = max_y - min_y;
	//Depending on interpolation option, create point set
	let point_set = self.data.on_data(|d| d.iter().map(|(px, py)|{
	    let x = self.area.from.x + (((px - min_x) / x_extent) as Unit * self.area.extent().x);
	    let y = self.area.from.y + self.area.extent().y - ((*py as f32 - min_y as f32) / y_extent as f32 * self.area.extent().y);
	    Key::new(x, y, if self.interpolation{ Interpolation::CatmullRom }else{Interpolation::Linear})
	}).collect());

	self.cached_spline = Spline::from_vec(point_set);
    }
    
    fn get_draw_points(&self, area: Area) -> Vec<Vector2<Unit>>{
	let num_samples = (area.extent().x / PIXEL_PER_SEGMENTS).ceil() as usize;
	(0..num_samples).map(|i| {
	    let x = area.from.x + area.extent().x * (i as f32 / num_samples as f32);

	    let v: Vector2<Unit> = Vector2::new(
		x,
		self.cached_spline.sample(x).unwrap_or(0.0)
	    );
	    
	    v
	}).collect()
    }
}


impl Widget for LineGraph{
    fn on_event(&mut self, _event: &Event, _location: &Vector2<Unit>) {
	
    }

    fn area(&self) -> Area {
	self.area
    }

    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	_draw_time: Instant,
	_interface: &Interface,
	level: usize
    ) -> DrawCalls {
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	if self.area.from.x > self.area.to.x || self.area.from.y > self.area.to.y{
	    //Cannot draw diagram, therefore return already
	    return DrawCalls::new(vec![]);
	}


	if !self.is_up_to_date || self.data.is_shared(){
	    self.update_spline();
	    self.is_up_to_date = false;
	}
	
	let mut prims = Vec::new();

	
	let located_points = self.get_draw_points(self.area);
	let style = Style::from_color(self.color.clone().unwrap_or(Color::Float([0.8, 0.8, 0.2])));
	prims.push(
	    Prim{
		prim: PrimType::Lines(Lines{
		    points: located_points,
		    width: 1.0,
		}),
	        drawing_area: self.area,
	        scissors: None,
	        style,
	        level,
	    }
	);
	
	DrawCalls::new(prims)
    }
}

impl Graph for LineGraph{
    fn get(&self, x: f32) -> Option<f32> {
	self.cached_spline.sample(x)
    }
    
}
