use std::sync::{Arc, RwLock, RwLockReadGuard, RwLockWriteGuard};

use crate::io::TextEditable;


///A sender, is created from a Recv.
#[derive(Clone)]
pub struct Sender<T> {
    shared: Arc<RwLock<Vec<T>>>,
}

impl<T> Sender<T> {
    pub fn new() -> Sender<T> {
        Sender {
            shared: Arc::new(RwLock::new(Vec::new())),
        }
    }

    pub fn new_receiver(&self) -> Recv<T> {
        Recv {
            shared: self.shared.clone(),
        }
    }

    pub fn send(&self, data: T) {
        self.shared
            .write()
            .expect("Could not write value")
            .push(data);
    }
}

pub struct Recv<T> {
    shared: Arc<RwLock<Vec<T>>>,
}

impl<T> Recv<T> {
    pub fn new() -> Recv<T> {
        Recv {
            shared: Arc::new(RwLock::new(Vec::new())),
        }
    }

    pub fn new_sender(&self) -> Sender<T> {
        Sender {
            shared: self.shared.clone(),
        }
    }

    pub fn recv(&self) -> Vec<T> {
        let mut buff = Vec::with_capacity(
            self.shared
                .read()
                .expect("Could not read buffer size")
                .len(),
        );
        std::mem::swap(
            &mut *self.shared.write().expect("Could not swap data for recv()"),
            &mut buff,
        );

        buff
    }
}

///A value that is shared between the interface and the user
///TODO exchange to RCU once a good, working RCU is found.
#[derive(Clone)]
pub struct MutVal<T: ?Sized> {
    shared: Arc<RwLock<T>>,
}

impl MutVal<dyn TextEditable + Send + Sync + 'static>{
    pub fn new_shared_text<T>(data: T) -> Self where T: TextEditable + Send + Sync + 'static{
	MutVal{
	    shared: Arc::new(RwLock::new(data))
	}
    }
    pub fn get<'a>(&'a self) -> RwLockReadGuard<'a, dyn TextEditable + Send + Sync + 'static> {
        self.shared.read().expect("Failed to get value!")
    }

    pub fn get_mut<'a>(&'a self) -> RwLockWriteGuard<'a, dyn TextEditable + Send + Sync + 'static> {
        self.shared.write().expect("Failed to get mutable value")
    }
}

impl<T: TextEditable + Send + Sync + 'static> MutVal<T>{
    ///Converts the MutVal<T> into a dynamic trait object for TextEditables
    pub fn into_shared_text(self) -> MutVal<dyn TextEditable + Send + Sync + 'static>{
	MutVal{
	    shared: self.shared
	}
    }
}

impl<T: Send + Sync> MutVal<T> {
    pub fn new(data: T) -> Self {
        MutVal {
            shared: Arc::new(RwLock::new(data)),
        }
    }

    pub fn get<'a>(&'a self) -> RwLockReadGuard<'a, T> {
        self.shared.read().expect("Failed to get value!")
    }

    pub fn get_mut<'a>(&'a self) -> RwLockWriteGuard<'a, T> {
        self.shared.write().expect("Failed to get mutable value")
    }
}
