use std::path::Path;

use neith::math::Vector2;




///Loads a object from an obj file into a 2d vertex buffer and index buffer.
///Note that the vertices should only be put into the area [0, 1] on x/y. Otherwise strange stuff will happen.
///returns none of the loading fails. The error will be on the console.
pub fn load_polygon(obj_data: &'static [u8]) -> Option<(Vec<Vector2<f32>>, Vec<u32>)>{
    let objs = if let Ok(obj) = obj::ObjData::load_buf(obj_data){
	obj
    }else{
	println!("Failed to load obj");
	return None;
    };

    assert!(objs.objects.len() == 1, "There was more then one object in the file!");
    assert!(objs.objects[0].groups.len() == 1, "There was more then one group on the object");
    
    let vertex_buffer = objs.position.iter().map(|[x,y,z]| {
	Vector2::new(*x, *y)
    }).collect();

    let mut index_buffer = Vec::with_capacity(objs.objects[0].groups[0].polys.len() * 3);

    for poly in objs.objects[0].groups[0].polys.iter(){
	assert!(poly.0.len() == 3, "Object had non triangulated face, num_polys: {}!", poly.0.len());
	
	index_buffer.push(poly.0[0].0 as u32);
	index_buffer.push(poly.0[1].0 as u32);
	index_buffer.push(poly.0[2].0 as u32);
	let len = index_buffer.len();
    }
    
    Some((vertex_buffer, index_buffer))
}
