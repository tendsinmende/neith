
//! # IO Widgets
//! Contains most of the widgets related to editing values. Every editable value can usually be contained within a widget
//! or shared via a `MutVal<T>`.
//!
pub mod button;
pub use button::*;

pub mod label;
pub use label::Label;

///Some text field that can edit any value that implements `TextEditable`
pub mod edit;
pub use edit::Edit;

pub mod slider;
pub use slider::Slider;

use crate::com::MutVal;

pub mod drop_down;
pub use drop_down::*;

///A value can either be just some type T, or a shared type T aka. `MutVal<T>`.
pub enum Value<T>{
    Own(T),
    MutVal(MutVal<T>)
}

impl<T> Value<T> where T: Send + Sync + 'static{
    pub fn on_data_mut<F, R>(&mut self, mut f: F) -> R where F: FnMut(&mut T) -> R{
	match self{
	    Value::Own(ref mut t) => f(t),
	    Value::MutVal(t) => f(&mut *t.get_mut()),
	}
    }
    pub fn on_data<F, R>(&self, mut f: F) -> R where F: FnMut(&T) -> R{
	match self{
	    Value::Own(ref t) => f(t),
	    Value::MutVal(t) => f(&mut *t.get_mut()),
	}
    }

    pub fn is_shared(&self) -> bool{
	if let Value::Own(_) = self{
	    true
	}else{
	    false
	}
    }
}

///Trait that must be implemented for any type that can be manipulated by some text based edit field.
pub trait TextEditable{
    ///Creates a string from the current value that might be displayed in the interface toolkit
    fn to_string(&self) -> String;
    ///overwrites self with a value pared from the string. Might do nothing if parsing failes. In that case `false` is returned
    fn from_string(&mut self, string: &str) -> bool;   
}

impl TextEditable for String{
    fn to_string(&self) -> String {
	self.to_owned()
    }

    fn from_string(&mut self, string: &str) -> bool {
	*self = string.to_owned();
	true
    }
}

///Trait used for any value that can be altered by some slider or something similar.
///
/// # Note
/// The trait extents `TextEditable`, so every dynamic value can be converted from/to a string.
pub trait DynamicValue : TextEditable{
    fn add(&mut self, addition: Self);
    fn subtract(&mut self, sub: Self);
    fn get(&self) -> &Self;
    fn get_mut(&mut self) -> &mut Self;
    ///Returns the value that is "correct" on a range [min..max] with given step_size
    fn value_for_percentage(min: &Self, max: &Self, step_size: &Self, perentage: f32) -> Self;
    ///Returns the percentage the current value has on a range of [min..max]
    fn percentage_for_value(&self, min: &Self, max: &Self) -> f32;
}

macro_rules! impl_DynamicValueOnBasicType{
    ($impl_type:ty) => {
	impl TextEditable for $impl_type{
	    fn to_string(&self) -> String{
		format!("{}", self)
	    }

	    fn from_string(&mut self, string: &str) -> bool{
		if let Ok(parsed) = string.parse::<$impl_type>(){
		    *self = parsed;
		    true
		}else{
		    false
		}
	    }	    
	}
	
	impl DynamicValue for $impl_type{
	    fn add(&mut self, addition: Self){
		*self = *self + addition;
	    }
	    fn subtract(&mut self, sub: Self){
		*self = *self - sub;
	    }
	    fn get(&self) -> &Self{
		self
	    }
	    fn get_mut(&mut self) -> &mut Self{
		self
	    }
	    fn value_for_percentage(min: &Self, max: &Self, step_size: &Self, percentage: f32) -> Self{
		let ext = (max - min) as f32 * percentage;
		let steps = (ext / *step_size as f32).round();
		min + (steps as Self * step_size)
	    }
	    fn percentage_for_value(&self, min: &Self, max: &Self) -> f32{
		((*self - min) as f32/ (max - min) as f32).max(0.0).min(1.0)
	    }
	}
    }
}

impl_DynamicValueOnBasicType!(usize);
impl_DynamicValueOnBasicType!(u32);
impl_DynamicValueOnBasicType!(u64);
impl_DynamicValueOnBasicType!(u16);
impl_DynamicValueOnBasicType!(u8);
impl_DynamicValueOnBasicType!(f32);
impl_DynamicValueOnBasicType!(f64);
impl_DynamicValueOnBasicType!(isize);
impl_DynamicValueOnBasicType!(i16);
impl_DynamicValueOnBasicType!(i8);
impl_DynamicValueOnBasicType!(i32);
impl_DynamicValueOnBasicType!(i64);
impl_DynamicValueOnBasicType!(u128);


