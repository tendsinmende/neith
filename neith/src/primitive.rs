use crate::math::*;
use crate::Style;


///A font name decribeing which font a character should have.
/// When `Font::default()` is used, the backend can decide which is used.
#[derive(Clone, Hash, Eq, PartialEq)]
pub struct FontName{
    name: String
}

impl From<&str> for FontName{    
    fn from(name: &str) -> Self {
	FontName{
	    name: String::from(name)
	}
    }
}

impl Default for FontName{
    fn default() -> Self{
	FontName{
	    name: String::from("Default")
	}
    }
}

#[derive(Clone)]
pub struct Lines{
    ///Points on this line
    pub points: Vec<Vector2<Unit>>,
    ///Width of this line, should not be > 2. In that case consider building a polygon, which is much faster on the gpu. 
    pub width: f32,
}

#[derive(Clone)]
pub struct Character{
    pub ch: char,
    pub size: Unit,    
    pub font: FontName
}

///The handle that describes a polygone that was already registered at the backend.
/// use the interface's backend's register_polygone() to obtain such a handle. 
#[derive(Clone, Debug)]
pub struct PolyHandle{
    ///Id that is used by the backend to identify the polygone
    pub id: &'static str
}

impl PolyHandle{
    ///Creates a new handle. Should only be used by the backend, since the id must be valid.
    /// otherwise the polygone might not be drawn. However if you know that a poylgone with the given name exists,
    /// you are free to create the handle your self.
    pub fn new(id: &'static str) -> Self{
	PolyHandle{
	    id
	}
    }
}

#[derive(Clone)]
pub enum PrimType{
    Polygon(PolyHandle),
    Lines(Lines),
    Rect,
    Character(Character)
}

///Rendering Primitive
#[derive(Clone)]
pub struct Prim{
    pub prim: PrimType,
    ///The Area in which the primitive should be drawn. `from` is the position of the primitives origin. Everything that
    /// would be drawn outside this area should be ignored.
    pub drawing_area: Area,
    ///If needed a custom scissors area that defines an area which the drawn primitive can not exceed.
    pub scissors: Option<Area>,
    pub style: Style, //Might move into primitive later
    pub level: usize
}

impl Prim{
    pub(crate) fn offset(&mut self, offset: Vector2<Unit>){
	self.drawing_area.offset(offset);
	if let Some(ref mut sc) = self.scissors{
	    sc.offset(offset);
	}
	match &mut self.prim{
	    PrimType::Lines(l) => {
		for p in l.points.iter_mut(){
		    *p = *p + offset;
		}
	    },
	    _ => {} //Other prims are not offseted.
	}
	
	
    }

    pub fn area(&self) -> Area{
	if let Some(sc) = self.scissors{
	    sc
	}else{
	    self.drawing_area
	}
    }
}
