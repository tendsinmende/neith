use neith::math::Area;
use neith::primitive::Character;
use neith::primitive::Lines;
use neith::primitive::PolyHandle;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::Style;

use crate::character::CharacterManager;
use crate::polygon::Poly;
use std::sync::Arc;
use std::sync::RwLock;

use crate::vulkan_prelude::*;
use crate::rectangle::Rectangle;
use crate::line::LinesDraw;

pub(crate) struct FrameData{
    #[allow(dead_code)]
    msaa_color_image: Arc<Image>,
    #[allow(dead_code)]
    msaa_emission_image: Arc<Image>,
    color_image: Arc<Image>,
    emission_image: Arc<Image>,
    #[allow(dead_code)]
    pub(crate) color_format: vk::Format,
    #[allow(dead_code)]
    msaa_depth_image: Arc<Image>,
    depth_format: vk::Format,

    framebuffer: Arc<Framebuffer>
}

impl FrameData{
    pub fn new(
	device: Arc<Device>,
	color_format: vk::Format,
	depth_format: vk::Format,
	(width,height): (u32,u32),
	renderpass: Arc<RenderPass>,
	msaa_count: u32,
    ) -> Self{	

	let color_image_info = ImageInfo::new(
            ImageType::Image2D {
                width,
                height,
                samples: 1,
            },
            color_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                color_attachment: true,
                input_attachment: true,
                transfer_src: true,
		storage: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

        let color_image = marp::image::Image::new(
            device.clone(),
            color_image_info.clone(),
            SharingMode::Exclusive,
        ).expect("Failed to create color image!");

	let emission_image = marp::image::Image::new(
            device.clone(),
            color_image_info,
            SharingMode::Exclusive,
        ).expect("Failed to create emission image!");

	let msaa_color_image_info = ImageInfo::new(
            ImageType::Image2D {
                width,
                height,
                samples: msaa_count,
            },
            color_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                color_attachment: true,
                input_attachment: true,
                transfer_src: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );

	let msaa_depth_image_info = ImageInfo::new(
            ImageType::Image2D {
                width,
                height,
                samples: msaa_count,
            },
            depth_format,
            None,
            Some(MipLevel::Specific(1)),
            ImageUsage {
                depth_attachment: true,
                input_attachment: true,
		transfer_src: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None,
        );
	
        let msaa_color_image = marp::image::Image::new(
            device.clone(),
            msaa_color_image_info.clone(),
            SharingMode::Exclusive,
        ).expect("Failed to create color image!");
	
	let msaa_emission_image = marp::image::Image::new(
            device.clone(),
            msaa_color_image_info,
            SharingMode::Exclusive,
	).expect("Failed to create emission image!");
	
	let msaa_depth_image = marp::image::Image::new(
            device.clone(),
            msaa_depth_image_info,
            SharingMode::Exclusive,
	).expect("Failed to create depth image!");
	
	//Now build the frame buffer in order of the attachment description
        let framebuffer = marp::framebuffer::Framebuffer::new(
            device.clone(),
            renderpass.clone(),
            vec![
		color_image.clone(),
		emission_image.clone(),
		msaa_color_image.clone(),
		msaa_emission_image.clone(),
		msaa_depth_image.clone()
	    ],
        ).expect("Failed to create first frame buffer");
	

	FrameData{
	    color_format,
	    color_image,
	    emission_image,
	    msaa_color_image,
	    msaa_emission_image,
	    
	    depth_format,
	    msaa_depth_image,
		
	    framebuffer
	}
    }
}

pub struct InterfacePass{

    device: Arc<Device>,
    
    ///Holds the frames final depth and color image
    pub(crate) frame_data: Vec<FrameData>,
    pub(crate) rectangle: Rectangle,
    pub(crate) character: CharacterManager,
    pub(crate) lines: LinesDraw,
    pub(crate) polys: RwLock<Poly>,
    
    render_pass: Arc<RenderPass>,

    screen_area: Area,
    msaa_count: u32,
}

impl InterfacePass{
    pub fn new(
	device: Arc<Device>,
	queue: Arc<Queue>,
	(width, height): (u32, u32),
	num_frames: usize,
	_swapchain_format: vk::Format,
    ) -> Self{

	//Find out max sampling count for msaa	
	let physical_limit = device
	    .get_physical_device()
	    .get_device_properties()
	    .limits
	    .framebuffer_depth_sample_counts
	    &
	    device
	    .get_physical_device()
	    .get_device_properties()
	    .limits.framebuffer_color_sample_counts;

	let color_count = {
	    if (physical_limit & vk::SampleCountFlags::TYPE_32) > vk::SampleCountFlags::TYPE_1 { 64 }

	    else if (physical_limit & vk::SampleCountFlags::TYPE_32) > vk::SampleCountFlags::TYPE_1 { 32 }

	    else if (physical_limit & vk::SampleCountFlags::TYPE_16) > vk::SampleCountFlags::TYPE_1 { 16 }

	    else if (physical_limit & vk::SampleCountFlags::TYPE_8) > vk::SampleCountFlags::TYPE_1 { 8 }

	    else if (physical_limit & vk::SampleCountFlags::TYPE_4) > vk::SampleCountFlags::TYPE_1 { 4 }

	    else if (physical_limit & vk::SampleCountFlags::TYPE_2) > vk::SampleCountFlags::TYPE_1 { 2 }
	    else{1}
	};

	let msaa_count = color_count;
	let  msaa_flag = match msaa_count{
	    1 => vk::SampleCountFlags::TYPE_1,
	    2 => vk::SampleCountFlags::TYPE_2,
	    4 => vk::SampleCountFlags::TYPE_4,
	    8 => vk::SampleCountFlags::TYPE_8,
	    16 => vk::SampleCountFlags::TYPE_16,
	    32 => vk::SampleCountFlags::TYPE_32,
	    64 => vk::SampleCountFlags::TYPE_64,
	    _ => vk::SampleCountFlags::TYPE_1,
	};
	
	
	//Find depth format and create depth attachment image for it
	let depth_format = device
            .get_useable_format(
                vec![
                    vk::Format::D16_UNORM_S8_UINT,
                    vk::Format::D24_UNORM_S8_UINT,
                    vk::Format::D32_SFLOAT_S8_UINT,
                ],
                &ImageUsage {
                    depth_attachment: true,
                    depth_aspect: true,
                    input_attachment: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a depth format");

	
	//Same story for the main image attachment
	let color_format = device
            .get_useable_format(
                vec![
                    vk::Format::R8G8B8A8_UNORM,
                    vk::Format::R8G8B8A8_UINT,
		],
                &ImageUsage {
                    color_attachment: true,
                    input_attachment: true,
                    transfer_src: true,
                    ..Default::default()
                },
                vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a color format");	
	 
	//let color_format = swapchain_format;
	println!("Formats: col: {:?}, depth: {:?} with {}x msaa", color_format, depth_format, msaa_count);
	
	//Setup the renderpass we want to use while rendering the triangle
        let attachment_descs = vec![
            //The color attachment
            marp::framebuffer::AttachmentDescription::new(
                vk::SampleCountFlags::TYPE_1,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::GENERAL, //After the renderpass, transition to GENERAL for the post progress pass
            ),
	    //emission attachment
	    marp::framebuffer::AttachmentDescription::new(
                vk::SampleCountFlags::TYPE_1,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::GENERAL, //After the renderpass, transition to GENERAL as well
            ),
	    //The msaa color image
	    marp::framebuffer::AttachmentDescription::new(
                msaa_flag,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL, //After the renderpass, transition to to make resolve possible
            ),
	    //msaa emission image
	    marp::framebuffer::AttachmentDescription::new(
                msaa_flag,
                color_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL, //After the renderpass, transition to to make resolve possible
            ),
	    //The msaa depth image
	    marp::framebuffer::AttachmentDescription::new(
                msaa_flag,
		depth_format,
                vk::AttachmentStoreOp::STORE,
                vk::AttachmentLoadOp::CLEAR,
                None,                                  // no stencil store
                None,                                  // no stencil load
                None, //No initial layout needed since we clear anyways
                vk::ImageLayout::GENERAL, //After the renderpass, transition to TRANSFER_OPTIMAL
            )
	     
        ];

        //This are the subpasses this renderpass goes through. However, this is easy in a triangle example.
        let render_pass = marp::framebuffer::RenderPass::new(attachment_descs)
            .add_subpass(vec![
                marp::framebuffer::AttachmentRole::Resolve, //Normal color image
		marp::framebuffer::AttachmentRole::Resolve, //normal emission image,
                marp::framebuffer::AttachmentRole::OutputColor, //msaa color image
		marp::framebuffer::AttachmentRole::OutputColor, //msaa emission image
                marp::framebuffer::AttachmentRole::DepthStencil, //msaa depth image
            ])
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                vk::SUBPASS_EXTERNAL,                            //from external pass
                0,                                               //To first and only subpass
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //When color output has finished on src
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //Color output can start on this one
                vk::AccessFlags::empty(),                        //Don't know access type of source
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //We need color read and write support
            ))
            .add_dependency(marp::framebuffer::SubpassDependency::new(
                0,
                vk::SUBPASS_EXTERNAL,
                vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::AccessFlags::COLOR_ATTACHMENT_READ | vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //Don't know access type of source
                vk::AccessFlags::empty(),
            ))
            .build(device.clone())
            .expect("Failed to build renderpass!");

        let frame_infos: Vec<_> = (0..num_frames).into_iter().map(|_i| FrameData::new(
	    device.clone(),
	    color_format,
	    depth_format,
	    (width, height),
	    render_pass.clone(),
	    msaa_count
	)).collect();

	
	InterfacePass{
	    render_pass: render_pass.clone(),
	    frame_data: frame_infos,
	    
	    rectangle: Rectangle::new(device.clone(), queue.clone(), render_pass.clone(), msaa_flag),
	    character: CharacterManager::new(device.clone(), queue.clone(), render_pass.clone(), msaa_flag),
	    lines: LinesDraw::new(device.clone(), queue.clone(), render_pass.clone(), msaa_flag),
	    polys: RwLock::new(Poly::new(device.clone(), queue.clone(), render_pass.clone(), msaa_flag)), //Currently dirty so we dont have to &mut the whole backend to just register a polygone.
	    
	    screen_area: Area{
		from: (0.0, 0.0).into(),
		to: (width as f32, height as f32).into()
	    },
	    device,
	    msaa_count,
	}
    }

    ///Recreates the images based on the new extent
    pub fn resize(&mut self, new_extent: (u32, u32), num_frames: u32){

	let color_format = self.frame_data[0].color_format;
	let depth_format = self.frame_data[0].depth_format;

	self.frame_data = (0..num_frames).into_iter().map(|_i| FrameData::new(
	    self.device.clone(),
	    color_format,
	    depth_format,
	    new_extent,
	    self.render_pass.clone(),
	    self.msaa_count
	)).collect();

	self.screen_area = Area{
	    from: (0.0, 0.0).into(),
	    to: (new_extent.0 as f32, new_extent.1 as f32).into()
	};
    }

    ///Oders the 
    fn order_calls(
	calls: Vec<Prim>,
	rectangle_calls: &mut Vec<(Area, Option<Area>, Style, f32)>,
	character_calls: &mut Vec<(Area, Option<Area>, Style, f32, Character)>,
	line_calls: &mut Vec<(Area, Option<Area>, Style, f32, Lines)>,
	poly_calls: &mut Vec<(Area, Option<Area>, Style, f32, PolyHandle)>
    ){
	let mut max_level = 0;
	
	for mut call in calls.into_iter(){

	    max_level = max_level.max(call.level);
	    if let Some(ref mut sc) = call.scissors{
		sc.from.x = sc.from.x.max(0.0);
		sc.from.y = sc.from.y.max(0.0);
		sc.to.x = sc.to.x.max(0.0);
		sc.to.y = sc.to.y.max(0.0);

		if sc.has_zero_dim(){
		    continue; //Ignore call
		}
	    }
	    
	    match call.prim{

		PrimType::Lines(line) => {
		    line_calls.push((
			call.drawing_area,
			call.scissors,
			call.style,
			call.level as f32,
			line
		    ));
		},
		PrimType::Rect => {		    
		    rectangle_calls.push((
			call.drawing_area,
			call.scissors,
			call.style,
			call.level as f32
		    ));
		},
		PrimType::Character(ch) => {
		    character_calls.push((
			call.drawing_area,
			call.scissors,
			call.style,
			call.level as f32,
			ch
		    ));
		},
		PrimType::Polygon(handle) => {
		    poly_calls.push(
			(
			    call.drawing_area,
			    call.scissors,
			    call.style,
			    call.level as f32,
			    handle
			)
		    );
		}
	    }
	}

	//Update all levels to be within the range of [0,1]
	for c in rectangle_calls.iter_mut(){
	    c.3 = (c.3 as f32) * (1.0 / max_level as f32);
	}

	for c in character_calls.iter_mut(){
	    c.3 = (c.3 as f32) * (1.0 / max_level as f32);
	}

	for c in line_calls.iter_mut(){
	    c.3 = (c.3 as f32) * (1.0 / max_level as f32);
	}

	for c in poly_calls.iter_mut(){
	    c.3 = (c.3 as f32) * (1.0 / max_level as f32);
	}
	
    }

    ///Starts a renderpass that writes to the inner framebuffer of the given `index`.
    pub fn draw_interface(
	&mut self,
	command_buffer: Arc<CommandBuffer>,
	index: usize,
	prmitives: Vec<Prim>
    ){

	//Before executing anything, update subsystems
	let mut rectangle_calls = Vec::new();
	let mut character_calls = Vec::new();
	let mut line_calls = Vec::new();
	let mut poly_calls = Vec::new();
	InterfacePass::order_calls(
	    prmitives,
	    &mut rectangle_calls,
	    &mut character_calls,
	    &mut line_calls,
	    &mut poly_calls
	);
	
	self.character.update(self.device.clone(), command_buffer.clone());
	self.lines.update_buffers(self.screen_area, line_calls);
	
	let clear_values = vec![
	    vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [1.0, 0.0, 1.0, 1.0],
                },
            },
	    vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [1.0, 0.0, 1.0, 1.0],
                },
            },
	    vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [1.0, 0.0, 1.0, 1.0],
                },
            },
	    vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [1.0, 0.0, 1.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 0.0,
                    stencil: 0,
                },
            },
        ];

	let rendering_rect = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: vk::Extent2D {
                width: self.screen_area.extent().x.floor() as u32,
                height: self.screen_area.extent().y.floor() as u32,
            },
        };
	command_buffer
            .cmd_begin_render_pass(
                self.render_pass.clone(),
                self.frame_data[index].framebuffer.clone(),
                rendering_rect,
                clear_values,
                vk::SubpassContents::INLINE,
            ).expect("Failed to start renderpass!");

	//Set viewport for rendering pass
	command_buffer
            .cmd_set_viewport(vec![vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: self.screen_area.extent().x as f32,
                height: self.screen_area.extent().y as f32,
                min_depth: 0.0,
                max_depth: 1.0,
            }])
            .expect("Failed to set viewport");


	//Now execute the calls on the sub systems
	self.lines.draw(
	    command_buffer.clone(),
	    self.screen_area
	);
	
	self.character.draw(
	    command_buffer.clone(),
	    character_calls,
	    self.screen_area
	);
	
	self.rectangle.draw_at(
	    self.screen_area,
	    &rectangle_calls,
	    command_buffer.clone()
	);

	self.polys.read().unwrap().draw_at(
	    self.screen_area,
	    poly_calls,
	    command_buffer.clone()
	);
	
	command_buffer.cmd_end_render_pass().expect("Failed to end renderpass!");
	
    }

    ///Returns the single sampled color image of this pass
    pub fn get_image(&self, index: usize) -> Arc<Image>{
	self.frame_data[index].color_image.clone()
    }

    pub fn get_images(&self) -> Vec<(Arc<Image>, Arc<Image>)>{
	self.frame_data.iter().map(|d| (d.color_image.clone(), d.emission_image.clone())).collect()
    }
}
