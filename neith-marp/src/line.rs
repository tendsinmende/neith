use neith::widget::Style;
use neith::math::Area;
use crate::pipelines::lines::LinesPipeline;
use crate::pipelines::surface::SurfacePush;
use crate::vulkan_prelude::*;
use std::sync::Arc;
use neith::primitive::Lines;

use crate::command_builder::Vertex;

///Rectangle primitive. Uses the `sruface` shader to draw some generic rectangle
pub struct LinesDraw{
    device: Arc<Device>,
    queue: Arc<Queue>,
    
    pipeline: LinesPipeline,
    //Current vertex buffer
    vertex_buffer: Arc<Buffer>,

    //The cpu side data used for the draw funtion.
    prims: Vec<(Area, Option<Area>, Style, f32, Lines)>
}


impl LinesDraw{
    pub fn new(
	device: Arc<Device>,
	queue: Arc<Queue>,
	render_pass: Arc<RenderPass>,
	msaa_flag: vk::SampleCountFlags
    ) -> Self{

	//Dummy buffer
        let vertices = vec![
            Vertex {
                pos: [0.0, 1.0, 0.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0, 0.0, 1.0],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [1.0, 0.0, 0.0, 1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.0, 0.0, 1.0],
                uv: [0.0, 0.0],
            },
        ];

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            device.clone(),
            queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

	let pipeline = LinesPipeline::new(device.clone(), render_pass, msaa_flag);

	LinesDraw{
	    device,
	    queue,
	    
	    vertex_buffer,
	    pipeline,
	    prims: vec![]
	}
    }

    ///Updates inner vertex and index buffer with new lines for this call.
    ///TODO schedule upload on command buffer.
    pub fn update_buffers(
	&mut self,
	screen_area: Area,
	prims: Vec<(Area, Option<Area>, Style, f32, Lines)>
    ){

	if prims.len() == 0{
	    self.prims = vec![];
	    return;
	}
	
	//Generate vertex buffer
	let mut vertices = Vec::with_capacity(prims.iter().fold(0, |last, (_a, _s, _st, _l, line)| last + line.points.len()));

	for (_ar, _sci, _st, _lvl, lines) in prims.iter(){
	    for p in lines.points.iter(){
		vertices.push(Vertex{
		    pos: [
			((p.x / screen_area.extent().x) * 2.0 ) - 1.0,
			((p.y / screen_area.extent().y) * 2.0 ) - 1.0,
			0.0,
			1.0
		    ],
		    uv: [0.0, 0.0]
		})
	    }
	}
	
        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            self.device.clone(),
            self.queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

	self.vertex_buffer = vertex_buffer;
	self.prims = prims;
    }
    
    
    ///the lines that where submitted to the update_buffers function.
    pub fn draw(
	&mut self,
	command_buffer: Arc<CommandBuffer>,
	screen_area: Area,
    ){

	if self.prims.len() == 0{
	    return;
	}
	//Take current prims and generate draw calls
	let mut taken_prims = vec![];
	std::mem::swap(&mut taken_prims, &mut self.prims);
	
	let mut push_scissors: Vec<_> = vec![];
	let mut offset = 0;
	for (area, scissors, style, level, line) in taken_prims.into_iter(){
	    let _transform = area.to_transform(&screen_area);
	    
	    let push_buf = SurfacePush{
		transform: neith::math::IDENTITY,
		color: style.color,
		power: style.power,
		level,
		pad1: [0.0; 2]
	    };

	    let scissors_area = if let Some(_sc) = scissors{
		area
	    }else{
		area
	    };

	    let last_offset = offset;
	    offset += line.points.len();
	    
	    push_scissors.push((last_offset, line.points.len(), line.width, push_buf, area_to_scissors(scissors_area, screen_area)));
	}

	self.pipeline.draw(
	    self.vertex_buffer.clone(),
	    &push_scissors,
	    command_buffer
	);
    }
}
