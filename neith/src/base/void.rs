use crate::Interface;
use crate::config::Color;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::PolyHandle;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;
use std::time::Instant;


///Fills the whole area of this widget with the background color. Can be used as a place holder.
pub struct Void{
    area: Area
}

impl Void{
    pub fn new() -> Self{
	Void{
	    area: Area::empty()
	}
    }
}

impl Widget for Void{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, _event: &Event, _location: &Vector2<Unit>){
	
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	_draw_time: Instant,
	_interface: &Interface,
	level: usize
    ) -> DrawCalls {	
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};
	DrawCalls::new(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level
	    }
	])
    }
}
