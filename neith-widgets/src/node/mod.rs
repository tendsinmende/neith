//! # Custom canvas and nodes
//! Usage of this widget is a little bit more advanced then just adding it to the interface. Since the Canvas allows actual 
//! communication between the nodes, as well as type checking, a common datatype for the underlying, abstract data must be enforced.
//!
//! That's what the `I` parameter is for. `I` must implement `NodeContent`. A trait that lets some node describe it self. It also
//! provides functions that are called whenever two nodes are connected over their ports.
//!
//! ## Custom canvas wrapper
//! To get some node in and out of an canvas, the functions `add_node(node: I) -> NodeId` and `remove_node(id: NodeId)` are provided.
//! The easiest way is, to write a smaller wrapper around the `NodeCanvas` that handles adding an removing nodes, depending on what you want.
//! Adding a node could happen when a button or a key is pressed. That's up to you.
//!
//! ## Custom nodes
//! To actually make an type `I` be usable as node, you'll only have to implement `NodeContent`. You'll only have to make one big decision: Do you need your abstract data node shared, or not?
//!
//! If your node is shared, implement it like this:
//!
//! ```rust
//! //needed since your can't implement a foreign trait on a foreign type, so we just construct a "new" type wrapper.
//! struct NodeWrapper(Arc<RwLock<T>>)
//! impl NodeContent for NodeWrapper{
//!     type Node = NodeWrapper;
//!     ...
//! }
//! ```
//!
//! Of you don't need access to your node after giving it to the canvas, just implement it for your type:
//!
//! ```rust
//! impl NodeContent for T{
//!     type Node = T;
//!     ....
//! }
//! ```
//! 
//! # Example
//! An in action example can be found in `neith-synth/src/test_nodes.rs`. It implements the shared version and adds new nodes
//! to the canvas when pressing "a".
//!
//!
//!


pub mod canvas;
pub mod node;

pub use node::*;
pub use canvas::*;

mod edge_container;
pub mod node_content;
pub use node_content::*;
