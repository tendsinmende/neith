use neith::com::MutVal;
use neith::config::Color;
use neith::io::TextEditable;
use neith::io::Value;
use crate::node::*;
use neith::widget::Widget;
use core::any::TypeId;
use std::fmt::Display;
use std::str::FromStr;

///Trait that must be implemented by any child of this node.
/// Tells the node the in and outputs the child has. Is also used to pass
/// ui actions down to this node.
pub trait NodeContent{
    ///Type deceleration how a generic node looks in this graph.
    type Node;
    ///Returns a ordered set of inputs for this node. Each input contains a type id as well as the name of this input.
    fn inputs(&self) -> Vec<(TypeId, String)>;
    ///Returns a ordered set of outputs for this node.
    fn outputs(&self) -> Vec<(TypeId, String)>;
    ///Gets called by the `NodeBox` whenever a edge tries to be connected to the `input_index` of this node.
    ///
    /// # Throwing error
    /// Can return an Err if the edge that is connecting has a wrong type, or tries to connect to an out of range index.
    fn try_in_connect(&mut self, input_index: usize, source: &Self::Node, source_out_index: usize) -> Result<(), ConnectionError>;
    fn try_out_connect(&mut self, out_index: usize, target: &Self::Node, target_in_index: usize) -> Result<(), ConnectionError>;

    ///Tells that this out port was disconnected
    fn in_disconnected(&mut self, in_index: usize) -> Result<(), ConnectionError>;
    fn out_disconnected(&mut self, out_index: usize) -> Result<(), ConnectionError>;


    ///Can be implemented by a node, if an out port at `idx` allows a default value to be mutated. Should be some small
    ///widget like a slider.
    fn in_default_value(&mut self, _idx: usize) -> Option<Box<dyn Widget + Send + 'static>>{
	None
    }

    ///Can be implemented by a node, if an out port at `idx` allows a default value to be mutated. Should be some small
    ///widget like a slider.
    fn out_default_value(&mut self, _idx: usize) -> Option<Box<dyn Widget + Send + 'static>>{
	None
    }
    
    ///Returns the interface that should be used for that node. Can return nothing if the node should be left empty.
    fn get_interface(&self) -> Option<Box<dyn Widget + Send + 'static>>{
	None
    }

    ///Returns a static title that's used for this node. Might return nothing, if there is no appropriate title for this node.
    fn get_title(&self) -> Option<String>{
	None
    }

    ///Can return a color that is used as the frame color for the created node.
    fn frame_color(&self) -> Option<Color>{
	None
    }
}
