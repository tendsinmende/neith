use log::warn;

use crate::Interface;
use crate::base::Text;
use crate::event::Event;
use crate::event::WindowEvent;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;
use crate::com::MutVal;
use crate::io::Value;

use std::rc::Rc;
use std::sync::Arc;
use std::time::Instant;

use super::TextEditable;


///Some editable value. There are two ways its content can be handled:
/// 1. Some `Value` is stored within the edit that can be retrieved via `get`
/// 2. A `MutVal<Value>` is stored, which can be shared.
///
/// While the 1. can only read the current state of the value, the 2. way actually allows you to change the edits content
/// from within your program. Also it allows you to easily access the value from other threads, since MutVal is `Send + Sync`.
pub struct Edit<E>{
    pub value: crate::io::Value<E>,
    text_field: Text,
    edit_changed: bool,
}

///Special wrapper that allows some anonym boxed value to be `E` in the edit.
pub struct BoxedValue(MutVal<dyn TextEditable + Send + Sync + 'static>);
impl TextEditable for BoxedValue{
    fn to_string(&self) -> String {
	self.0.get().to_string()
    }

    fn from_string(&mut self, string: &str) -> bool {
	self.0.get_mut().from_string(string)
    }
}

impl<E> Edit<E> where E: TextEditable + Send + Sync + 'static{
    ///Creates the edit with an `String` as value. If you want to use the shared value, add
    /// a `with_mut_val()` to your creation.

    ///Creates a new edit for some value type `E`. Use 
    pub fn new(val: E) -> Self{

	let val_text = Text::new().with_text(&val.to_string());
	Edit{
	    value: Value::Own(val),
	    text_field: val_text,
	    edit_changed: false,
	}
    }

    pub fn new_anonym(val: MutVal<dyn TextEditable + Send + Sync + 'static>) -> Edit<BoxedValue>{
	let val_text = Text::new().with_text(&val.get().to_string());
	Edit{
	    value: Value::Own(BoxedValue(val)),
	    text_field: val_text,
	    edit_changed: false,
	}	
    }

    pub fn with_mut_val<N>(self, val: MutVal<N>) -> Edit<N> where N: TextEditable + Send + Sync + 'static{
	let mut text_field = self.text_field;
	text_field.set_text(self.value.on_data(|v| v.to_string()));
	Edit{
	    value: Value::MutVal(val),
	    text_field,
	    edit_changed: self.edit_changed
	}
    }

    pub fn with_text_field(mut self, text: Text) -> Self{
	self.text_field = text.with_text(&self.value.on_data(|v| v.to_string()));
	self
    }

    pub fn get(&self) -> &Value<E>{
	&self.value
    }

    pub fn get_mut(&mut self) -> &mut Value<E>{
	&mut self.value
    }
}

impl<E> Widget for Edit<E> where E: TextEditable + Send + Sync + 'static{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	//If an edit command was send, switch switch, since we have to update the text val to the label, not the other way
	match event {
	    Event::WindowEvent(WindowEvent::ReceivedCharacter(_)) => self.edit_changed = true,
	    _ => {}
	}
	self.text_field.on_event(event, location)
    }
    ///Returns current occupied size
    fn area(&self) -> Area{
	self.text_field.area()
    }
    
    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{	
	//Update widgets text if out of sync
	if self.value.on_data(|v| v.to_string()) != self.text_field.get_text(){
	    
	    if self.edit_changed{
		self.edit_changed = false;
		match &mut self.value{
		    Value::MutVal(val) => {
			if val.get_mut().from_string(&self.text_field.get_text()) == false{
			    warn!("Failed to parse edit text back to type!");
			}	
		    },
		    Value::Own(own) => if !own.from_string(&self.text_field.get_text()){
			warn!("Failed to parse text!");
		    },
		}
	    }else{
		//So the mut val has changed, therefore set to mut val
		self.text_field.set_text(self.value.on_data(|v| v.to_string()));
	    }
	    
	}

	self.text_field.get_calls(host_extent, draw_time, interface, level)
    }
}
