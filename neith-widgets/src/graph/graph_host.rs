use std::time::Instant;

use neith::Interface;
use neith::config::Color;
use neith::event::Event;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith;

use neith::primitive::Lines;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;

use crate::graph::Graph;

pub const MAX_RES: f32 = 1.0;
pub const PIXEL_PER_SEGMENTS: Unit = 1.0;

///Can host one or more graphs
pub struct GraphHost{
    resolution: f32,
    area: Area,
    graph_area: Area,
    graphs: Vec<Box<dyn Graph + Send>>,
}

impl GraphHost{
    pub fn new() -> Self{
	GraphHost{
	    resolution: 1.0,
	    area: Area::empty(),
	    graph_area: Area::empty(),
	    graphs: Vec::new(),
	}
    }

    ///How many steps per pixel should be displayed. So 1 means one step per pixel, 0.5 is two pixel per step etc. Usually
    /// the graph looks smooth until ~0.1.
    ///
    ///Is capped to 1.0
    pub fn with_resolution(mut self, resolution: f32) -> Self{
	self.resolution = resolution.min(MAX_RES);
	self
    }

    pub fn with_graph<G>(mut self, graph: G) -> Self where G: Graph + Send + 'static{
	self.graphs.push(Box::new(graph));
	self
    }
}


impl Widget for GraphHost{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	for graph in &mut self.graphs{
	    let local_event = event.offset(self.graph_area.from * -1.0);
	    graph.on_event(&local_event, &(*location + self.graph_area.from * -1.0));
	}
    }

    fn area(&self) -> Area {
	self.area
    }

    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	mut level: usize
    ) -> DrawCalls {
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	//Draw background
	let mut prims = Vec::new();
	prims.push(
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(neith::get_styling().background),
		level
	    },
	);

	level += 1;

	let diagram_area = Area{
	    from: self.area.from + Vector2::new(10.0, 10.0),
	    to: self.area.to - Vector2::new(10.0, 10.0)
	};

	if diagram_area.from.x > diagram_area.to.x || diagram_area.from.y > diagram_area.to.y{
	    //Cannot draw diagram, therefore return already
	    return DrawCalls::new(prims);
	}

	self.graph_area = diagram_area;	
	
	level += 1;
	
	//Draw the diagrams axes
	prims.push(
	    Prim{
		prim: PrimType::Lines(Lines{
		    points: vec![
			diagram_area.from,
			Vector2::new(diagram_area.from.x, diagram_area.to.y),
			diagram_area.to
		    ],
		    width: 1.0,
		}),
	        drawing_area: self.area,
	        scissors: None,
	        style: Style::from_color(Color::Float([1.0, 1.0, 1.0])),
	        level,
	    }
	);

	//Draw the actual graph.	
	let mut calls = DrawCalls::new(prims);

	for graph in &mut self.graphs{
	    let mut gcalls = graph.get_calls(self.graph_area.extent(), draw_time, interface, level + 1);

	    gcalls.offset(self.graph_area.from);
	    
	    calls.merge(gcalls);
	}

	calls
    }
}
