///A widget that does not need to occupy any space by itself, but paints the space black which it does occupy. 
pub mod void;
pub use void::Void;

///Text primitive which can be manipulated through normal text edit operation.
pub mod text;
pub use text::*;
///The root widget of every interface. You can use it, but it is only a wrapper around some child widget
pub mod root_widget;
pub use root_widget::RootWidget;

///Takes a set of widgets and positions them vertically. Usually used together with a viewport to create a scrollable list in some way.
pub mod list_array;

///Draws a standard frame around its inner widget.
pub mod frame;
pub use frame::Frame;

///If some widget state within the tree needs to be changed at runtime,
/// use this as a cloneable wrapper.
pub mod arc_widget;
pub use arc_widget::ArcWidget;

///If a widget needs to be shared, use this one. It can be cloned,
/// but does not erase its childs typ.
pub mod shared_widget;
pub use shared_widget::*;
