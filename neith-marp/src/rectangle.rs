use neith::widget::Style;
use neith::math::Area;
use crate::pipelines::surface::{SurfacePipeline, SurfacePush};
use crate::vulkan_prelude::*;
use std::sync::Arc;

use crate::command_builder::Vertex;

///Rectangle primitive. Uses the `sruface` shader to draw some generic rectangle
pub struct Rectangle{
    pipeline: SurfacePipeline,
    vertex_buffer: Arc<Buffer>,
    index_buffer: Arc<Buffer>,
}


impl Rectangle{
    pub fn new(
	device: Arc<Device>,
	queue: Arc<Queue>,
	render_pass: Arc<RenderPass>,
	msaa_flag: vk::SampleCountFlags
    ) -> Self{
	let indices: Vec<u32> = vec![0, 1, 2, 2, 3, 0];
        let (index_upload, index_buffer) = Buffer::from_data(
            device.clone(),
            queue.clone(),
            indices.clone(), //The buffer size we need
            BufferUsage {
                index_buffer: true,
                storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        index_upload.wait(std::u64::MAX).expect("Failed to wait for index upload!");
        //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
        let vertices = vec![
            Vertex {
                pos: [0.0, 1.0, 0.0, 1.0],
                uv: [0.0, 1.0],
            },
            Vertex {
                pos: [1.0, 1.0, 0.0, 1.0],
                uv: [1.0, 1.0],
            },
            Vertex {
                pos: [1.0, 0.0, 0.0, 1.0],
                uv: [1.0, 0.0],
            },
            Vertex {
                pos: [0.0, 0.0, 0.0, 1.0],
                uv: [0.0, 0.0],
            },
        ];

        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            device.clone(),
            queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");

        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

	let pipeline = SurfacePipeline::new(device.clone(), render_pass, msaa_flag);

	Rectangle{
	    vertex_buffer,
	    index_buffer,
	    pipeline
	}
    }

    ///Draws a rectangle at `location` with given extent
    ///Screen area is the area in pixels that is covered by the screen.
    pub fn draw_at(
	&self,
	screen_area: Area,
	rectangles: &[(Area, Option<Area>, Style, f32)],
	command_buffer: Arc<CommandBuffer>
    ){
	
	let push_scissors: Vec<_> = rectangles.iter().map(|(area, scissors, style, level)|{
	    let transform = area.to_transform(&screen_area);
	    
	    let push_buf = SurfacePush{
		transform,
		color: style.color,
		power: style.power,
		level: *level,
		pad1: [0.0; 2]
	    };

	    let scissors_area = if let Some(sc) = scissors{
		sc
	    }else{
		area
	    };
	    
	    (push_buf, area_to_scissors(*scissors_area, screen_area))
	}).collect();

	self.pipeline.draw(
	    (6, self.index_buffer.clone()),
	    self.vertex_buffer.clone(),
	    &push_scissors,
	    command_buffer
	);
    }
}



