use neith::Interface;
use neith::base::Frame;
use neith::base::Text;
use neith::base::Void;
use neith::base::list_array::ListArray;
use neith::config::Color;
use neith::event::Event;
use neith::event::WindowEvent;
use neith::io::Edit;
use neith::io::Label;
use neith::io::edit::BoxedValue;
use neith::layout::Align;
use neith::layout::Direction;
use neith::layout::Floating;
use neith::layout::Split;
use neith::layout::split::SplitType;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::primitive::PolyHandle;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;

use std::cell::Ref;
use std::cell::RefCell;
use std::cell::RefMut;
use std::time::Instant;
use core::any::TypeId;
use super::node_content::*;

#[derive(Eq, PartialEq, Debug, Hash)]
pub enum ConnectionError{
    NoSuchInputIndex,
    ///Wrong type id for input, needs to be this one.
    WrongInputTypeId(TypeId),
    ///When some connection attempt failed
    ConnectionFailed,
    ///When disconnection of an edge failed
    DisconnectionFailed,
}



pub(crate) const FRAME_SIZE: Unit = 2.5;
///Additional space that is added to the handles of this node when searching for edges/corners
const HANDLE_ADD: Unit = 15.0;
pub(crate) const PORT_SIZE: Unit = 10.0;
pub(crate) const PORT_SPACE: Unit = 25.0;
pub(crate) const PORT_AREA: Unit = PORT_SIZE + PORT_SPACE;

pub(crate) const NODE_BAR_HEIGHT: Unit = 25.0;
pub(crate) const VIRT_AREA_REDUCE: Unit = PORT_SIZE - FRAME_SIZE;
pub(crate) const PORT_NAME_GROW: Unit = PORT_SPACE / 2.0;
pub(crate) const PORT_TEXT_SIZE: Unit = 16.0;

const NAME_PORT_IN: &'static str = "port_in";
const NAME_PORT_OUT: &'static str = "port_out";

pub struct Port{
    pub area: Area,
    pub color: Color,
    pub ty: TypeId,
}

///Generates the poly handle for an used input_port
fn poly_port_in() -> (Vec<Vector2<f32>>, Vec<u32>){
    crate::utils::load_polygon(include_bytes!("../../resources/node_handle.obj")).unwrap()
}

fn poly_port_out() -> (Vec<Vector2<f32>>, Vec<u32>){
    //Same but mirrored on the x axis
    let (mut vbuf, ibuf) = poly_port_in();
    for vert in vbuf.iter_mut(){
	vert.x = 1.0 - vert.x;
    }

    (vbuf, ibuf)
}

pub enum NodeLocation{
    Top(Vector2<Unit>),
    Corner,
    InPort(usize),
    OutPort(usize),
    Body,
    None,
}

pub struct NodeBox<I>{
    ///The virtual area that the `content` can occupy.
    virt_area: Area,
    area: Area,
    content: RefCell<I>,
    ///Layout of the nodes interface
    layout: Split,

    ///Possible color used for the frame
    frame_color: Option<Color>,
    
    ///Collection of the input and output port location
    in_ports: Vec<Port>,
    out_ports: Vec<Port>,

    ///PolyHandle for the in/out ports in used and unused state.
    port_in: Option<PolyHandle>,
    port_out: Option<PolyHandle>,

    ///Static min area based on the max of in/output ports for the height as well as 3*port_area_width for the width.
    min_extent: Vector2<Unit>
}

impl<I> NodeBox<I> where I: NodeContent<Node = I> + Send + 'static{
    pub fn new(mut content: I) -> Self{
	let inner_interface = content.get_interface();
	let title_text = if let Some(title) = content.get_title(){
	    Some(Label::new(&title).with_size(NODE_BAR_HEIGHT))
	}else{
	    None
	};
	//Generate initial port layout, locations area updated at each
	//draw calls since the node size might change.
	let (in_ports, in_ports_list): (Vec<Port>, ListArray) = content.inputs().into_iter().enumerate().fold(
	    (Vec::new(), ListArray::new(Direction::Vertical, vec![]).with_element_size(PORT_AREA)),
	     |(mut in_ports, mut port_widgets), (idx, (ty, name))|{
		 let new_port = Port{
		     area: Area::empty(),
		     color: Color::from_type_id(&ty),
		     ty,
		 };
		 in_ports.push(new_port);
		 //For each editable port, push title + edit field, for uneditable ports, just push the name
		 port_widgets.on_list(|list|list.push(
		     if let Some(default_value) = content.in_default_value(idx){
			 Box::new(
			     Floating::new()
				 .with_element(
				     Area{from: (0.0, 0.0).into(), to: (1.0, 0.5).into()},
				     Align::new(Label::new(&name).with_size(PORT_TEXT_SIZE))
				 ).with_element_boxed(
				     Area{from: (0.0, 0.5).into(), to: (1.0, 1.0).into()},
				     default_value
				 )
			 )
		     }else{
			 //Just the title
			 Box::new(Align::new(Label::new(&name).with_size(PORT_TEXT_SIZE)))
		     }
		 ));
		 (in_ports, port_widgets)
	    }   
	);
	
	let (out_ports, out_ports_list): (Vec<Port>, ListArray) = content.outputs().into_iter().enumerate().fold(
	    (Vec::new(), ListArray::new(Direction::Vertical, vec![]).with_element_size(PORT_AREA)),
	     |(mut out_ports, mut port_widgets), (idx, (ty, name))|{
		 let new_port = Port{
		     area: Area::empty(),
		     color: Color::from_type_id(&ty),
		     ty,
		     //name: Box::new(Text::new().with_text(&name).with_single_line().with_size(PORT_TEXT_SIZE)),
		 };

		 out_ports.push(new_port);

		 //For each editable port, push title + edit field, for uneditable ports, just push the name
		 port_widgets.on_list(|list|list.push(
		     if let Some(default_value) = content.out_default_value(idx){
			 Box::new(
			     Floating::new()
				 .with_element(
				     Area{from: (0.0, 0.0).into(), to: (1.0, 0.5).into()},
				     Align::new(Label::new(&name).with_size(PORT_TEXT_SIZE))
				 ).with_element_boxed(
				     Area{from: (0.0, 0.5).into(), to: (1.0, 1.0).into()},
				     default_value
				 )
			 )
		     }else{
			 //Just the title
			 Box::new(Align::new(Label::new(&name).with_size(PORT_TEXT_SIZE)))
		     }
		 ));
		 (out_ports, port_widgets)
	    }   
	);

	
	let middle_layout: Box<dyn Widget + Send + 'static> = match (inner_interface, title_text){
	    (Some(inner), Some(title)) => Box::new(
		Split::new()
		    .with_one(title)
		    .with_two_boxed(inner)
		    .with_direction(Direction::Horizontal)
		    .with_split_type(SplitType::Absolute(NODE_BAR_HEIGHT + 5.0))),
	    (Some(inner), None) => inner,
	    (None, Some(titel)) => Box::new(titel),
	    (None, None) => Box::new(Void::new())
	};

	
	let min_extent = out_ports_list.extent().max(in_ports_list.extent());
	
	//Build the virtual areas layout.
	//Depending on if there are in/out puts, create a layout for them

	let layout = match (in_ports.len() > 0, out_ports.len() > 0){
	    (true, true) => {
		Split::new()
		    .with_one(
			in_ports_list
		    ).with_two(
			Split::new()
			    .with_one_boxed(
				middle_layout
			    ).with_two(
				out_ports_list
			    ).with_direction(Direction::Vertical)
			    .with_split_type(SplitType::Absolute(50.0))
			    .with_inv_reference()
			    .with_moveable(true)
		    ).with_direction(Direction::Vertical)
		    .with_split_type(SplitType::Absolute(50.0))
		    .with_moveable(true)
	    },
	    (false, true) => {
		Split::new()
		    .with_one_boxed(
			middle_layout
		    ).with_two(
			out_ports_list
		    ).with_direction(Direction::Vertical)
		    .with_split_type(SplitType::Absolute(50.0))
		    .with_inv_reference()
		    .with_moveable(true)
	    },
	    (true, false) => {
		Split::new()
		    .with_one(
			in_ports_list
		    ).with_two_boxed(
			middle_layout
		    ).with_direction(Direction::Vertical)
		    .with_split_type(SplitType::Absolute(50.0))
		    .with_moveable(true)
	    },
	    (false, false) => {
		Split::new()
		    .with_one_boxed(middle_layout)
	    }
	};
	
	

	NodeBox{
	    virt_area: Area::empty(),
	    area: Area::empty(),
	    content: RefCell::new(content),
	    layout,

	    frame_color: None,
	    
	    in_ports,
	    out_ports,

	    port_in: None,
	    port_out: None,

	    min_extent: Vector2::new(
		3.0 * PORT_AREA + FRAME_SIZE * 2.0,
		min_extent + FRAME_SIZE * 2.0
	    )
	}
    }

    ///Returns the minimum area this node should be big. Otherwise the content might be glitchy.
    pub fn min_extent(&self) -> Vector2<Unit>{
	self.min_extent
    }
    
    pub(crate) fn content<'a>(&'a self) -> Ref<'a, I>{
	self.content.borrow()
    }

    pub(crate) fn content_mut<'a>(&'a self) -> RefMut<'a, I>{
	self.content.borrow_mut()
    }
    
    pub fn set_color(&mut self, color: Color){
	self.frame_color = Some(color);
    }
    /*
    ///Takes the interface out of this node
    pub fn take_interface(&mut self) -> Option<Box<dyn Widget + Send + 'static>>{
	self.inner_interface.take()
    }

    ///Sets the interface of this node
    pub fn set_interface(&mut self, interface: Box<dyn Widget + Send + 'static>){
	self.inner_interface = Some(interface);
    }
    */
    pub fn get_location_info(&self, location: &Vector2<Unit>) -> NodeLocation{

	let corner_area = Area{
	    from: (self.area.to.x - VIRT_AREA_REDUCE - HANDLE_ADD, self.area.to.y - HANDLE_ADD).into(),
	    to: (self.area.to.x - VIRT_AREA_REDUCE, self.area.to.y).into()
	};

	if corner_area.is_in(location){
	    return NodeLocation::Corner;
	}

	let body_area = Area{
	    from: (
		VIRT_AREA_REDUCE + FRAME_SIZE,
		FRAME_SIZE + NODE_BAR_HEIGHT
	    ).into(),
	    to: (
		self.area.to.x - (VIRT_AREA_REDUCE + FRAME_SIZE),
		self.area.to.y - FRAME_SIZE
	    ).into()
	};
	
	//check if this is only overlabbing the inner body
	if body_area.is_in(location){
	    return NodeLocation::Body;
	}
	
	//First of all check if it is overlapping any ports
	for (i, ip) in self.in_ports.iter().enumerate(){
	    if ip.area.is_in(location){
		return NodeLocation::InPort(i)
	    }
	}

	for (i, op) in self.out_ports.iter().enumerate(){
	    if op.area.is_in(location){
		return NodeLocation::OutPort(i)
	    }
	}
	let top_area = Area{
	    from: (VIRT_AREA_REDUCE, 0.0).into(),
	    to: (self.area.extent().x - VIRT_AREA_REDUCE, NODE_BAR_HEIGHT + FRAME_SIZE).into()
	};
	if top_area.is_in(location){
	    return NodeLocation::Top(*location);
	}

	
	NodeLocation::None
    }

    pub fn get_in_ports(&self) -> &[Port]{
	&self.in_ports
    }

    
    pub fn get_out_ports(&self) -> &[Port]{
	&self.out_ports
    }

    pub fn get_in_ports_mut(&mut self) -> &mut [Port]{
	&mut self.in_ports
    }
    
    pub fn get_out_ports_mut(&mut self) -> &mut [Port]{
	&mut self.out_ports
    }
    
    pub fn get_in_port_location(&self, index: usize) -> Option<Vector2<Unit>>{
	if let Some(p) = self.in_ports.get(index){
	    Some(p.area.from + p.area.extent() / Vector2::new(2.0, 2.0))
	}else{
	    None
	}
    }

    pub fn get_out_port_location(&self, index: usize) -> Option<Vector2<Unit>>{
	if let Some(p) = self.out_ports.get(index){
	    Some(p.area.from + p.area.extent() / Vector2::new(2.0, 2.0))
	}else{
	    None
	}
    }
    
    fn update_port_locations(&mut self){

	let thin_border = neith::get_config().border_slim as Unit;
	
	let mut start = FRAME_SIZE + thin_border + ((PORT_AREA / 2.0) - (PORT_SIZE / 2.0)); //Space from top for first port
	for port in &mut self.in_ports{
	    port.area = Area{
		from: (0.0, start).into(),
		to: (PORT_SIZE, start + PORT_SIZE).into()
	    };

	    start += PORT_AREA + thin_border;
	}
	
	let mut start = FRAME_SIZE + thin_border + ((PORT_AREA / 2.0) - (PORT_SIZE / 2.0)); //Space from top for first port
	for port in &mut self.out_ports{
	    port.area = Area{
		from: (self.area.to.x - PORT_SIZE, start).into(),
		to: (self.area.to.x, start + PORT_SIZE).into()
	    };

	    start += PORT_AREA + thin_border;
	}
	
    }
}

impl<I> Widget for NodeBox<I> where I: NodeContent<Node = I> + Send + 'static{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	let off = Vector2::new(-PORT_SIZE, FRAME_SIZE);    
	self.layout.on_event(
	    &event.offset(off),
	    &(*location + off)
	);
    }
    fn area(&self) -> Area {
        self.area
    }
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {

	//If needed load poly handles
	if self.port_in.is_none(){
	    let (vbuf, ibuf) = poly_port_in();
	    self.port_in = Some(interface.get_backend().register_polygone(NAME_PORT_IN, vbuf, ibuf));
	}
	if self.port_out.is_none(){
	    let (vbuf, ibuf) = poly_port_out();
	    self.port_out = Some(interface.get_backend().register_polygone(NAME_PORT_OUT, vbuf, ibuf));
	}
	
	
	
        self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	self.virt_area = Area{
	    from: (VIRT_AREA_REDUCE + FRAME_SIZE, FRAME_SIZE).into(),
	    to: (self.area.to.x - VIRT_AREA_REDUCE - FRAME_SIZE, self.area.to.y - FRAME_SIZE).into()
	};

	self.update_port_locations();

	let mut calls = DrawCalls::new(vec![
	    //Node Frame background
	    Prim{
		prim: PrimType::Rect,
		drawing_area: Area{
		    from: (VIRT_AREA_REDUCE, 0.0).into(),
		    to: (self.area.to.x - VIRT_AREA_REDUCE, self.area.to.y).into()
		},
		scissors: None,
		style: if let Some(fc) = &self.frame_color{
		    Style::from_color(fc.clone())
		}else{
		    Style::from_color(neith::get_styling().background_sec_selected)
		},
		level: level
	    },
	    //Virtual area background
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.virt_area,
		scissors: None,
		style: Style::from_color(neith::get_styling().background),
		level: level + 1
	    }
	]);
	
	//Add inner child to calls if there is an interface
	let mut child_calls = self.layout.get_calls(
	    self.virt_area.extent(),
	    draw_time,
	    interface,
	    level + 2
	);
	child_calls.clip_to(self.virt_area.extent());
	child_calls.offset(self.virt_area.from);
	calls.merge(child_calls);
	

	//Add line in lower right corner to mark the drag handle
	let max_lvl = calls.max_level();

	//Draw ports over frame as  well as their names
	calls.add(self.in_ports.iter().enumerate().map(|(idx, p)|Prim{
	    prim: PrimType::Polygon(self.port_in.as_ref().unwrap().clone()),
	    drawing_area: p.area,
	    scissors: None,
	    style: Style::from_color(p.color.clone()),
	    level: max_lvl + 2
	}).collect());

	calls.add(self.out_ports.iter().map(|p|Prim{
	    prim: PrimType::Polygon(self.port_out.as_ref().unwrap().clone()),
	    drawing_area: p.area,
	    scissors: None,
	    style: Style::from_color(p.color.clone()),
	    level: max_lvl + 2
	}).collect());

	calls
    }
}
