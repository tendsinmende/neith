//! # Neith
//! Neith is mostly a layouting / draw call generation framework. Together with a backend like `neith-marp` it becomes a interface
//! toolkit.
//!
//! ## Structure
//! The library is build around a tree of `dyn Widget`s. This tree can be asked for a set of `DrawCalls`
//! that a renderer needs to display. Input events are given to the tree which evaluates them to possible state changes. 
//! 
//! ## Widgets
//! The most common layouting widgets as well as io widgets are implemented, this contains:
//! - layouting
//!   - horizontal / vertical split
//!   - viewport (provides an area bigger than its host area as well as adaptive scroll bars to navigate)
//!   - lists
//!   - cell_layout / table
//!   - alignment (rules based alignment of some child within the host)
//! - io
//!   - button
//!   - slider
//!   - edit
//!   - label
//!   - generic text (can be edited but also used to implement some custom text based widget like number edit, or date edit)
//!
//!
//! ## Custom widgets
//! At the core of this library is the idea, that it should be easy to implement custom widgets. For that to work, you only 
//! have to implement the `Widget` trait on some type that should be an widget.
//!
//! the `on_event()` function gets called whenever an event happens that concerns your widget. All coordinates are relative to the
//! top left of your widget. If you use sub-widgets within your new widget, you'll have to take care of pushing down those events and possibly offsetting the to the right location.
//!
//! The `get_calls()` function gets called whenever a renderer requests the current layout of the interface. Usually you'll update
//! your inner area size with the provided `host_extent`, then get the calls of possible sub_widgets, and finally add your own primitives like lines, font characters, rectangles etc. to the DrawCalls.
//!
//! Have a look at the already implemented widget like `button.rs` or `label.rs` to get an idea how that works.
//!
//!
//! # Node graphs
//! A special feature of this toolkit is, that it contains a type checking node graph system. However usage of this is a bit more
//! complicated, have a look at the `node` module for more information.

extern crate lazy_static;
///Used for config saveing and loading
extern crate ron;
extern crate serde;
extern crate log;


///Defines all layout related widgets
pub mod layout;
///Defines all basic widgets needed to build more sophisticated UIs.
pub mod base;

///Defines standart input/output related widgets, like buttons, edits and labels
pub mod io;

///configuration properties for the interface toolkit
pub mod config;
///Communication primitives between parts of the toolkit.
pub mod com;
///Describes all mathematical primitives of the library
pub mod math;
///Generic Widget description
pub mod widget;
pub mod event;
pub mod primitive;


use config::Config;
use config::Styling;
use event::CursorIcon;
use event::Event;
use event::KeyState;
use event::MouseButton;
use event::WindowEvent;
use math::Vector2;
use primitive::PolyHandle;
use widget::Widget;

use crate::math::Area;
use crate::math::Unit;
use crate::primitive::Character;
use crate::primitive::Prim;
use crate::widget::Style;

use crate::base::root_widget::RootWidget;

use std::collections::HashSet;
use std::sync::RwLock;

use std::time::Instant;
use std::sync::Arc;

pub trait Input{
    fn get_events(&self) -> Vec<Event>;
    fn add_event(&self, event: Event);
    ///When called, the input provider should stop working.
    fn should_end(&self);
}

pub trait Backend{
    fn get_rendering_area(&self) -> Area;
    fn get_character_area(&self, ch: &Character) -> Area;
    fn draw_primitives(&mut self, primitives: Vec<Prim>);
    fn set_cursor(&mut self, cursor: CursorIcon);
    ///Registers a new polygone on the backend. Expects a set of vertices in 2D space and a set of indices. Every chunk of 3 indices make up one triangle. so `indices.len() % 3 == 0` should be given.
    /// # Note on locations
    /// The vertices should be defined in the space of [-1, 1]. When drawing a polygone, the vertices will be transformed to the area of the draw call.
    fn register_polygone(&self, name: &'static str, vertices: Vec<Vector2<f32>>, indices: Vec<u32>) -> PolyHandle;
}


lazy_static::lazy_static!{
    static ref CONFIG: RwLock<Config> = RwLock::new(Config::default());
}

///Returns a reference to the currently correct styling information
pub fn get_styling() -> Styling{
    CONFIG.read().expect("Failed to get styling info").styling.clone()
}

///Returns a copy of the currently correct configuration
pub fn get_config() -> Config{
    CONFIG.read().expect("Failed to get config").clone()
}

///Reloads the configuration file, assuming that the path in `config.path` is correct. Otherwise the default configuration
///will be loaded.
pub fn reload_config(){
    *CONFIG.write().expect("Failed to reload configuration") = Config::load(&get_config().path);
}

///Overwrites the current configuration with the given one
pub fn overwrite_config(new_config: Config){
    *CONFIG.write().expect("Failed to overwrite config") = new_config;
}

struct Cursor{
    location: (Unit, Unit),
    left_pressed: bool,
    right_pressed: bool,

    //While the cursor is draging something, this is "Some"
    drag_start: Option<(Unit, Unit)>,
    icon: RwLock<CursorIcon>,
}

impl Cursor{
    fn get_events(&self) -> Vec<Event>{

	let mut events = Vec::new();
	//Currently this can only emmitt the drag event.
	if let Some(start) =  self.drag_start{
	    events.push(Event::WindowEvent(WindowEvent::CursorDrag{start, end: self.location}));
	}

	events
    }
}

pub struct Interface{    
    ///The inner backend used for rendering
    backend: Box<dyn Backend + Send + Sync>,

    ///Input provider
    input: Box<dyn Input + Send + Sync>,

    ///Last known location of the cursor.
    cursor: Cursor,
    
    ///The area in which all widgets and overlays have to be drawn.
    area: Area,
    
    ///Root of the main interface tree
    root: Arc<RwLock<RootWidget>>,
    ///All currently active overlays. Stuff like dialogues of popup widgets.
    overlays: RwLock<HashSet<usize>>,

    ///Tracks if the icon cursor has changed, if so, on next update change we be signaled to backend
    cursor_changed: RwLock<bool>,
}

impl Interface{
    ///Creates a new instance based on a backend and input provider. If given a config file is loaded, otherwise the default config is used.
    pub fn new<B,I>(backend: B, input: I, config_path: Option<&'static str>) -> Self where B: Backend  + Send + Sync + 'static, I: Input + Send + Sync + 'static{

	if let Some(c_path) = config_path{
	    *CONFIG.write().expect("Failed to set config to provided one") = Config::load(c_path);
	}

	let root = Arc::new(RwLock::new(RootWidget::new()));
	Interface{

	    input: Box::new(input),
	    
	    area: backend.get_rendering_area(),

	    cursor: Cursor{
		location: (0.0, 0.0),
		left_pressed: false,
		right_pressed: false,
		drag_start: None,
		icon: RwLock::new(CursorIcon::Default)
	    },

	    root,
	    
	    overlays: RwLock::new(HashSet::new()),

	    backend: Box::new(backend),
	    cursor_changed: RwLock::new(false),
	}
    }

    pub fn get_root(&self) -> Arc<RwLock<RootWidget>>{
	self.root.clone()
    }
    
    pub fn get_backend(&self) -> &Box<dyn Backend + Send + Sync>{
	&self.backend
    }

    pub fn get_backend_mut(&mut self) -> &mut Box<dyn Backend + Send + Sync>{
	&mut self.backend
    }

    pub fn get_input(&self) -> &Box<dyn Input + Send + Sync>{
	&self.input
    }

    pub fn get_input_mut(&mut self) -> &mut Box<dyn Input + Send + Sync>{
	&mut self.input
    }

    pub fn root(&self) -> &Arc<RwLock<RootWidget>>{
	&self.root
    }

    pub fn get_cursor_icon(&self) -> CursorIcon{
	*self.cursor.icon.read().unwrap()
    }
    
    pub fn set_cursor_icon(&self, new: CursorIcon){
	*self.cursor_changed.write().unwrap() = true;
	*self.cursor.icon.write().unwrap() = new;
    }

    ///Registers an overlay that will be rendered over everything else, returns the id of this overlay.
    /// The id must be used to remove the overlay when its not active anymore.
    pub fn register_overlay(&self) -> usize{
	let mut overlay_lock = self.overlays.write().unwrap();
	let mut id = 0;
	while overlay_lock.contains(&id){
	    id += 1;
	}

	overlay_lock.insert(id);
	id
    }
    
    pub fn remove_overlay(&self, id: usize){
	self.overlays.write().unwrap().remove(&id);
    }
    
    ///Updates the interface based on all received events since the last update
    ///Returns true if the runner should quit updating the interface
    pub fn update(&mut self) -> bool{

	//Check if we have got to change the cursor icon
	if *self.cursor_changed.read().unwrap(){
	    *self.cursor_changed.write().unwrap() = false;
	    self.backend.set_cursor(*self.cursor.icon.read().unwrap());
	}	
	
	let events = self.input.get_events();
	let mut schedule_redraw = false;
	
	for e in events{
	    match &e{
		event::Event::WindowEvent(ev) => match ev{
		    event::WindowEvent::Resized(new_area) => {
			self.area = new_area.clone();
			schedule_redraw = true;
		    },
		    event::WindowEvent::MouseClick{button,state} => {

			//Cursor drag catch
			match (button, state){
			    (MouseButton::Left, KeyState::Pressed) => {
				//Started a drag event, key the location within the cursor
				self.cursor.drag_start = Some(self.cursor.location);
				self.cursor.left_pressed = true;
			    },
			    (MouseButton::Left, KeyState::Released) => {
				//end a drag event
				self.cursor.drag_start = None;
				self.cursor.left_pressed = false;
			    },
			    (MouseButton::Right, KeyState::Pressed) => self.cursor.right_pressed = true,
			    (MouseButton::Right, KeyState::Released) => self.cursor.right_pressed = true,
			    _ => {} //could add drag events for rightclick as well or something.
			}
			
		    },
		    event::WindowEvent::CursorMoved(new_loc) =>	self.cursor.location = (new_loc.0 as Unit, new_loc.1 as Unit),
		    _ => (),
		},
		event::Event::MetaEvent(meta) => match meta{
		    event::MetaEvent::Redraw => schedule_redraw = true,
		    event::MetaEvent::Destroyed | event::MetaEvent::CloseRequest =>{
			//return therefore signaling that the host should end.
			self.input.should_end();
			return true
		    },
		    _ => (),
		},
		_ => ()
	    }

	    //After matching, push the events down the tree
	    self.root().write().expect("Failed to lock toolkits root!").on_event(&e, &self.cursor.location.into());
	}

	//Now push all generated events for the cursor down the tree
	for e in self.cursor.get_events(){
	    self.root().write().expect("Failed to lock toolkits root!").on_event(&e, &self.cursor.location.into());
	}

	if schedule_redraw{
	    let redraw_time = Instant::now();
	    //Collect main window Primitives
	    let mut prims = self.root.write().expect("Failed to get roots primitives")
		.get_calls(self.backend.get_rendering_area().extent(), redraw_time, &self, 0);

	    let mut max_lvl = prims.max_level();
	    //Offset the overlay primitives over the interface
	    for (_k, oprims) in prims.overlay_prims.iter_mut(){
		let this_max_level = oprims.iter().fold(0, |max, this| max.max(this.level));
		//Offset prims over current max level, the add levels of this overlay for the next
		for p in oprims.iter_mut(){
		    p.level += max_lvl;
		}
		
		max_lvl += this_max_level;
	    }

	    //Now merge overlay prims and normal prims
	    let mut all_prims = prims.primitives;
	    for (_k, mut oprims) in prims.overlay_prims.into_iter(){
		all_prims.append(&mut oprims);
	    }
	    
	    self.backend.draw_primitives(all_prims);
	}
	false
    }
}
