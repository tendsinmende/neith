extern crate lazy_static;
extern crate neith;
extern crate neith_widgets;
use neith::com::MutVal;
use neith::config::Color;
use neith::io::Edit;
use neith::io::Label;
use neith::layout::HorizontalAlign;
use neith::layout::VerticalAlign;
use neith::math::Area;
use neith::math::Vector2;
use neith::widget::Style;
use neith::widget::Widget;
use neith::{
    layout::split::*,
    layout::Direction,
};

extern crate neith_marp;
use neith_widgets::graph::SplineEditor;
use neith_widgets::utility::Logger;


use std::sync::{Arc, RwLock};

mod test_nodes;

fn widget_list() -> Vec<Box<dyn Widget + Send>>{
    let elements = vec![
	"- I Could Be a Star Now",
	"- Catholic Girls",
	"- Bobby Brown Goes Down",
	"- You are what you is",
	"- We Are not alone",
	"- Cheap Thrills",
	"- The Mudshark interview",
	"- Hot Plate at the Green Hotel",
	"- Zomby Woof",
	"- The Torture Never Stops",
	"- Joe's Garage",
	"- My Guitar wants to kill your Mama",
	"- Going for the Money"
    ];

    elements.into_iter().map(|e|{
	let item = Box::new(
		neith::base::Text::new()
		    .with_text(e)
		    .with_single_line()
		    .with_size(42.0)
	);
	let cast: Box<dyn Widget + Send> = item;
	cast
    }).collect()
}


fn main() {
    neith_marp::new_interface(None, None, |mut interface|{

	let should_end = Arc::new(RwLock::new(false));
	let se_cp = should_end.clone();

	let text_val = MutVal::new("Some Nice Text".to_owned());
	let edit_val = MutVal::new(4.0 as f32);
	let num_val: MutVal<isize> = MutVal::new(10);


	let arc_widget = neith::base::ArcWidget::new(neith::io::Label::new("Click the button!"));
	let ac = arc_widget.clone();
	let mut exchange_widget: Box<dyn Widget + Send + 'static> = Box::new(neith::io::Label::new("Yay some other widget, click it again...!"));
	
	let text_widget = neith::base::Text::new()
	    .with_text("This is some crazy nice multi line text I wrote here. Look evän some noiß ŝpeciälê€s characters. Everything is rendered with 8x Msaa. And now a line break:\nText rendering uses rust-type and lion to generate \"perfect\" glyphs that are sharp at any resolution.")
	    .with_style(
		Style::from_color(Color::Hex("0facff".to_owned()))
		    .with_power(10.0))
	    .with_size(42.0)
            .with_line_space(25.0);

	let mut_graph = MutVal::new(vec![(0.0, 0.0), (0.2, 0.2), (0.4, 0.7), (0.6, 0.6), (0.8, 0.2), (1.0, 0.0)]);
	
	let split = neith::layout::Split::new()
	    .with_direction(Direction::Horizontal)
	    .with_moveable(true)
	    .with_one(
		neith::layout::Split::new()
		    .with_direction(Direction::Vertical)
		    .with_one(
			neith::layout::Split::new()
			    .with_one(
				neith::layout::Align::new(
				    neith::base::Void::new()
				).with_horizontal_alignment(neith::layout::HorizontalAlign::Left)
				    .with_vertical_alignment(neith::layout::VerticalAlign::Top)
			    ).with_two(
				/*
				neith::layout::Split::new()
				    .with_one(
					neith_widgets::graph::GraphHost::new()
					    .with_graph(
						neith_widgets::graph::LineGraph::new()
						    .with_mut_val(mut_graph.clone())
					    )
				    ).with_two(
					*/
					neith_widgets::graph::GraphHost::new()
					    .with_graph(
						neith_widgets::graph::SplineEditor::new((-1.0, -1.0), (1.0, 1.0))
						    .with_free_keys(vec![(-0.5, 0.1), (-0.2, 0.3), (0.3, 0.2), (0.4, 0.3), (0.5, 0.2)])
						    .with_protected_keys(vec![(-1.0, 0.0), (0.35, 0.1), (1.0, 0.0)])
					    )
				    //)
			    ).with_direction(Direction::Horizontal)
			    .with_moveable(true)
		    ).with_two(
			neith::layout::Split::new()
			    .with_one(
				test_nodes::WrapCanvas::new()
			    ).with_two(
				neith::layout::Split::new()
				    .with_one(

					Logger::new()
					/*
					neith::layout::Align::new(
					    neith::io::DropDown::new()
						.with_elements(vec!["Teddy","Bär","War","Hier","Und", "Trank", "Bier", "Aus", "Einem", "Krug"])
						.with_on_selection_changed(|_list, element| println!("Selected: {}", element))
					).with_child_size(Vector2::new(300.0, 30.0))
					*/
				    ).with_two(
					neith::layout::StaticList::new(Direction::Horizontal)
					    .with_items(
						vec![
						    Box::new(
							neith::io::Edit::new(20)
							    .with_mut_val(num_val.clone())
						    ),
						    Box::new(
							neith::io::Edit::new(20.0)
							    .with_mut_val(edit_val.clone())
						    ),
						]
					    )
				    )
			    ).with_moveable(true)
		    ).with_split_type(SplitType::Relative(0.33))
		    .with_moveable(true)
	    ).with_two(
		neith::layout::Split::new()
		    .with_direction(Direction::Vertical)
		    .with_one(
			neith::layout::Viewport::new(text_widget)
		    ).with_two(
			neith::layout::Split::new()
			    .with_direction(Direction::Horizontal)
			    .with_one(
				neith::layout::List::new(Direction::Vertical)
				    .with_items(widget_list())
				    .with_element_size(50.0)
			    ).with_two(
				neith::layout::Align::new(
				    neith::layout::StaticList::new(Direction::Horizontal)
					.with_items(vec![
					    Box::new(
						neith::layout::Align::new(
						    neith::io::Button::new("end this misery")
							.with_action(move ||*should_end.write().unwrap() = true)
						)
					    ),
					    Box::new(
						neith::layout::Align::new(
						    neith::io::Button::new("Press me")
							.with_action(move ||{
							    ac.exchange(&mut exchange_widget);
							})
						)
					    ),
					    Box::new(
						neith::layout::Floating::new()
						    .with_element(
							Area{from: (0.1, 0.2).into(), to: (0.8, 0.5).into()},
							Label::new("Hi there")
						    ).with_element(
							Area{from: (0.5, 0.6).into(), to: (0.95, 1.0).into()},
							Edit::new("Oi I'm floating".to_owned())
						    ).with_element(
							Area{from: (0.1, 0.6).into(), to: (0.5, 0.9).into()},
							arc_widget
						    )
					    )
					])
					
				).with_vertical_alignment(VerticalAlign::Center)
				    .with_horizontal_alignment(HorizontalAlign::Center)
			    )
		    )
	    );
	//Setup standard interface
	let root = interface.root().clone();
	root.write().expect("Could not lock root").set_widget(split);


	let start = std::time::Instant::now();
	let mut last_add = std::time::Instant::now();
	while !interface.update(){

	    if last_add.elapsed() > std::time::Duration::from_secs_f32(0.5){
		mut_graph.get_mut().push((start.elapsed().as_secs_f32(), start.elapsed().as_secs_f32().sin() * 2.0));
		last_add = std::time::Instant::now();
	    }

	    
	    let power = ((start.elapsed().as_secs_f32() * 3.0).sin() + 1.0) * 10.0;
	    *text_val.get_mut() = format!("text glow power {} after {}sec. NumVal is: {}", power, start.elapsed().as_secs(), num_val.get());

	    if *se_cp.read().unwrap(){
		interface.get_input().should_end();
	    }
	}
    });
}
