pub mod cell_layout;

pub mod split;
pub use split::Split;

pub mod viewport;
pub use viewport::Viewport;

pub mod list;
pub use list::List;

pub mod static_list;
pub use static_list::*;

///Aligns some child within its area.
///Good to fit for instance a text to the center of some area
pub mod align;
pub use align::{
    Align,
    HorizontalAlign,
    VerticalAlign
};

pub enum Direction{
    Horizontal,
    Vertical
}


///Allows placing several widgets "somewhere" within this area.
///Can be used to creates custom layouts quickly.
pub mod floating;
pub use floating::Floating;

