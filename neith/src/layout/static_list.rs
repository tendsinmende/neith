
use std::time::Instant;
use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use super::Direction;

///Conatains a list of elements. Contrary to `List` it is not embedded into a viewport. Therefore
/// each Element gets the same amount of space.
pub struct StaticList{
    list_array: Vec<(Area, Box<dyn Widget + Send>)>,
    direction: Direction,
    area: Area,
}

impl StaticList{
    pub fn new(dir: Direction) -> Self{
	

	StaticList{
	    list_array: Vec::new(),
	    direction: dir,
	    area: Area::empty()
	}
    }

    pub fn with_items(mut self, items: Vec<Box<dyn Widget + Send>>) -> Self{
	self.list_array = items.into_iter().map(|i| (Area::empty(), i)).collect();
	self
    }

    pub fn push(mut self, item: Box<dyn Widget + Send>) -> Self{
	self.list_array.push((Area::empty(), item));
	self
    }

    ///Executes something on the internal list of widgets
    pub fn on_list<F>(&mut self, mut f: F) where F: FnMut(&mut Vec<(Area, Box<dyn Widget + Send>)>){
	f(&mut self.list_array);
    }

    pub fn add_arc(&mut self, item: Box<dyn Widget + Send>){
	self.list_array.push((Area::empty(), item))
    }

    pub fn add<I>(&mut self, item: I) where I: Widget + Send + 'static{
	self.list_array.push((Area::empty(), Box::new(item)));
    }

    fn update_offset(&mut self){

	let border = crate::get_config().border_slim as Unit;
	
	match self.direction{
	    Direction::Horizontal => {
		let mut off = Vector2::new(border, border);
		let width_per = (self.area.extent().x - ((self.list_array.len() + 1) as Unit * border)) / self.list_array.len() as Unit;
		for (area, _i) in &mut self.list_array{
		    *area = Area{
			from: off,
			to: Vector2::new(off.x + width_per, self.area.to.y - border)
		    };
		    off = off + Vector2::new(width_per + border, 0.0);
		}
	    },
	    Direction::Vertical => {
		let mut off = Vector2::new(border, border);
		let height_per = (self.area.extent().y - ((self.list_array.len() + 1) as Unit * border)) / self.list_array.len() as Unit;
		for (area, _i) in &mut self.list_array{
		    *area = Area{
			from: off,
			to: Vector2::new(self.area.to.x - border, off.y + height_per)
		    };
		    off = off + Vector2::new(0.0, height_per + border);
		}
	    }
	}
    }
}


impl Widget for StaticList{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	if self.area.is_in(location){
	    for (off, i) in &mut self.list_array{
		i.on_event(&event.offset(off.from * -1.0), &(*location - off.from))
	    }
	}
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	let mut calls = DrawCalls::new(vec![]);

	calls.add(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background_sec),
		level: level
	    }
	]);

	self.update_offset();
	for (area, i) in &mut self.list_array{
	    calls.add(vec![
		Prim{
		    prim: PrimType::Rect,
		    drawing_area: *area,
		    scissors: None,
		    style: Style::from_color(crate::get_styling().background),
		    level: level + 1
		}
	    ]);

	    let mut call = i.get_calls(
		area.extent(),
		draw_time,
		interface,
		level + 2
	    );


	    call.clip_to(area.extent());
	    call.offset(area.from);	    
	    calls.merge(call);
	}

	calls
    }
}
