//! Layout mechanics to bed some `String` within an area.
use log::warn;
use std::sync::RwLock;

use crate::Interface;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Character;
use crate::primitive::FontName;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;

///Defines the justification of some text layout within its area.
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
pub enum Justification{
    Left,
    Right,
    Center
}

enum Cursor{
    Loc(usize),
    Range(usize, usize),
    None
}

#[allow(dead_code)]
enum Block{
    LineBreak,
    Tab,
    Header(String, usize),
    Generic(String),
    Code(String),
    Bold(String),
    Cursive(String)
}

#[allow(dead_code)]
enum ParseInfo{
    Heading(usize),
    Bold,
    Cursive,
    Normal,
}

impl Block{
    fn parse_string(string: String, _size: f32, _extent: Unit) -> Vec<Self>{
        //Split blocks at line breaks.			  
	let mut blocks = string.rsplit('\n').rev().fold(Vec::new(), |mut blocks, block|{
            blocks.push(Block::Generic(block.to_string()));
            blocks.push(Block::LineBreak);
            blocks
        });
        //Remove last line break
        if let Some(Block::LineBreak) = blocks.last(){
            let _last_line_break = blocks.pop().unwrap();
            blocks
        }else{
            blocks
        }
    }

    //Returns how many chars this block has
    fn get_length(&self) -> usize{
	match self{
	    Block::Header(st, _) => st.chars().count(),
	    Block::Generic(st) => st.chars().count(),
	    Block::Bold(st) => st.chars().count(),
	    Block::Cursive(st) => st.chars().count(),
	    _ => 1
	}
    }

    ///True if this blocks type contains text
    fn is_textual(&self) -> bool{
	match self{
	    Block::Header(_, _) => true,
	    Block::Generic(_) => true,
	    Block::Code(_) => true,
	    Block::Bold(_) => true,
	    Block::Cursive(_) => true,
	    _ => false,
	}
    }

    fn insert(&mut self, ch: char, location: usize){
	match self{
	    Block::Header(org_str, _) => insert_into_string(org_str, location, ch),
	    Block::Generic(org_str) => insert_into_string(org_str, location, ch),
	    Block::Code(org_str) => insert_into_string(org_str, location, ch),
	    Block::Bold(org_str) => insert_into_string(org_str, location, ch),
	    Block::Cursive(org_str) => insert_into_string(org_str, location, ch),
	    _ => warn!("Tried to insert into non text block!"),
	}
    }

    ///Removes the char at `location`. However does not detect formating changes.
    ///TODO fix and use instead of reparsing the whole layout everytime.
    #[allow(dead_code)]
    fn remove(&mut self, location: usize){
	match self{
	    Block::Header(org_str, _) => remove_from_string(org_str, location),
	    Block::Generic(org_str) => remove_from_string(org_str, location),
	    Block::Code(org_str) => remove_from_string(org_str, location),
	    Block::Bold(org_str) => remove_from_string(org_str, location),
	    Block::Cursive(org_str) => remove_from_string(org_str, location),
	    _ => warn!("Tried to insert into non text block!"),
	}	
    }

    fn print(&self){
	match self{
	    Block::Header(h, s) => println!("HEADER[{}]: {}", s, h),
	    Block::Generic(g) => println!("GENERIC: {}", g),
	    Block::Code(c) => println!("CODE: {}", c),
	    Block::Bold(b) => println!("BOLD: {}", b),
	    Block::Cursive(c) => println!("CURSIVE: {}", c),
	    Block::LineBreak => println!("LINEBREAK"),
	    Block::Tab => println!("TAB"),
	}
    }
}

///The final layout that can be queried for the primitives to draw. Possibly in a sub region.
///
/// Currently the layout only supports string written from left to right, like Latin or English. However it would be possible
/// to include right to left or other systems as well.
///
/// The layouting supports a subset of markdown. The following features are supported:
///
/// - Header (lvl 1,2,3 via #, ##, ###)
/// - Bold writing (via ** ... **)
/// - inline code (via \` ... \`)
/// - cursive (via * ... *)
///
/// All other formatting can be handled by the user via line breaks and tabulators / spaces.
pub struct Layout{
    cursor: Cursor,
    pub text_size: Unit,
    pub line_space: Unit,
    ///The width for which the layout is currently generated.
    pub extent: Unit,
    ///The last area the layout had to occupy within the configured extent.
    pub last_area: Area,
    ///Drawcalls needed to draw the current layout
    calls: DrawCalls,

    pub justification: Justification,
    pub is_single_line: bool,
    ///Positional layout of the current calls
    layout: Vec<(Area, char)>,
    ///A block of text. A new block is started every time formatting information changes.
    blocks: Vec<Block>,
    ///Cached representation of this layouts text content as string. Might be none if something changed and no one requested a string,
    ///since the collection of this string might be expensive.
    string_cache: RwLock<Option<String>>,
}

impl Layout{
    pub fn new(string: String, text_size: Unit, extent: Unit, line_space: Unit) -> Self{
	let blocks = Block::parse_string(string, text_size, extent);
	let pre_layout = Layout{
	    cursor: Cursor::None,
	    justification: Justification::Left,
	    is_single_line: false,
	    blocks,
	    text_size,
	    line_space,
	    extent,
            last_area: Area::empty(),
            layout: vec![],
	    calls: DrawCalls::new(vec![]),
	    string_cache: RwLock::new(None),
	};

	pre_layout
    }

    ///Builds a string from the current content.
    /// TODO cache string when build once?
    pub fn as_string(&self) -> String{

	if let Some(cache) = self.string_cache.read().unwrap().as_ref(){
	    return cache.clone();
	}

	let mut string = String::from("");
        for b in &self.blocks{
            match b{
                Block::Generic(t) => string.push_str(&t),
                Block::LineBreak => string.push('\n'),
                _ => println!("ignored while building string!"),
            }
        }

	*self.string_cache.write().unwrap() = Some(string.clone());
        string
    }
    
    pub fn update_calls(&mut self, interface: &Interface){
	let mut new_calls = DrawCalls::new(vec![]);
        let mut new_layout = vec![];
	let mut last_location = Vector2::new(0.0, 0.0);
	let mut maxes: Vector2<Unit> = Vector2::new(0.0, 0.0);
	for block in &self.blocks{
	    match block{
		Block::LineBreak => {
		    last_location = Vector2::new(0.0, last_location.y + self.text_size + self.line_space);
		    maxes.x = maxes.x.max(last_location.x);
		    maxes.y = maxes.y.max(last_location.y);
		},
		Block::Tab => last_location.x += 4.0 * self.text_size, //Move to the right
		Block::Generic(text) => {
                    new_calls.merge(layout(
                        &text,
                        self.extent,
                        &mut last_location,
			&mut maxes,
                        self.text_size,
                        self.line_space,
			self.is_single_line,
                        interface,
                        &mut new_layout
                    ));
		},
                _ => println!("Ignoring Block!"),
	    }
	    
	    //Sanitize location anyways
	    if last_location.x > self.extent{
		last_location = Vector2::new(0.0, last_location.y + self.text_size + self.line_space);
		maxes.x = maxes.x.max(last_location.x);
		maxes.y = maxes.y.max(last_location.y);
	    }
	}
        self.last_area = Area{from: (0.0, 0.0).into(), to: maxes};
        self.layout = new_layout;
        self.calls = new_calls;
    }

    fn clean_insert(&mut self, character: char, loc: usize){

	//Remove cached string since it might become invalid
	*self.string_cache.write().unwrap() = None;
	
        let mut last_end = 0;
        //If blocks need to be exchanged, this is some.
        let mut exchange_info: Option<(usize, Vec<Block>)> = None;

	let mut did_insert = false;
	
	for (b_idx, b) in self.blocks.iter_mut().enumerate(){
	    if (b.get_length() + last_end) > loc{
		
		b.insert(character, loc - last_end);

                //parse back the block, if we got a new controlling sequence, insert it. otherwise leave it be.
                let new_blocks = match b{
                    Block::Generic(gen_str) => Block::parse_string(gen_str.to_string(), self.text_size, self.extent),
                    Block::Code(gen_str) => Block::parse_string(gen_str.to_string(), self.text_size, self.extent),
                    Block::Bold(gen_str) => Block::parse_string(gen_str.to_string(), self.text_size, self.extent),
                    Block::Cursive(gen_str) => Block::parse_string(gen_str.to_string(), self.text_size, self.extent),
                    Block::Header(gen_str, _title_level) => Block::parse_string(gen_str.to_string(), self.text_size, self.extent),
                    _ => vec![],
                };

                if new_blocks.len() > 1{
                    exchange_info = Some((b_idx, new_blocks));
                }
		did_insert = true;
                break;
	    }else{
                last_end += b.get_length();
            }
	}
	
	//If still false, we did not yet insert the character. Therefore put it at the end
	if !did_insert{
	    //Search for last textual block and add if possible
	    let mut idx = self.blocks.len() - 1;
	    let mut added_to_textual_block = false;
	    loop{
		if let Some(b) = self.blocks.get_mut(idx){
		    if b.is_textual(){			
			let len = b.get_length();
			b.insert(character, len);
			added_to_textual_block = true;
			break;
		    }else{
			idx -= 1;
		    }
		}else{
		    //Did not find last textual block.
		    break;
		}
	    }
	    
	    if !added_to_textual_block{
		//No block yet, add block to layout with only this character
		self.blocks.append(&mut Block::parse_string(character.to_string(), self.text_size, self.extent));
	    }
	}

        if let Some((inset_at, blocks)) = exchange_info{
            let _outdated_block = self.blocks.remove(inset_at);
            for (new_idx, new_block) in blocks.into_iter().enumerate(){
                self.blocks.insert(inset_at + new_idx, new_block);
            }
            
        }
    }

    fn clean_remove(&mut self, loc: usize){
	//Currently just reparsing the whole string. However, thats a little bit slow probably.
	//However, otherwise I'd need to detect block type changes, which I'm currently to lazy for.
	let mut string = self.as_string();
	remove_from_string(&mut string, loc);
	self.blocks =  Block::parse_string(string, self.text_size, self.extent);
    }

    fn remove_range(&mut self, start: usize, end: usize){
	//Delete range, then insert
        let string = self.as_string();
        let mut build_string = String::with_capacity(string.len() - (end - start));
        for (idx, c) in string.chars().enumerate(){
            if idx < start || idx >= end{
                build_string.push(c);
            }
        }
        //Re parse blocks
        self.blocks = Block::parse_string(build_string, self.text_size, self.extent);
        //Reset inner layout
        self.layout = vec![];
	//reset cache
	*self.string_cache.write().unwrap() = None;
    }
    
    ///Inserts `character` at the current cursor location. Does nothing if no cursor is set.
    pub fn insert(&mut self, character: char){
	match self.cursor{
	    Cursor::Loc(ref mut loc) => {
		let loccpy = loc.clone();
		*loc += 1;
		self.clean_insert(character, loccpy);
	    },
	    Cursor::Range(start, end) => {
		self.remove_range(start, end);
                //Set cursor to location at which we reset
                self.cursor = Cursor::Loc(start + 1);
                //Now insert the actual character
                self.clean_insert(character, start);
		
	    },
	    Cursor::None => {}
	}
    }

    //Deletes either the character in front of the cursor, or the currently selected range.
    pub fn delete(&mut self){
	match self.cursor{
	    Cursor::Loc(ref mut loc) => {
		let loccpy: usize = *loc;
		//Move cursor back
		*loc = loc.checked_sub(1).unwrap_or(0);
		self.clean_remove(loccpy.checked_sub(1).unwrap_or(0));
	    },
	    Cursor::Range(start, end) => {
		self.remove_range(start, end);
		self.cursor = Cursor::Loc(start + 1);
	    },
	    Cursor::None => return, //Do nothing
	}
	
	//Delete cache
	*self.string_cache.write().unwrap() = None;
    }
    
    fn index_for_location(&self, location: &Vector2<Unit>) -> Option<usize>{
        for (idx, (area, _c)) in self.layout.iter().enumerate(){
            if area.is_in(location){
                return Some(idx);
            }
        }
        None
    }

    pub fn move_cursor(&mut self, offset: isize){
	match &mut self.cursor{
	    Cursor::Loc(loc) => {
		let cast = *loc as isize;
		let mut new_loc = cast + offset;
		if new_loc < 0{
		    new_loc = 0;
		}
		if new_loc >= self.layout.len() as isize && self.layout.len() > 0{
		    new_loc = (self.layout.len() - 1) as isize;
		}else if self.layout.len() == 0{
		    new_loc = 0;
		}

		*loc = new_loc as usize;
	    },
	    Cursor::Range(start, _end) => {
		self.cursor = Cursor::Loc(*start);
	    },
	    Cursor::None => {},
	}
    }

    pub fn select_range(&mut self, mut start: Vector2<Unit>, mut end: Vector2<Unit>){

        //Make sure that the start is "Before" (above and left from the end), so we can do some
        //assumptions of the locations.
        if start.x > end.x{
            std::mem::swap(&mut start.x, &mut end.x);
        }
        if start.y > end.y{
            std::mem::swap(&mut start.y, &mut end.y);
        }
        
        match (self.index_for_location(&start), self.index_for_location(&end)){
            (Some(st), Some(en)) => {
		if st != en{
		    self.cursor = Cursor::Range(st, en)
		}else{
		    self.cursor = Cursor::Loc(st);
		}
	    },
            (None, Some(en)) => self.cursor = Cursor::Range(0, en),
            (Some(st), None) => self.cursor = Cursor::Range(st, self.layout.len() - 1),
            (None, None) => self.cursor = Cursor::None,
        }
    }

    pub fn remove_cursor(&mut self){
	self.cursor = Cursor::None;
    }

    ///Returns the calls made for the last know extent and content
    pub fn into_calls(&self, mut level: usize) -> DrawCalls{
	let mut current_calls = DrawCalls::new(vec![]);

	//Add the current cursor
	match self.cursor{
	    Cursor::Loc(at) => {
		let src_area = if let Some((a, _c)) = self.layout.get(at){
		    *a
		}else{
		    Area::empty()
		};
		
		let cursor_area = Area{
		    from: (
			(src_area.from.x - (self.text_size * 0.025)).max(0.5),
			src_area.from.y
		    ).into(),
		    to: (
			(src_area.from.x + (self.text_size * 0.025)).max(0.5),
			src_area.to.y
		    ).into()
		};
		
		current_calls.add(vec![
		    Prim{
			prim: PrimType::Rect,
			drawing_area: cursor_area,
			scissors: None,
			style: Style::from_color(crate::get_styling().text_cursor),
			level
		    }
		]);

		level += 1;
	    },
	    Cursor::Range(start, end) => {

		//Find all lines based on start/end and push them into the primitives
		current_calls.add(
		    find_lines(&self.layout, start, end)
			.into_iter()
			.map(|area| Prim{
			    prim: PrimType::Rect,
			    drawing_area: area,
			    scissors: None,
			    style: Style::from_color(crate::get_styling().text_selected),
			    level
			}).collect()
		);

		level += 1;
	    },
	    Cursor::None => {}
	}

	//Add character draw calls
	let mut own_calls = self.calls.clone();
	for c in own_calls.primitives.iter_mut(){
	    c.level = level;
	}
	current_calls.merge(own_calls);

	current_calls
    }

    pub fn area(&self) -> Area{
	self.last_area
    }
    
    pub fn print_layout(&self){
	for b in self.blocks.iter(){
	    b.print();
	}
    }
}

fn find_lines(layout: &[(Area, char)], start: usize, end: usize) -> Vec<Area>{
    let mut lines = Vec::new();

    let mut last_char_area = layout[start].0;
    let mut line_start = last_char_area;
    let mut idx = start;

    while idx < end{
	//Search until we found a break
	while let Some((newa, _newc)) = layout.get(idx){
	    if newa.to.y > last_char_area.to.y || idx >= end{
		break;
	    }else{
		idx += 1;
		last_char_area = *newa;
	    }
	}

	//Build line area
	lines.push(Area{
	    from: line_start.from,
	    to: last_char_area.to
	});

	//Advance onto next line
	if let Some((newa, _newc)) = layout.get(idx){
	    last_char_area = *newa;
	    line_start = *newa;
	}else{
	    //break out of search loop since we found last line
	    break;
	}
	idx += 1;
    }

    lines
}

fn insert_into_string(string: &mut String, location: usize, ch: char){
    let mut new_string = String::with_capacity(string.len() + 1);
    let mut added = false;
    for (idx, org_c) in string.chars().enumerate(){
	if idx == location{
	    new_string.push(ch);
	    added = true;
	}
	new_string.push(org_c);
    }
    if !added{
	new_string.push(ch);
    }

    *string = new_string
}

fn remove_from_string(string: &mut String, location: usize){
    *string = string.chars().enumerate().filter_map(|(idx, ch)| if idx == location{None}else{Some(ch)}).collect()
}

///Takes some string and produces drawcalls in a layout based on the `last_location`.
fn layout(
    string: &String,
    extent: Unit,
    last_location: &mut Vector2<Unit>,
    maxes: &mut Vector2<Unit>,
    text_size: Unit,
    line_space: Unit,
    is_single_line: bool,
    interface: &Interface,
    new_layout: &mut Vec<(Area, char)>
) -> DrawCalls{


    //Find the length per word
    let word_length: Vec<(String, Unit, Vec<Area>)> = string.split(' ').map(|word|{
	let mut l = 0.0;
        let mut char_areas = Vec::new();
        for c in word.chars(){
            let char_prim = Character{
                ch: c,
                size: text_size,
                font: FontName::default(),
            };
            let char_area = interface.get_backend().get_character_area(&char_prim);
            l += char_area.extent().x;
            char_areas.push(char_area);
        }
	//Add a space after this word since spaces get deleted by the split
	let char_prim = Character{
            ch: ' ',
            size: text_size,
            font: FontName::default(),
        };
        let char_area = interface.get_backend().get_character_area(&char_prim);
        l += char_area.extent().x;
        char_areas.push(char_area);
	let mut word = word.to_string();
	word.push(' ');
	
	//Finally collect new areas
        (word, l, char_areas)   
    }).collect();


    let mut put_word_after_linebreak = false;
    word_length.into_iter().map(|(word, length, word_chars_area)|{        
        //If needed add line break. However, only add once, since realy small extents might become infinit 
        if last_location.x + length > extent && put_word_after_linebreak && !is_single_line{
            last_location.x = 0.0;
            last_location.y += text_size + line_space;
	    put_word_after_linebreak = false;
        }

	//Update maxes with this len
	maxes.x = maxes.x.max(last_location.x + length);
	maxes.y = maxes.y.max(last_location.y + text_size);
	
        let mut inner_prims = Vec::new();
        for (idx, c) in word.chars().enumerate(){
            let character = Character{
                ch: c,
                size: text_size,
                font: FontName::default(),
            };

            let char_area = Area{
                from: *last_location,
                to: *last_location + word_chars_area[idx].extent()
            };

            new_layout.push((char_area, c));
            
            inner_prims.push(Prim{
                prim: PrimType::Character(character),
                drawing_area: char_area,
                scissors: None,
                style: Style::from_color(crate::get_styling().text),
                level: 0,
            });
	    put_word_after_linebreak = true;
            last_location.x += char_area.extent().x;
        }
        inner_prims
    }).fold(DrawCalls::new(vec![]),|mut calls, new_prims| {
        calls.add(new_prims);
        calls
    })
}
