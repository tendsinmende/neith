# Neith

Yet another experimental Rust UI toolkit. Main features will be easy extension of the toolkit as well as a graphical and terminal backend.

## Description
Yes this is another rust based UI project. I actually did one before: [widkan](https://gitlab.com/tendsinmende/widkan).
I learned a lot from the last one and try to do many things better this time.

Main features I try to archive are:
- Easy extension of the toolkit
- Not just text elements, but also first class implementation of node graphs and custom canvases that can utilize vulkan, graphs etc.
- Better UX experience by employing post progress effects like bloom to better highlight important elements.
- A Vulkan based graphical backend
- Possibility to implement other backend if needed.

## Structure
The crate is split in three parts (plus an example application) at the moment.
- neith: The main interface toolkit part. Contains normal widgets for layouting and input/output as well as communication primitives.
- neith-marp: Vulkan based backend as well as winit based input provider
- neith-widgets: Special widgets like a node canvas, 2d graphs or a spline editor.

## What is the synth crate?
While creating the toolkit I try to also develop a graphical, node based synthesizer to test most aspects of the toolkit. You are free to try it by running 

``` rust
cargo run --bin neith-synth --release
```

## Crate Description
- neith: Main toolkit crate, providing standart widgets as well as anything you need to create a interface with a choosen backend and input provider.
- neith-marp: A [marp](https://gitlab.com/tendsinmende/marp) + [winit](https://github.com/rust-windowing/winit) based backend and input provider. Renders the toolkit base on Vulkan. Note that, because of some technical details the input has to be gathered on the main thread. Therefore your app (and the interface update loop) have to be moved into seperate threads.
- neith-synth: A node based synthesizer, also the test app for this toolkit.

## Debuggin vulkan
You can activate the `debug-layer` feature on the backend (neith-marp), which will activate the lunar-g layers. Make sure they are actually installed.

## License
The whole project is release under the [MPL license](https://www.mozilla.org/en-US/MPL/).
All contributions will be licensed under it as well.

## Current ToDos
- [ ] Add support for polygones
- [ ] update renderer to use instancing
