#!/bin/sh

echo "Compiling shader"
echo "==="
echo ""

echo "Compile Surface shader ..."
glslangValidator -V glsl/surface.frag 
glslangValidator -V glsl/surface.vert 
mv frag.spv spirv/surface_frag.spv
mv vert.spv spirv/surface_vert.spv

echo "Compile Text vertex shader ..."
glslangValidator -V glsl/text.vert
glslangValidator -V glsl/text.frag 
mv vert.spv spirv/text_vert.spv
mv frag.spv spirv/text_frag.spv


echo "Compile Shadow shader ..."
glslangValidator -V glsl/pp_shadows.comp 
mv comp.spv spirv/pp_shadows.spv

echo "Compile Bloom shader ..."
glslangValidator -V glsl/pp_bloom.comp 
mv comp.spv spirv/pp_bloom.spv


echo ""
echo "==="
echo "Finished compiling shaders"
