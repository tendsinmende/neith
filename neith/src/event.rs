use std::path::PathBuf;
use crate::math::*;

///Cursor icons that can be set. More or less copied from Winit@0.22
#[derive(Clone, Copy, Eq, PartialEq)]
pub enum CursorIcon {
    Default,
    Crosshair,
    Hand,
    Arrow,
    Move,
    Text,
    Wait,
    Help,
    Progress,
    NotAllowed,
    ContextMenu,
    Cell,
    VerticalText,
    Alias,
    Copy,
    NoDrop,
    Grab,
    Grabbing,
    AllScroll,
    ZoomIn,
    ZoomOut,
    EResize,
    NResize,
    NeResize,
    NwResize,
    SResize,
    SeResize,
    SwResize,
    WResize,
    EwResize,
    NsResize,
    NeswResize,
    NwseResize,
    ColResize,
    RowResize,
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum KeyState{
    Pressed,
    Released
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Key{
    pub code: KeyCode,
    pub state: KeyState,
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum MouseButton{
    Left,
    Right,
    Middle,
    Other(u16)
}

#[derive(Clone)]
pub enum WindowEvent{
    Resized(Area),
    ///Position of the windows has changed.
    Moved(Vector2<i32>),
    DroppedFile(PathBuf),
    HoveredFile(PathBuf),
    HoveredFileCancelled,
    ReceivedCharacter(char),
    Focused(bool),
    ///Represents the new state of all known modifiers
    ModifiersChanged{
	shift: bool,
	ctrl: bool,
	alt: bool,
	super_key: bool
    },
    ///When the cursor moved. Contains new position relative to the windows top left corner.
    CursorMoved((f64, f64)),
    ///Draw events occure while the left mouse button is clicked and the cursor is moved.
    ///This event is more or less a shortcut for implementing a cursor state.
    CursorDrag{
	start: (Unit, Unit),
	end: (Unit, Unit)
    },
    Scroll((f32, f32)),
    CursorEntered,
    CursorLeft,
    MouseClick{
	button: MouseButton,
	state: KeyState
    },
    ///Key press event with a mapping to some keycode
    Key(Key)
}

#[derive(Clone)]
pub enum DeviceEvent{
    ///notifies about delta in mouse movement. This is the hardware movement of the mouse.
    MouseMoved((f64, f64)),
    ///Notifies about scroll delta in (x,y) direction. 
    MouseWheel((f32, f32)),
}



#[derive(Clone, Copy, Eq, PartialEq)]
pub enum Theme{
    Dark,
    Light
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub enum MetaEvent{
    Destroyed,
    CloseRequest,
    Suspended,
    Resumed,
    Redraw,
    ThemeChanged(Theme)
}


#[derive(Clone)]
pub enum Event{
    WindowEvent(WindowEvent),
    DeviceEvent(DeviceEvent),
    MetaEvent(MetaEvent),
}

impl Event{
    pub fn scale(&self, scale: Unit) -> Self{
	match self{
	    Event::WindowEvent(WindowEvent::CursorMoved(loc)) => Event::WindowEvent(WindowEvent::CursorMoved(
		(
		    loc.0 * scale as f64,
		    loc.1 * scale as f64
		)
	    )),
	    Event::WindowEvent(WindowEvent::CursorDrag{start, end}) => Event::WindowEvent(WindowEvent::CursorDrag{
		start: (start.0 * scale, start.1 * scale),
		end: (end.0 * scale, end.1 * scale)
	    }),

	    _ => self.clone()
	}
    }
    //Creates an events that, if it has any inner location saved, is offseted
    pub fn offset(&self, offset: Vector2<Unit>) -> Self{
	match self{
	    Event::WindowEvent(WindowEvent::CursorMoved(loc)) => Event::WindowEvent(WindowEvent::CursorMoved(
		(
		    loc.0 + offset.x as f64,
		    loc.1 + offset.y as f64
		)
	    )),
	    Event::WindowEvent(WindowEvent::CursorDrag{start, end}) => Event::WindowEvent(WindowEvent::CursorDrag{
		start: (start.0 + offset.x, start.1 + offset.y),
		end: (end.0 + offset.x, end.1 + offset.y)
	    }),

	    _ => self.clone()
	}
    } 
}

#[derive(Eq, PartialEq, Hash, Clone, Copy)]
pub enum KeyCode{
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    Key0,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Escape,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,
    F13,
    F14,
    F15,
    F16,
    F17,
    F18,
    F19,
    F20,
    F21,
    F22,
    F23,
    F24,
    Snapshot,
    Scroll,
    Pause,
    Insert,
    Home,
    Delete,
    End,
    PageDown,
    PageUp,
    Left,
    Up,
    Right,
    Down,
    Back,
    Return,
    Space,
    Compose,
    Caret,
    Numlock,
    Numpad0,
    Numpad1,
    Numpad2,
    Numpad3,
    Numpad4,
    Numpad5,
    Numpad6,
    Numpad7,
    Numpad8,
    Numpad9,
    AbntC1,
    AbntC2,
    Add,
    Apostrophe,
    Apps,
    At,
    Ax,
    Backslash,
    Calculator,
    Capital,
    Colon,
    Comma,
    Convert,
    Decimal,
    Divide,
    Equals,
    Grave,
    Kana,
    Kanji,
    LAlt,
    LBracket,
    LControl,
    LShift,
    LWin,
    Mail,
    MediaSelect,
    MediaStop,
    Minus,
    Multiply,
    Mute,
    MyComputer,
    NavigateForward,
    NavigateBackward,
    NextTrack,
    NoConvert,
    NumpadComma,
    NumpadEnter,
    NumpadEquals,
    OEM102,
    Period,
    PlayPause,
    Power,
    PrevTrack,
    RAlt,
    RBracket,
    RControl,
    RShift,
    RWin,
    Semicolon,
    Slash,
    Sleep,
    Stop,
    Subtract,
    Sysrq,
    Tab,
    Underline,
    Unlabeled,
    VolumeDown,
    VolumeUp,
    Wake,
    WebBack,
    WebFavorites,
    WebForward,
    WebHome,
    WebRefresh,
    WebSearch,
    WebStop,
    Yen,
    Copy,
    Paste,
    Cut,
}
