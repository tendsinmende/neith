use log::error;
use log::warn;

use neith::Interface;
use neith::event::CursorIcon;
use neith::event::DeviceEvent;
use neith::event::Event;
use neith::event::Key;
use neith::event::KeyCode;
use neith::event::KeyState;
use neith::event::MouseButton;
use neith::event::WindowEvent;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::primitive::Lines;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;

use std::{collections::HashMap, time::Instant};

use crate::node::node::*;
use crate::node::edge_container::*;
use crate::node::node_content::*;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct NodeId{
    index: usize
}


struct CanvasNode<I> where I: NodeContent + Send + 'static{
    //The node in this graph
    node: NodeBox<I>,
    //The global location within its canvas as well as its extent from there
    area: Area,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct EdgeId{
    //Node at which the line starts
    start_node: NodeId,
    ///The output idx of the start node
    start_node_port: usize,

    ///Node at which the line ends
    end_node: NodeId,
    ///Input port of end node
    end_node_port: usize
}

enum UnAttachedEdge{
    FromIn{start_node: NodeId, node_in_idx: usize, floating_end: Vector2<Unit>},
    FromOut{start_node: NodeId, node_out_idx: usize, floating_end: Vector2<Unit>},
}


///A canvas to which nodes can be added.
///The canvas also handles connecting nodes and displaying their edges between them.
///
/// # Whats I?
/// I is the type of the node that a node within this canvas is the interface for.
/// The Library is designed with [async_graph](https://gitlab.com/tendsinmende/asyncgraph) in mind,
/// however, you could technically use any thing that implements `NodeContent`.
///
/// # Note on coordinates
/// There are two coordinate systems, the global one, and the local one.
/// local means local to the current position of the view (self.location). However, global
/// is used for all coordinates until something is rendered. Use the local_to_global/local_to_origin (and vice versa)
/// functions to convert. Otherwise you might get difficult to debug bugs.
pub struct NodeCanvas<I> where I: NodeContent<Node = I> + Send + 'static{
    scaling: Unit,

    area: Area,
    //Global location of the current view's top left corner
    location: Vector2<Unit>,

    is_ctrl_pressed: bool,
    is_panning: bool,
    //Some if currently doing something on node (usize). 
    is_on_node: Option<(NodeId, NodeLocation)>,
    //Some if an event happend that changes the cursor
    is_changing_cursor: Option<CursorIcon>,
    //if the is_on_node event is some in/out plug, this might have some unattached line.
    //contains: the index into the lines vector which at which this line is.
    un_attached_line: Option<UnAttachedEdge>,
    //Current set of nodes displayed
    nodes: HashMap<NodeId, CanvasNode<I>>,
    edges: EdgeContainer,
}

//smallest grid size at any scale aka, the minimum a grid can become before a level is culled.
pub const GRID_SIZE: Unit = 50.0;

impl<I> NodeCanvas<I> where I: NodeContent<Node = I> + Send + 'static{
    pub fn new() -> Self{
	NodeCanvas{
	    area: Area::empty(),
	    location: (0.0, 0.0).into(),
	    is_ctrl_pressed: false,
	    is_panning: false,
	    is_on_node: None,
	    is_changing_cursor: None,
	    un_attached_line: None,
	    scaling: 1.0,
	    nodes: HashMap::new(),
	    edges: EdgeContainer::new(),
	}
    }

    fn new_node_id(&self) -> NodeId{
	let mut index = 0;
	while self.nodes.contains_key(&NodeId{index}){
	    index += 1;
	}

	NodeId{index}
    }

    ///Adds the node at the local `location`. This usually should be the location where the cursor was at.
    pub fn add_node_at(&mut self, node: I, location: Vector2<Unit>, size: Vector2<Unit>) -> NodeId{
	let id = self.new_node_id();

	let global_add_location = self.local_to_global(location);

	let target_frame_color = node.frame_color();


	let node = NodeBox::new(node);
	
	//ensure that event the minimum size is guranteed.
	let size = Vector2::new(
	    size.x.max(node.min_extent().x),
	    size.y.max(node.min_extent().y)
	);
	
	let mut cn = CanvasNode{
	    node,
	    area: Area{from: global_add_location, to: global_add_location + size}
	};

	//Check if we should modify the appearance of the node
	if let Some(color) = target_frame_color{
	    cn.node.set_color(color);
	}
	
	self.nodes.insert(id, cn);

	id
    }

    ///Returns a  node if, if some node is at `location`
    pub fn get_node_at(&self, location: &Vector2<Unit>) -> Option<NodeId>{
	let global_loc = self.local_to_global(*location);
	for (nk, node) in self.nodes.iter(){
	    if node.area.is_in(&global_loc){
		return Some(*nk);
	    }
	}
	None
    }

    pub fn get_node(&self, id: NodeId) -> Option<&NodeBox<I>>{
	if let Some(cn) = self.nodes.get(&id){
	    Some(&cn.node)
	}else{
	    None
	}
    }
    
    pub fn get_node_mut(&mut self, id: NodeId) -> Option<&mut NodeBox<I>>{
	if let Some(cn) = self.nodes.get_mut(&id){
	    Some(&mut cn.node)
	}else{
	    None
	}
    }
    
    fn global_to_local(&self, global: Vector2<Unit>) -> Vector2<Unit>{
	(global - self.location) * (1.0 / self.scaling)
    }

    fn local_to_global(&self, local: Vector2<Unit>) -> Vector2<Unit>{
	(local * self.scaling) + self.location
    }

    ///Takes some point in the global coordinates system and returns this point relative to the given origin.
    fn local_to_origin(global_origin: Vector2<Unit>, global_loc: Vector2<Unit>) -> Vector2<Unit>{
	global_loc - global_origin
    }
    ///Takes a point relative to a given origin and returns this point in the global system.
    fn global_to_origin(local_origin: Vector2<Unit>, local_loc: Vector2<Unit>) -> Vector2<Unit>{
	local_loc + local_origin
    }
    
}

impl<I> Widget for NodeCanvas<I> where I: NodeContent<Node = I> + Send + 'static{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	let global_click_pos = self.local_to_global(*location);
	let global_event = event.scale(self.scaling).offset(self.location);
	//Check if we start / stop dragging anything on any node
	match event{
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state}) => {
		match state{
		    KeyState::Pressed => {
			//Check if we started resizing or dragging an edge
			for (id, node) in &self.nodes{
			    let node_local_click = Self::local_to_origin(node.area.from, global_click_pos);
			    let response = node.node.get_location_info(&node_local_click);
			    match response{
				NodeLocation::Corner => {
				    self.is_on_node = Some((*id, response));
				    self.is_changing_cursor = Some(CursorIcon::SeResize);
				},
				NodeLocation::InPort(in_idx) => {
				    //Check if we already have a line from this in port, if that's the case, remove connection and
				    //change into tracking mode, if we do not have a connection, just change into tracking mode
				    if let Some((org_node_out, org_node_out_idx)) = self.edges.remove_connection_from_in(*id, in_idx){
					if let Err(_e) = self.nodes.get(id).unwrap().node.content_mut().in_disconnected(in_idx){
					    warn!("Failed to disconnect in of node in graph");
					};
					if let Err(_e) = self.nodes.get(&org_node_out).unwrap().node.content_mut().out_disconnected(org_node_out_idx){
					    warn!("Failed to disconnect out of node in graph");
					};
					
					self.is_on_node = Some((org_node_out, NodeLocation::OutPort(org_node_out_idx)));
					self.un_attached_line = Some(UnAttachedEdge::FromOut{
					    start_node: org_node_out,
					    node_out_idx: org_node_out_idx,
					    floating_end: global_click_pos
					});					
				    }else{
					//Start on this node with some new line
					self.is_on_node = Some((*id, response));
					self.un_attached_line = Some(UnAttachedEdge::FromIn{
					    start_node: *id,
					    node_in_idx: in_idx,
					    floating_end: global_click_pos
					});
				    }

				    //Anyways change to hands
				    self.is_changing_cursor = Some(CursorIcon::Hand);
				},
				NodeLocation::OutPort(out_idx) => {
				    if let Some((org_node_in, org_node_in_idx)) = self.edges.remove_connection_from_out(*id, out_idx){
					//Notify partners about disconnection
					if let Err(_e) = self.nodes.get(id).unwrap().node.content_mut().out_disconnected(out_idx){
					    warn!("Failed to disconnect out of node in graph");
					};
					if let Err(_e)= self.nodes.get(&org_node_in).unwrap().node.content_mut().in_disconnected(org_node_in_idx){
					    warn!("Failed to disconnect in of node in graph");
					};
					
					self.is_on_node = Some((org_node_in, NodeLocation::InPort(org_node_in_idx)));
					self.un_attached_line = Some(UnAttachedEdge::FromIn{
					    start_node: org_node_in,
					    node_in_idx: org_node_in_idx,
					    floating_end: global_click_pos
					});					
				    }else{
					//Start on this node with some new line
					self.is_on_node = Some((*id, response));
					self.un_attached_line = Some(UnAttachedEdge::FromOut{
					    start_node: *id,
					    node_out_idx: out_idx,
					    floating_end: global_click_pos
					});
				    }
				    self.is_changing_cursor = Some(CursorIcon::Hand);
				},
				NodeLocation::Top(_) => {
				    self.is_on_node = Some((*id, response));
				    self.is_changing_cursor = Some(CursorIcon::Hand)
				},
				_ => {}
			    }
			}
		    },
		    KeyState::Released => {
			//If there was an on_node event before
			if let Some((id, node_event)) = &self.is_on_node{
			    //If we release on any node event other then "None", rest the cursor
			    if let NodeLocation::None = node_event{
				
			    }else{
				self.is_changing_cursor = Some(CursorIcon::Default);
			    }
			    match node_event{
				NodeLocation::InPort(org_in) => {
				    //Since there was a in port dragged out and we are releasing, check if we are releasing over and out port of some other node.
				    //If that's the case, check if the port has the right data type, if thats the case as well, connect both nodes on this ports.
				    for (new_id, node) in &self.nodes{
					if new_id == id{
					    continue; //Let not connect with self
					}
					
					if node.area.is_in(&global_click_pos){
					    let node_local_click = Self::local_to_origin(node.area.from, global_click_pos);
					    if let NodeLocation::OutPort(target_out) = node.node.get_location_info(&node_local_click){
						//Check that this port is not already in use
						if self.edges.out_port_in_use(new_id, target_out){
						    error!("In port was already in use!");
						    break;
						}
						//Check that the data type is correct
						let new_ty = node.node.get_out_ports()[target_out].ty;
						let own_ty = self.nodes.get(id).unwrap().node.get_in_ports()[*org_in].ty;
						if new_ty == own_ty{
						    //Can connect notify both nodes of event, if they return successfully, add edge to edge_container
						    //Safety: since we can be sure that the nodes are not he same, we can do the double mutable borrow.
						    let mut source_node_content = self.nodes.get(new_id).unwrap().node.content_mut();
						    let mut target_node_content = self.nodes.get(id).unwrap().node.content_mut();
						    
						    //Tell nodes about connection
						    if let Err(_err) = source_node_content.try_out_connect(target_out, &target_node_content, *org_in){
							//Did not connect, therefore return
							error!("Did not connect node, Out port already used!");
							continue;
						    };

						    if let Err(_err) = target_node_content.try_in_connect(*org_in, &source_node_content, target_out){
							//Did not connect, message source node the the out connection has to be removed again
							//TODO handle disconnect error porperly
							source_node_content.out_disconnected(target_out).expect("Could not disconnect wrongly connected in port!");
							continue;
						    };
						    self.edges.add_edge(Edge{
						        start_node: *new_id,
						        start_node_port: target_out,
						        end_node: *id,
						        end_node_port: *org_in,
						    });
						}
					    }
					}
				    }
				},
				NodeLocation::OutPort(org_out) => {
				    //Similar to the in port procedure, but some args are switched
				    for (new_id, node) in &self.nodes{
					if new_id == id{
					    continue; //Let not connect with self
					}
					
					if node.area.is_in(&global_click_pos){
					    let node_local_click = Self::local_to_origin(node.area.from, global_click_pos);
					    if let NodeLocation::InPort(target_in) = node.node.get_location_info(&node_local_click){
						//Check that this port is not already in use
						if self.edges.in_port_in_use(new_id, target_in){
						    error!("In port was already in use!");
						    break;
						}

						//Check that the data type is correct
						let new_ty = node.node.get_in_ports()[target_in].ty;
						let own_ty = self.nodes.get(id).unwrap().node.get_out_ports()[*org_out].ty;
						if new_ty == own_ty{
						    let mut target_node_content = self.nodes.get(new_id).unwrap().node.content_mut();
						    let mut source_node_content = self.nodes.get(id).unwrap().node.content_mut();
						    
						    //Tell nodes about connection
						    if let Err(_err) = source_node_content.try_out_connect(*org_out, &target_node_content, target_in){
							//Did not connect, therefore return
							error!("Did not connect node, In port already used!");
							continue;
						    };

						    if let Err(_err) = target_node_content.try_in_connect(target_in, &source_node_content, *org_out){
							//Did not connect, message source node the the out connection has to be removed again
							//TODO handle disconnect error porperly
							source_node_content.out_disconnected(*org_out).expect("Could not disconnect wrongly connected out port!");
							continue;
						    };
						    //Can connect
						    self.edges.add_edge(Edge{
						        start_node: *id,
						        start_node_port: *org_out,
						        end_node: *new_id,
						        end_node_port: target_in,
						    });
						}
					    }
					}
				    }
				},
				_ => {}
			    }
			}

			//What ever happened is handled at this point and we can return to unattached state
			self.is_on_node = None;
			self.un_attached_line = None;
		    }
		}
	    },
	    _ => {}
	}

	let consider_area_grow = Vector2::new(crate::node::node::PORT_NAME_GROW + 100.0, crate::node::node::PORT_NAME_GROW + 100.0);

	//Check if the event concerns any nodes body
	if !self.is_panning && self.is_on_node.is_none() && !self.is_ctrl_pressed{
	    for (_id, node) in &mut self.nodes{
		let node_local_click = Self::local_to_origin(
		    node.area.from,
		    global_click_pos
		);
		if node.node.area().grow_to(consider_area_grow).shrink_from(consider_area_grow).is_in(&node_local_click){
		    let new_ev = global_event.offset(node.area.from * -1.0);
			//Pass event down to node
			node.node.on_event(
			    &new_ev,
			    &node_local_click
			);
		}
	    }
	}
	
	
	match event{
	    //Check if we should start/stop panning.
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::LControl, state})) => {
		match state{
		    KeyState::Pressed => self.is_ctrl_pressed = true,
		    KeyState::Released => self.is_ctrl_pressed = false,
		}
	    }
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Middle, state}) => {
		if state == &KeyState::Pressed && self.area.is_in(location){
		    self.is_panning = true;
		    self.is_changing_cursor = Some(CursorIcon::AllScroll);
		}else if state == &KeyState::Released{
		    self.is_panning = false;
		    self.is_changing_cursor = Some(CursorIcon::Default);
		}
	    },
	    Event::WindowEvent(WindowEvent::CursorDrag{start: _, end: _}) => {
		//Check if we area currently draging something on a node
		if let Some((id, d)) = &self.is_on_node{
		    let narea: Area = self.nodes.get(id).unwrap().area;
		    let min_area = self.nodes.get(id).unwrap().node.min_extent();
		    match d{
			NodeLocation::Corner => self.nodes.get_mut(id).unwrap().area.to = Vector2::new(
			    global_click_pos.x.max(narea.from.x + min_area.x),
			    global_click_pos.y.max(narea.from.y + min_area.y),
			),
			NodeLocation::InPort(_in_idx) => {
			    //Update drag location
			    if let Some(UnAttachedEdge::FromIn{start_node: _, node_in_idx: _, floating_end}) = &mut self.un_attached_line{
				*floating_end = global_click_pos;
			    }
			},
			NodeLocation::OutPort(_out_idx) => {
			    if let Some(UnAttachedEdge::FromOut{start_node: _, node_out_idx: _, floating_end}) = &mut self.un_attached_line{
				*floating_end = global_click_pos;
			    }
			},
			NodeLocation::Top(offset) => {
			    let old_extent = self.nodes.get(id).unwrap().area.extent();
			    let new_from = global_click_pos - *offset;
			    self.nodes.get_mut(id).unwrap().area = Area{
				from: new_from,
				to: new_from + old_extent
			    };
			}
			NodeLocation::None | NodeLocation::Body => {}
		    }
		}
	    },
	    Event::WindowEvent(WindowEvent::Scroll((_x,y))) => {
		if self.area.is_in(location) && self.is_ctrl_pressed{
		    self.scaling = (self.scaling + if *y > 0.0{ 0.1 }else{ -0.1 }).max(1.0).min(200.0);
		}
	    }
	    Event::DeviceEvent(DeviceEvent::MouseMoved(delta)) => {
		if self.is_panning{
		    self.location = self.location - (delta.0 as Unit * self.scaling, delta.1 as Unit * self.scaling).into();
		}
	    },
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::Delete, state: KeyState::Pressed})) => {
		//Delete node if we are over one
		if let Some(node_id) = self.get_node_at(location){
		    let num_in_ports = self.nodes.get(&node_id).unwrap().node.get_in_ports().len();
		    let num_out_ports = self.nodes.get(&node_id).unwrap().node.get_out_ports().len();

		    //remove all connections to / from this node
		    for i in 0..num_in_ports{
			if let Some((removed_edge_on_node, removed_out_idx)) = self.edges.remove_connection_from_in(node_id, i){
			    if let Err(e) = self.nodes.get(&removed_edge_on_node).unwrap().node.content_mut().out_disconnected(removed_out_idx){
				error!("Failed to send disconnect to node: {:?}", e);
			    }
			    if let Err(e) = self.nodes.get(&node_id).unwrap().node.content_mut().in_disconnected(i){
				error!("Failed to send disconnect to deleted node: {:?}", e);
			    }
			}
		    }
		    for i in 0..num_out_ports{
			if let Some((removed_edge_on_node, removed_in_idx)) = self.edges.remove_connection_from_out(node_id, i){
			    if let Err(e) = self.nodes.get(&removed_edge_on_node).unwrap().node.content_mut().in_disconnected(removed_in_idx){
				error!("Failed to send disconnect to node: {:?}", e);
			    }
			    if let Err(e) = self.nodes.get(&node_id).unwrap().node.content_mut().out_disconnected(i){
				error!("Failed to send disconnect to deleted node: {:?}", e);
			    }
			}
		    }
		    //Now delete node it self
		    self.nodes.remove(&node_id);
		}
	    }
	    _ => {}
	}
    }
    

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {	
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	//Check if we want to be the cursor in tracking mode
	if let Some(new_cursor) = self.is_changing_cursor.take(){
	    interface.set_cursor_icon(new_cursor);
	}

	
	//Background color
	let mut calls = DrawCalls::new(vec![Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(neith::get_styling().background),
		level: level
	}]);


	calls.add(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: Area{
		    from: self.global_to_local((-10.0, -10.0).into()),
		    to: self.global_to_local((10.0, 10.0).into())
		},
		scissors: None,
		style: Style::from_color(neith::get_styling().warning),
		level: level + 1
	    }
	]);
	
	
	//If there is some line being tracked, draw it.
	if let Some(floating_line) = &self.un_attached_line{
	    match floating_line{
		UnAttachedEdge::FromIn{start_node, node_in_idx, floating_end} => {
		    let mut start_location = self.nodes.get(start_node).unwrap()
			.node.get_in_port_location(*node_in_idx).unwrap();
		    
		    start_location = Self::global_to_origin(
			self.nodes.get(start_node).unwrap().area.from,
			start_location
		    );
		    start_location = self.global_to_local(start_location);

		    let end = self.global_to_local(*floating_end);

		    let color = self.nodes.get(start_node).unwrap()
			.node.get_in_ports()[*node_in_idx].color.clone();
		    
		    calls.add(vec![
			Prim{
			    prim: PrimType::Lines(Lines{
				points: vec![
				    start_location,
				    end
				],
				width: 2.0
			    }),
			    drawing_area: self.area,
			    style: Style::from_color(color),
			    scissors: None,
			    level: level + 2
			}
		    ])
		},
		UnAttachedEdge::FromOut{start_node, node_out_idx, floating_end} => {
		    let mut start_location = self.nodes.get(start_node).unwrap()
			.node.get_out_port_location(*node_out_idx).unwrap();
		    
		    start_location = Self::global_to_origin(
			self.nodes.get(start_node).unwrap().area.from,
			start_location
		    );
		    start_location = self.global_to_local(start_location);

		    let end = self.global_to_local(*floating_end);

		    let color = self.nodes.get(start_node).unwrap()
			.node.get_out_ports()[*node_out_idx].color.clone();
		    
		    calls.add(vec![
			Prim{
			    prim: PrimType::Lines(Lines{
				points: vec![
				    start_location,
				    end
				],
				width: 2.0
			    }),
			    drawing_area: self.area,
			    style: Style::from_color(color),
			    scissors: None,
			    level: level + 2
			}
		    ])
		}
	    }
	}

	//Prefetch, since we can't double borrow in iter_mut
	let location = self.location;
	let scaling = self.scaling;
	//Calculate all nodes inner calls in global space
	calls.merge(self.nodes.iter_mut().map(|(_id , node)|{
	    let mut node_calls = node.node.get_calls(
		node.area.extent(),
		draw_time,
		interface,
		level + 3
	    );

	    //scale to local space
	    //Now offset into global space
	    node_calls.offset(node.area.from);
	    node_calls.offset(location * -1.0);
	    node_calls = node_calls.scale(1.0 / scaling);
	    
	    node_calls
	}).fold(DrawCalls::new(vec![]), |mut acc, new_calls| {
	    acc.merge(new_calls);
	    acc
	}));
	

	
	calls.add(self.edges.get_edges().map(|l|{
	    //Calculate the relative start and end location
	    let mut line_start = self.nodes.get(&l.start_node).unwrap().node.get_out_ports()[l.start_node_port].area.from + Vector2::new(PORT_SIZE / 2.0, PORT_SIZE / 2.0);
	    line_start = Self::global_to_origin(self.nodes.get(&l.start_node).unwrap().area.from, line_start);
	    line_start = self.global_to_local(line_start);
	    
	    let mut line_end = self.nodes.get(&l.end_node).unwrap().node.get_in_ports()[l.end_node_port].area.from + Vector2::new(PORT_SIZE / 2.0, PORT_SIZE / 2.0);
	    line_end = Self::global_to_origin(self.nodes.get(&l.end_node).unwrap().area.from, line_end);
	    line_end = self.global_to_local(line_end);

	    let color = self.nodes.get(&l.end_node).unwrap().node.get_in_ports()[l.end_node_port].color.clone();
	    
	    
	    Prim{
	        prim: PrimType::Lines(Lines{
		    points: vec![
			line_start,
			line_end
		    ],
		    width: 2.0,
		}),
	        drawing_area: self.area,
	        scissors: None,
	        style: Style::from_color(color),
	        level: level + 3,
	    }
	}).collect());

	//Now clip everyting within the canvas
	calls.clip_to(host_extent);
	
	calls
    }
}
