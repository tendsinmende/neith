use std::sync::Arc;
use std::sync::Mutex;
use std::time::Instant;

use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;

///Some widget that can be cloned. Therefore this can be used to exchange widgets within a widget tree at runtime.
///Is thread safe.
#[derive(Clone)]
pub struct ArcWidget{
    child: Arc<Mutex<Box<dyn Widget + Send + 'static>>>
}

impl ArcWidget{
    pub fn new<W>(child: W) -> Self where W: Widget + Send + 'static{
	ArcWidget{
	    child: Arc::new(Mutex::new(Box::new(child)))
	}
    }

    ///Exchanges the inner `child` with `other. Similar to `std::mem::swap`.
    pub fn exchange(&self, other: &mut Box<dyn Widget + Send + 'static>){
	std::mem::swap(&mut *self.child.lock().unwrap(), other);
    }
}

impl Widget for ArcWidget{
    fn area(&self) -> Area {
	self.child.lock().unwrap().area()
    }

    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	self.child.lock().unwrap().on_event(event, location)
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls {
	self.child.lock().unwrap().get_calls(host_extent, draw_time, interface, level)
    }
}
