use std::time::Instant;
use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;

pub struct RootWidget{
    area: Area,
    pub child: Option<Box<dyn Widget + Send>>,
}

impl RootWidget{

    pub fn new() -> Self{
	RootWidget{
	    area: Area::empty(),
	    child: None
	}
    }

    ///Sets `new` as  the widget of this root, returns the old widget if any was set before.
    pub fn set_widget<W>(&mut self, new: W) -> Option<Box<dyn Widget + Send>>
	where W: Widget + Send + 'static,
    {

	let old = self.child.take();
	self.child = Some(Box::new(new));	
	old
    }
    pub fn set_widget_arc(&mut self, new: Box<dyn Widget + Send +>) -> Option<Box<dyn Widget + Send>>
    {

	let old = self.child.take();
	self.child = Some(new);	
	old
    }
}

impl Widget for RootWidget{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	if let Some(c) = &mut self.child{
	    c.on_event(event, location);
	}
    }
    
    ///Returns current occupied size
    fn area(&self) -> Area{
	self.area
    }

    ///Collects the primitives that have to be draw, provides the time at which they will be drawn.
    /// `draw_time` can be used to implement time dependent effects.
    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};
	
	if let Some(c) = &mut self.child{
	    c.get_calls(host_extent, draw_time, interface, level + 1)
	}else{
	    DrawCalls::new(vec![]) //Nothing, might be a covering rectangle later
	}
    }
}
