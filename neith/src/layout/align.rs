use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;

use std::time::Instant;

pub enum VerticalAlign{
    Top,
    Center,
    Bottom
}

pub enum HorizontalAlign{
    Left,
    Center,
    Right
}

///Positions some child widget within this area. The child widget will always get its minimal area and be positioned accordingly to the set rules.
///
/// If the host area is smaller then the child's minimal area, it might be not completely visible. In this case maybe a [Viewport] should be used.
pub struct Align<T>{
    pub child: Box<T>,
    pub va: VerticalAlign,
    pub ha: HorizontalAlign,
    pub child_size: Option<Vector2<Unit>>,
    area: Area
}

impl<T> Align<T> where T: Widget + Send + 'static{
    pub fn new(child: T) -> Self{
	Align{
	    child: Box::new(child),
	    va: VerticalAlign::Center,
	    ha: HorizontalAlign::Center,
	    child_size: None,
	    area: Area::empty()
	}
    }

    pub fn with_child_size(mut self, size: Vector2<Unit>) -> Self{
	self.child_size = Some(size);
	self
    }
    
    pub fn with_horizontal_alignment(mut self, alignment: HorizontalAlign) -> Self{
	self.ha = alignment;
	self
    }

    pub fn with_vertical_alignment(mut self, alignment: VerticalAlign) -> Self{
	self.va = alignment;
	self
    }

    pub fn child_offset(&self) -> Vector2<Unit>{
	
	let child_area = self.child.area();
	let mut offset: Vector2<Unit> = (0.0, 0.0).into();
	match self.ha{
	    HorizontalAlign::Left => {
		//No offset needed, 0 is 0 in this
	    },
	    HorizontalAlign::Center => {
		//Move the center of the widget to the center of the host
		let child_half_width = child_area.extent().x / 2.0; //Since it is always starting at 0
		let self_half_width = self.area.extent().x / 2.0;
		
		offset.x = self_half_width - child_half_width;
	    },
	    HorizontalAlign::Right => {
		let child_ext = child_area.extent().x;
		offset.x = self.area.to.x - child_ext;
	    }
	}

	match self.va{
	    VerticalAlign::Top => {
		//No Offset
	    },
	    VerticalAlign::Center => {
		let child_half_width = child_area.extent().y / 2.0; //Since it is always starting at 0
		let self_half_width = self.area.extent().y / 2.0;
		
		offset.y = self_half_width - child_half_width;
	    },
	    VerticalAlign::Bottom =>{
		let child_ext = child_area.extent().y;
		offset.y = self.area.to.y - child_ext;
	    }
	}

	offset
    }

    fn child_area(&self) -> Area{
	Area{from: (0.0, 0.0).into(), to: self.child_size.unwrap_or(self.area.extent())}
    }
}

impl<T> Widget for Align<T> where T: Widget + Send + 'static{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){

	let offset = self.child_offset() * -1.0;
	let e = event.offset(offset);
	self.child.on_event(&e, &(*location + offset))
	
    }
    
    ///Resizes the Widget in a way that it lays within this area.
    
    ///Returns current occupied size
    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{	
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	let child_area = self.child_area();
	let mut child_calls = self.child.get_calls(
	    child_area.extent(), draw_time, interface, level
	);

	let offset = self.child_offset();
	
	child_calls.offset(offset);
	//Clip if exceeds host area
	child_calls.clip_to(host_extent);
	child_calls
    }
}
