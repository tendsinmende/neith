use crate::Interface;
use crate::event::DeviceEvent;
use crate::event::Event;
use crate::event::KeyState;
use crate::event::MouseButton;
use crate::event::WindowEvent;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use std::time::Instant;

use super::Label;

///Some button that executes some `action` when pressed.
///
/// The action is defines as an `Fn()`. Therefore it cannot mutate its context. If you still want to mutate something,
/// use the `move` keyword as well as interior mutability like RefCell, RwLock or Mutex.
pub struct Button{
    label: Box<crate::layout::Align<Label>>,

    is_selected: bool,

    action: Option<Box<dyn FnMut() + Send>>,    
    area: Area
}

impl Button{
    pub fn new(label: &str) -> Self{
	
	Button{
	    label: Box::new(crate::layout::Align::new(crate::io::Label::new(label))),
	    action: None,
	    is_selected: false,
	    area: Area::empty()
	}
    }

    pub fn with_action<A: 'static>(mut self, action: A) -> Self where A: FnMut() + Send{
	self.action = Some(Box::new(action));
	self
    }

    pub fn set_label(&mut self, label: Box<Label>){
	self.label.child = label;
    }
}

impl Widget for Button{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	match event{
	    Event::DeviceEvent(DeviceEvent::MouseMoved(_)) => {
		if self.area.is_in(location){
		    self.is_selected = true;
		}else{
		    self.is_selected = false;
		}
	    }

	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state: KeyState::Pressed}) => {
		if self.area.is_in(location) {
		    if let Some(ref mut a) = self.action{
			a();
		    }
		}
	    }
	    _ => {}
	}
    }

    ///Returns current occupied size
    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{	

	let default_button_extent: Vector2<Unit> = (crate::get_config().default_button_size as (Unit, Unit)).into();
	if host_extent.x > default_button_extent.x && host_extent.y > default_button_extent.y{
	    self.area = Area{from: (0.0, 0.0).into(), to: default_button_extent};
	}else{
	    self.area = Area{from: (0.0, 0.0).into(), to: host_extent};
	}
	
	//Draw label to see which size the button should have. Then Decide based on host and
	//preffered size, what we actually use
	let child_calls = self.label.get_calls(
	    self.area.extent(), draw_time, interface, level + 1);

	let prims = vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(
		    if self.is_selected{
			crate::get_styling().background_sec_selected
		    }else{
			crate::get_styling().background_sec
		    }
		),
		level: level
	    }
	];
	let mut calls = DrawCalls::new(prims);
	calls.merge(child_calls);

	calls
    }
}
