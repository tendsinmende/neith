use std::time::Instant;

use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;



///Can contain several sub widgets that are placed "somewhere" within this area.
///The locations coordinates used when placing a widget should be in the range [0.0, 1.0]. They area expanding at runtime to the percentage of the
/// actual area of the floating widget. So a widget occupying the whole range is occupying the whole area when rendered.
pub struct Floating{
    sub_widgets: Vec<(Area, Box<dyn Widget + Send + 'static>)>,
    area: Area
}

impl Floating{

    pub fn new() -> Self{
	Floating{
	    sub_widgets: vec![],
	    area: Area::empty(),
	}
    }

    pub fn with_element<W>(mut self, area: Area, element: W) -> Self where W: Widget + Send + 'static{
	//Normalize area
	let area = area.union(Area::unit());
	self.sub_widgets.push((area, Box::new(element)));	
	self
    }

    pub fn with_element_boxed(mut self, area: Area, element: Box<dyn Widget + Send + 'static>) -> Self{
	//Normalize area
	let area = area.union(Area::unit());
	self.sub_widgets.push((area, element));	
	self
    }
    
    ///Performs action on widgets, but corrects area to be in the actual coordinates based on the current area of the floating widget.
    fn on_widgets<F, R>(&mut self, mut f: F) -> Vec<R> where F: FnMut(Area, &mut Box<dyn Widget + Send + 'static>) -> R{
	let area_extent = self.area.extent();
	self.sub_widgets.iter_mut().map(|(relativ_area, widget)|{
	    let expanded_area = Area{
		from: relativ_area.from * area_extent,
		to: relativ_area.to * area_extent
	    };
	    f(expanded_area, widget)
	}).collect()
    }
}

impl Widget for Floating{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	//Offset event into widget local space and push down
	self.on_widgets(|area, widget|{
	    widget.on_event(
		&event.offset(area.from * -1.0),
		&(*location - area.from)
	    )
	});
    }

    fn area(&self) -> Area {
	self.area
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls {

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};


	let area_extent = self.area.extent();
	let calls = DrawCalls::new(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level
	    }
	]);

	//TODO draw backround

	self.on_widgets(|area, widget|{
	    let mut calls = widget.get_calls(
		area.extent(),
		draw_time,
		interface,
		level+1
	    );

	    //Offset into local space
	    calls.offset(area.from);
	    calls.clip_to(area_extent);
	    calls
	}).into_iter().fold(calls, |mut calls, sub_calls| {
	    calls.merge(sub_calls);
	    calls
	})
    }
}
