//! Special neith widgets that are not as general purpose as the ones included in neith itself.

extern crate neith;
extern crate splines;

pub mod audio;
pub mod graph;
///Crate to create a node canvas, define custom nodes and add them to the node canvas.
///
/// Can be used to implement any node based systems.
pub mod node;

///Utility functions. For instance, loading vertices for a obj file.
pub mod utils;

///Utility widgets, for instance a log widget that prints out everything that goes into `log` macros.
pub mod utility;
