//! Collection of several graph types.


use neith::math::{Unit, Vector2};
use neith::event::Event;
use neith::widget::Widget;

pub mod bar_graph;

pub mod line_graph;
pub use line_graph::LineGraph;

pub mod spline_editor;
pub use spline_editor::*;


pub mod graph_host;
pub use graph_host::*;

pub trait Graph : Widget{
    ///samples the graph at some location. The value might mean different things, based on the graph you are sampling.
    fn get(&self, _x: f32) -> Option<f32>{
	None
    }
}
