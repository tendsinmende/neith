use crate::Interface;
use crate::event::Event;
use crate::event::Key;
use crate::event::KeyCode;
use crate::event::KeyState;
use crate::event::WindowEvent;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::FontName;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use std::time::Instant;

///Handles layouting of text.
pub mod layouting;

///Base text primitive that can be used to display any Text. Multi and single line. It supports a cursor as well as editing operations.
pub struct Text{
    pub font: Option<FontName>,
    pub style: Option<Style>,
    pub layout: layouting::Layout,
    has_changed: bool,
    area: Area
}

impl Text{
    pub fn new() -> Self{
	Text{
	    font: None,
	    style: None,
            layout: layouting::Layout::new(
                "".to_string(),
                crate::get_config().text_size as Unit,
                0.0,
                0.0,
            ),
	    has_changed: true,
	    area: Area::empty()
	}
    }

    ///Prohibits the widget from starting new lines of text.
    pub fn with_single_line(mut self) -> Self{
	self.layout.is_single_line = true;
	self
    }

    pub fn with_font(mut self, font: FontName) -> Self{
	self.font = Some(font);
	self
    }

    pub fn with_style(mut self, style: Style) -> Self{
	self.style = Some(style);
	self
    }

    pub fn with_size(mut self, size: Unit) -> Self{
        self.layout.text_size = size;
	self
    }

    pub fn with_line_space(mut self, space: Unit) -> Self{
        self.layout.line_space = space;
        self
    }

    pub fn get_size(&self) -> Unit{
	self.layout.text_size
    }

    pub fn get_line_space(&self) -> Unit{
        self.layout.line_space    }

    pub fn get_font(&self) -> FontName{
	self.font.clone().unwrap_or(FontName::default())
    }

    pub fn get_style(&self) -> Style{
	*self.style.as_ref().unwrap_or(&Style::from_color(crate::get_config().styling.text))
    }
    
    pub fn with_text(mut self, text: &str) -> Self{
	self.layout = layouting::Layout::new(
            text.to_string(),
            self.layout.text_size,
            self.area.extent().x,
            self.layout.line_space
        );
	self
    }

    pub fn set_text(&mut self, new: String){
	self.layout = layouting::Layout::new(
            new,
            self.layout.text_size,
            self.area.extent().x,
            self.layout.line_space
        );
	self.has_changed = true;
    }

    ///Builds the string from the current string.
    pub fn get_text(&self) -> String{
        self.layout.as_string()
    }
    
    fn update_lines(&mut self, interface: &Interface){
	if self.has_changed{
	    self.layout.extent = self.area.extent().x;
            self.layout.update_calls(interface);
    	    self.has_changed = false;
	}
    }
}

impl Widget for Text{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	if self.area.is_in(location){
	    match event{
		Event::WindowEvent(WindowEvent::CursorDrag{start, end}) => {
		    self.layout.select_range(start.clone().into(), end.clone().into());
		    self.has_changed = true;
		},
		Event::WindowEvent(WindowEvent::ReceivedCharacter(ch)) => {
		    if !ch.is_control(){
			//Is no controll character, therfore just add
			self.layout.insert(*ch);
		    }else{
			match ch{
			    '\n' | '\r' => self.layout.insert('\n'),
			    _ => {} //Ignore
			}
		    }
		    self.has_changed = true;
		},
		Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::Back, state: KeyState::Pressed})) => {
		    self.layout.delete();
		    self.has_changed = true;
		},
		Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::Left, state: KeyState::Pressed})) => {
		    self.layout.move_cursor(-1);
		    self.has_changed = true;
		},
		Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::Right, state: KeyState::Pressed})) => {
		    self.layout.move_cursor(1);
		    self.has_changed = true;
		}
		_ => {}
	    }
	}
    }
    
    ///Returns current occupied size
    fn area(&self) -> Area{
	self.layout.area()
    }

    
    fn get_calls(&mut self, host_extent: Vector2<Unit>, _draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls{
	//Set new area
	let new_area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	if new_area != self.area{
	    self.has_changed = true;
	}
	
	//Update lines
	self.update_lines(interface);

        let mut calls = self.layout.into_calls(level + 1);
	//Overwrite styling
	if let Some(st) = self.style{
	    for p in calls.primitives.iter_mut(){
		if let PrimType::Character(_) = &p.prim{
		    p.style = st;
		}
	    }
	}
	
	let layout_area = self.layout.area();
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: (host_extent.x.max(layout_area.extent().x), host_extent.y.max(layout_area.extent().y)).into()
	};
	calls
    }
}
