#version 450

layout (location = 0) in vec2 uv;
layout (location = 1) in vec4 pos;

layout (location = 0) out vec4 uFragColor;
layout (location = 1) out vec4 uEmission;

layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 color;
  float power;
  float level;
  vec2 pad1;
}pc;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main() {  
  uFragColor = vec4(pc.color.xyz, pc.level);
  uEmission = vec4(pc.power, vec3(0.0));
}
