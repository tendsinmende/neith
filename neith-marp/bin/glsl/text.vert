#version 450

//Pretty much the same sahder as surface.vert, but the level is always at 1.0. Things that are at 1.0 do not have shadow. 

//Standard vertex layout.
layout (location = 0) in vec4 pos;
layout (location = 1) in vec2 uv;


layout (push_constant) uniform PushConsts{
  mat4 transform;
  vec4 color;
  float power;
  float level;
  vec2 pad1;
}pc;

layout (location = 0) out vec2 out_uv;
layout (location = 1) out vec4 out_pos;

void main() {
  out_uv = uv;
  vec4 inter_pos = pc.transform * pos;
  
  gl_Position = vec4(inter_pos.x, inter_pos.y, pc.level, 1.0);
  out_pos = vec4(inter_pos.xy, pc.level, 1.0);
}
