use crate::vulkan_prelude::*;
use std::sync::Arc;

#[repr(C)]
#[derive(Clone,Copy,Debug)]
pub struct ShadowPush {
    pub darkening: f32,
    pub pad1: [f32;3]
}

#[repr(C)]
#[derive(Clone,Copy,Debug)]
pub struct BloomPush {
    pub is_vertical: u32,
    pub pad1: [u32;3]
}

fn shadow_shader() -> Vec<u32>{
    let bytes = include_bytes!("../bin/spirv/pp_shadows.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read pp_shadow shader!")
}

fn bloom_shader() -> Vec<u32>{
    let bytes = include_bytes!("../bin/spirv/pp_bloom.spv");
    marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("Could not read pp_bloom shader!")
}


static WORKGROUP_SIZE: f32 = 32.0;
fn dispatch_extent(e: vk::Extent3D) -> [u32;3]{
    [
	((e.width as f32) / WORKGROUP_SIZE).ceil() as u32,
	((e.height as f32) / WORKGROUP_SIZE).ceil() as u32,
	((e.depth as f32) / WORKGROUP_SIZE).ceil() as u32,
    ]
}

struct FrameData{
    image_in: Arc<Image>,
    after_shadow: Arc<Image>,
    after_vertical_bloom: Arc<Image>,
    image_out: Arc<Image>,
    
    shadow_descriptor: Arc<DescriptorSet>,
    bloom_v_descriptor: Arc<DescriptorSet>,
    bloom_h_descriptor: Arc<DescriptorSet>,
}

///Several linear passes that add bloom and shadows to the
/// interface to add a perception of depth
pub struct PostProgress{


    data: Vec<FrameData>,
    
    shadow_pipeline: Arc<dyn Pipeline + Send + Sync>,
    bloom_pipeline: Arc<dyn Pipeline + Send + Sync>,
}

impl PostProgress{
    pub fn new(
	device: Arc<Device>,
	color_format: vk::Format,
	extent: (u32, u32),
	count: usize,
	interface_pass_img: Vec<(Arc<Image>, Arc<Image>)>
    ) -> Self{

	let shadow_shader = ShaderModule::new_from_code(
            device.clone(),
            shadow_shader()
        ).expect("Failed to load vertex shader: pp_shadow");

	let bloom_shader = ShaderModule::new_from_code(
            device.clone(),
            bloom_shader()
        ).expect("Failed to load vertex shader: pp_bloom");

	let shadow_stage = shadow_shader.to_stage(Stage::Compute, "main");
	let bloom_stage = bloom_shader.to_stage(Stage::Compute, "main");	


	//We create only one descriptorset, that is shared by all stages, since it only contains the
	//final passes color image, all dynamic data is pushed.
	let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![
                vk::DescriptorPoolSize::builder()
                    .ty(vk::DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(count as u32 * 8)
                    .build(),
            ].as_slice(),
            count as u32 * 3 //one set per frame
        ).expect("Failed to create descriptor_pool_size!");


	let mut data_sets = Vec::new();

	assert!(interface_pass_img.len() == count, "Do not match");
	
	for (color_img, emission) in interface_pass_img{

	    let color_image_info = ImageInfo::new(
		ImageType::Image2D {
                    width: extent.0,
                    height: extent.1,
                    samples: 1,
		},
		color_format,
		None,
		Some(MipLevel::Specific(1)),
		ImageUsage {
                    transfer_src: true,
		    color_aspect: true,
		    storage: true,
                    ..Default::default()
		},
		MemoryUsage::GpuOnly,
		None,
            );

	    //Shadow descriptor set builder
	    let after_shadow = marp::image::Image::new(
		device.clone(),
		color_image_info.clone(),
		SharingMode::Exclusive,
            ).expect("Failed to create color compute image!");
	    
	    let mut descriptor_set_builder = descriptor_pool.next();
	    descriptor_set_builder.add(DescResource::new_image(
                0,
                vec![(color_img.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    descriptor_set_builder.add(DescResource::new_image(
                1,
                vec![(after_shadow.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    
	    let shadow_descriptor = descriptor_set_builder.build().expect("Failed to build descriptor set!");

	    //Bloom descriptor set
	    //Vertical
	    let after_v_bloom = marp::image::Image::new(
		device.clone(),
		color_image_info.clone(),
		SharingMode::Exclusive,
            ).expect("Failed to create color compute image!");
	    
	    let mut descriptor_set_builder = descriptor_pool.next();
	    descriptor_set_builder.add(DescResource::new_image(
                0,
                vec![(after_shadow.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    descriptor_set_builder.add(DescResource::new_image(
                1,
                vec![(emission.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    descriptor_set_builder.add(DescResource::new_image(
                2,
                vec![(after_v_bloom.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    let bloom_v_descriptor = descriptor_set_builder.build().expect("Failed to build descriptor set!");

	    //Horizontal
	    let after_h_bloom = marp::image::Image::new(
		device.clone(),
		color_image_info,
		SharingMode::Exclusive,
            ).expect("Failed to create color compute image!");
	    
	    let mut descriptor_set_builder = descriptor_pool.next();
	    descriptor_set_builder.add(DescResource::new_image(
                0,
                vec![(after_v_bloom.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    descriptor_set_builder.add(DescResource::new_image(
                1,
                vec![(emission.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    descriptor_set_builder.add(DescResource::new_image(
                2,
                vec![(after_h_bloom.clone(), None, vk::ImageLayout::GENERAL)],
                vk::DescriptorType::STORAGE_IMAGE
            )).expect("Failed to add image descriptor set");
	    let bloom_h_descriptor = descriptor_set_builder.build().expect("Failed to build descriptor set!");
	    
	    data_sets.push(FrameData{
		image_in: color_img,
		after_shadow,
		after_vertical_bloom: after_v_bloom,
		image_out: after_h_bloom,
		
		shadow_descriptor,
		bloom_v_descriptor,
		bloom_h_descriptor,
	    });
	}

	
	let fake_shadow_push = PushConstant::new(
            ShadowPush {
                darkening: 1.0,
		pad1: [0.0;3]
            },
            vk::ShaderStageFlags::ALL,
        );

	let shadow_pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data_sets[0].shadow_descriptor.layout()],
            vec![*fake_shadow_push.range()],
        ).expect("Failed to create PipelineLayout for Shadow pipeline");
	
	let fake_bloom_push = PushConstant::new(
            BloomPush {
                is_vertical: 0,
		pad1: [0;3]
            },
            vk::ShaderStageFlags::ALL,
        );

	let bloom_pipeline_layout = marp::pipeline::PipelineLayout::new(
            device.clone(),
            vec![*data_sets[0].bloom_v_descriptor.layout()],
            vec![*fake_bloom_push.range()],
        ).expect("Failed to create PipelineLayout for Bloom pipeline");


	let shadow_pipeline = ComputePipeline::new(
	    device.clone(),
	    shadow_stage,
	    shadow_pipeline_layout
	).expect("Failed to create shadow pipeline");

	let bloom_pipeline = ComputePipeline::new(
	    device.clone(),
	    bloom_stage,
	    bloom_pipeline_layout
	).expect("Failed to create bloom pipeline");

	PostProgress{
	    shadow_pipeline,
	    bloom_pipeline,
	    data: data_sets,
	}
    }

    pub fn execute(&self, command_buffer: Arc<CommandBuffer>, _src_image: Arc<Image>, index: usize){

	//Transfer out and intermediate images to general
	command_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![
		self.data[index].after_shadow.new_image_barrier(
                    Some(vk::ImageLayout::UNDEFINED),
                    Some(vk::ImageLayout::GENERAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		),
		self.data[index].after_vertical_bloom.new_image_barrier(
                    Some(vk::ImageLayout::UNDEFINED),
                    Some(vk::ImageLayout::GENERAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		),
		self.data[index].image_out.new_image_barrier(
                    Some(vk::ImageLayout::UNDEFINED),
                    Some(vk::ImageLayout::GENERAL),
                    None,
                    None, //No queue transfer
                    None, //No old access mask
                    None, //We want to write to it only
                    None //All of the image should be transfered
		),
	    ]
        ).expect("Failed to add pipeline barrier while copying");

	
	//Bind shadow pipeline and execute
	command_buffer.cmd_bind_descriptor_sets(
            vk::PipelineBindPoint::COMPUTE,
            self.shadow_pipeline.layout(),
            0,
            vec![self.data[index].shadow_descriptor.clone()],
            vec![]
        ).expect("Failed to bind descriptor set!");
	
	command_buffer.cmd_bind_pipeline(
            vk::PipelineBindPoint::COMPUTE,
            self.shadow_pipeline.clone()
        ).expect("Failed to bind shadow pipeline");

	command_buffer.cmd_push_constants(
	    self.shadow_pipeline.layout(),
	    &PushConstant::new(
		ShadowPush{
		    darkening: 0.4,
		    pad1: [0.0;3]
		},
                vk::ShaderStageFlags::ALL,
	    )
	).expect("Failed to push new consts for surface");
	
	command_buffer.cmd_dispatch(
            dispatch_extent(self.data[index].image_in.extent_3d())
        ).expect("Could not execute shadow pass!");

	let _thingy = vk::PipelineStageFlags::ALL_COMMANDS;
	
	//Wait for shadows, then execute bloom pass
	command_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![]
        ).expect("Failed to add pipeline barrier while waiting for shadows");

	command_buffer.cmd_bind_pipeline(
            vk::PipelineBindPoint::COMPUTE,
            self.bloom_pipeline.clone()
        ).expect("Failed to bind bloom pipeline");
	
	command_buffer.cmd_bind_descriptor_sets(
            vk::PipelineBindPoint::COMPUTE,
            self.bloom_pipeline.layout(),
            0,
            vec![self.data[index].bloom_v_descriptor.clone()],
            vec![]
        ).expect("Failed to bind descriptor set!");
	
	command_buffer.cmd_push_constants(
	    self.bloom_pipeline.layout(),
	    &PushConstant::new(
		BloomPush{
		    is_vertical: 1,
		    pad1: [0;3]
		},
                vk::ShaderStageFlags::ALL,
	    )
	).expect("Failed to push new consts");
	
	command_buffer.cmd_dispatch(
            dispatch_extent(self.data[index].image_in.extent_3d())
        ).expect("Could not execute bloom pass!");

	command_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::PipelineStageFlags::ALL_COMMANDS,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![]
        ).expect("Failed to wait for vertical pass");

	
	command_buffer.cmd_bind_descriptor_sets(
            vk::PipelineBindPoint::COMPUTE,
            self.bloom_pipeline.layout(),
            0,
            vec![self.data[index].bloom_h_descriptor.clone()],
            vec![]
        ).expect("Failed to bind descriptor set!");
	command_buffer.cmd_push_constants(
	    self.bloom_pipeline.layout(),
	    &PushConstant::new(
		BloomPush{
		    is_vertical: 0,
		    pad1: [0;3]
		},
                vk::ShaderStageFlags::ALL,
	    )
	).expect("Failed to push new consts");
	
	command_buffer.cmd_dispatch(
            dispatch_extent(self.data[index].image_in.extent_3d())
        ).expect("Could not execute bloom pass!");
    }

    //Returns the out image
    pub fn get_final_image(&self, index: usize) -> Arc<Image>{
	self.data[index].image_out.clone()
    }
}
