use crate::backend::BackendError;
use std::path::PathBuf;
use std::sync::RwLock;
use crate::command_builder::Vertex;
use std::collections::HashMap;

use crate::vulkan_prelude::*;
use crate::pipelines::text::{
    TextPipeline,
    TextPush
};

use std::sync::Arc;
use std::io::Read;

use neith::math::Area;
use neith::math::Unit;
use neith::primitive::Character;
use neith::primitive::FontName;
use neith::widget::Style;
use rusttype::Font;

use lyon::{
    math::{
	point,
	Point
    },
    path::Path,
    tessellation::*,
};

fn get_default_regular() -> Font<'static>{
    let font_data: &[u8] = include_bytes!("../bin/Fonts/Montserrat-Light.ttf");
    let font: Font<'static> = Font::from_bytes(font_data).expect("Failed to load default regular font");
    font
}


fn get_fallback_buffer(device: Arc<Device>, queue: Arc<Queue>) -> (Arc<Buffer>, Arc<Buffer>){
    let indices: Vec<u32> = vec![0, 1, 2, 2, 3, 0];
    let (index_upload, index_buffer) = Buffer::from_data(
        device.clone(),
        queue.clone(),
        indices.clone(), //The buffer size we need
        BufferUsage {
            index_buffer: true,
            storage_buffer: true,
            ..Default::default()
        },
        SharingMode::Exclusive,
        MemoryUsage::GpuOnly //we only want device access for this buffer.
    )
        .expect("Failed to create index buffer");
    index_upload.wait(std::u64::MAX).expect("Failed to wait for index upload!");
    //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
    let vertices = vec![
        Vertex {
            pos: [0.0, 1.0, 0.0, 1.0],
            uv: [0.0, 1.0],
        },
        Vertex {
            pos: [1.0, 1.0, 0.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [1.0, 0.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [0.0, 0.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
    ];

    let (vertex_upload, vertex_buffer) = Buffer::from_data(
        device.clone(),
        queue.clone(),
        vertices,
        BufferUsage {
            vertex_buffer: true,
            ..Default::default()
        },
        SharingMode::Exclusive,
        MemoryUsage::GpuOnly, //we only want device access for this buffer.
    )
        .expect("Failed to create index buffer");

    vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");

    (vertex_buffer, index_buffer)
}

struct CharacterBuffer{
    index_buffer: Arc<Buffer>,
    index_buffer_length: usize,
    vertex_buffer: Arc<Buffer>,
    ///The extent this character has including the space between this character and the next character.
    unit_extent: (f32, f32),
}

///Manages all loaded buffers for a font.
/// This is done lazy, so for every loaded font the vertex and index buffer for a certain character are generated when they are needed
/// for the first time.
pub struct FontBuffer{
    font: Font<'static>,

    max_vertical_extent: f32,
    baseline_offset: f32, //Where within the max_vertical_extent (from the top) the base line is
    buffer: HashMap<char, CharacterBuffer>,
}

impl FontBuffer{
    
    pub fn from_font(font: Font<'static>) -> Self{

	let v_metric = font.v_metrics(rusttype::Scale{x: 1.0, y: 1.0});
	let max_vertical_extent = v_metric.ascent.abs() + v_metric.descent.abs();
	
	FontBuffer{
	    font,
	    max_vertical_extent,
	    baseline_offset: v_metric.descent,
	    buffer: HashMap::new(),
	}
    }

    ///Returns the width of this character and the maximum extent. This area can be used to describe the
    ///Area in which a character is rendered.
    fn unit_length(&self, ch: char) -> (Unit, Unit){
	if ch == ' ' {
	    return (0.5, self.max_vertical_extent)
	}
	
	if let Some(cha) = self.buffer.get(&ch){
	    return (cha.unit_extent.0, self.max_vertical_extent);
	}


	//No char found, return the font reported values which is sometimes incorrect. tho
	let glype = self.font.glyph(ch)
	    .scaled(rusttype::Scale{x: 1.0, y: 1.0})
	    .positioned(rusttype::Point{x: 0.0, y: 0.0});
	
	//Calc generic extent
	let extent = (
	    glype.unpositioned().h_metrics().advance_width,
	    //width,
	    self.max_vertical_extent,
	);	
	extent
    }

    fn load_glyphe(&mut self, ch: char, device: Arc<Device>, command_buffer: Arc<CommandBuffer>){
	let glype = self.font.glyph(ch)
	    .scaled(rusttype::Scale{x: 1.0, y: 1.0})
	    .positioned(rusttype::Point{x: 0.0, y: 0.0});

	//Unit extent of the char including space
	let mut unit_extent = self.unit_length(ch);

	
	let mirror_axis = (unit_extent.0 / 2.0, unit_extent.1 / 2.0);
	
	let contours = if let Some(sh) = glype.shape(){
	    sh
	}else{
	    println!("Could not load shape for glype {} from font", ch);

	    vec![rusttype::Contour{
		segments: vec![
		    rusttype::Segment::Line(rusttype::Line{p: [
			rusttype::Point{x: 0.0, y: 0.05},
			rusttype::Point{x: 0.95, y: 1.0},
		    ]}),
		    rusttype::Segment::Line(rusttype::Line{p: [
			rusttype::Point{x: 0.95, y: 1.0},
			rusttype::Point{x: 1.0, y: 0.95},
		    ]}),
		    rusttype::Segment::Line(rusttype::Line{p: [
			rusttype::Point{x: 1.0, y: 0.95},
			rusttype::Point{x: 0.05, y: 0.0},
		    ]}),
		    rusttype::Segment::Line(rusttype::Line{p: [
			rusttype::Point{x: 0.05, y: 0.0},
			rusttype::Point{x: 0.0, y: 0.05},
		    ]}),
		]
	    }]
	};

	//Now generate a vertex buffer via lyon from the supplied contours. Each contour contains
	// a closed loop

	let mut builder = Path::builder();
	
	for con in contours{
	    //Start new shape
	    let mut is_first = true;
	    for seg in con.segments{
		if is_first{
		    let move_location = match seg{
			rusttype::Segment::Curve(c) => point(c.p[0].x, c.p[0].y),
			rusttype::Segment::Line(l) => point(l.p[0].x, l.p[0].y)
		    };
		    //Move to start point
		    builder.move_to(move_location);
		    is_first = false;
		}

		//Now add path segment to builder
		match seg{
		    rusttype::Segment::Curve(c) => {
			//Add quadric end and handle point
			builder.quadratic_bezier_to(
			    point(c.p[1].x,c.p[1].y), //ctrl point
			    point(c.p[2].x, c.p[2].y)
			);
		    },
		    rusttype::Segment::Line(l) => {
			//Draw simple line to end point, since the start has to be the end point of the last line
			builder.line_to(point(l.p[1].x, l.p[1].y));
		    }
		}
	    }
	    //Close this contour
	    builder.close();
	}

	//At this point the build should have the finished lines
	let path = builder.build();
	let mut geometry: VertexBuffers<Vertex, u32> = VertexBuffers::new();
	let mut tessellator = FillTessellator::new();
	
        // Compute the tessellation.
        tessellator.tessellate_path(
	    &path,
	    &FillOptions::default().with_tolerance(0.005),
	    &mut BuffersBuilder::new(&mut geometry, |pos: Point, _: FillAttributes| {

		let mut xy = pos.to_array();		
		//Note: Since the vulkan coordinate system on screen also has its origin in the top left and
		// extents from there downwards and to the right, we have to mirror the outputet characters on the middle x axis and the y axis

		
		xy = [
		    xy[0], //mirror_axis.0 + (mirror_axis.0 - xy[0]),
		    mirror_axis.1 + (mirror_axis.1 - xy[1]),
		];

		//Now also offset the glyph by the calculated ammount
		xy[1] += self.baseline_offset;
		
                Vertex {
		    pos: [xy[0], xy[1], 0.0, 1.0],
		    uv: [xy[0], xy[1]],
                }
	    }),
        ).expect("Failed to tessellate character!");

	let mut vertices: Vec<Vertex> = geometry.vertices;
	//Sadly at this point there is another fix we have to make: The reported width of a character is not always the actual width, therefor
	//we widen the vertexbuffer based on the actual width to the reported width.
	let mut min_x = std::f32::MAX;
	let mut max_x = std::f32::MIN;

	let mut min_y = std::f32::MAX;
	let mut max_y = std::f32::MIN;	
	for v in vertices.iter(){
	    min_x = min_x.min(v.pos[0]);
	    max_x = max_x.max(v.pos[0]);
	    
	    min_y = min_y.min(v.pos[1]);
	    max_y = max_y.max(v.pos[1]);
	}

	if unit_extent.0 < (max_x - min_x){
	    unit_extent.0 = max_x - min_x;
	}
	

	//Scale the vertices to the range of 0-1
	let scaling_factor = 1.0 / unit_extent.0;
	for v in vertices.iter_mut(){
	    v.pos[0] -= min_x;
	    v.pos[0] *= scaling_factor;
	}

	
	let indices: Vec<u32> = geometry.indices;
	let index_buffer_length = indices.len();

	//After tessellating, build the buffers and upload to gpu
	let vertex_buffer = Buffer::new(
	    device.clone(),
	    (std::mem::size_of::<Vertex>() * vertices.len()) as u64,
	    BufferUsage{
		transfer_dst: true,
		vertex_buffer: true,
		.. Default::default()
	    },
	    SharingMode::Exclusive,
	    MemoryUsage::GpuOnly
	).expect("Failed to create vertex buffer for character!");
	
	let index_buffer = Buffer::new(
	    device.clone(),
	    (std::mem::size_of::<u32>() * indices.len()) as u64,
	    BufferUsage{
		transfer_dst: true,
		index_buffer: true,
		.. Default::default()
	    },
	    SharingMode::Exclusive,
	    MemoryUsage::GpuOnly
	).expect("Failed to create index buffer for character!");

	//Schedule upload
	vertex_buffer.copy_sync(
	    command_buffer.clone(),
	    vertices
	).expect("Failed to upload character vertex buffer!");
	index_buffer.copy_sync(
	    command_buffer.clone(),
	    indices
	).expect("Failed to upload character index buffer!");

	//Now move the buffers and all other info into the Character buffer and add it to self.
	self.buffer.insert(ch, CharacterBuffer{
	    vertex_buffer,
	    index_buffer,
	    index_buffer_length,
	    unit_extent: (unit_extent.0, self.max_vertical_extent),
	});
    }
}

pub struct CharacterManager{
    fonts: HashMap<FontName, FontBuffer>,
    fallback_buffer: CharacterBuffer,
    text_pipeline: TextPipeline,
    scheduled_loads: RwLock<Vec<(FontName, char)>>,
}


impl CharacterManager{
    ///Creates a new default character manager. At least one default font is loaded which can be accessed via the font name: `FontName::default()`.
    pub fn new(
	device: Arc<Device>,
	queue: Arc<Queue>,
	renderpass: Arc<RenderPass>,
	msaa_flag: vk::SampleCountFlags
    ) -> Self{
	let mut fonts = HashMap::new();

	fonts.insert(FontName::default(), FontBuffer::from_font(get_default_regular()));

	let (vertex_buffer, index_buffer) = get_fallback_buffer(device.clone(), queue.clone());
	let fallback_buffer = CharacterBuffer{
	    unit_extent: (1.0, 1.0),
	    index_buffer,
	    vertex_buffer,
	    index_buffer_length: 6,
	};
	
	CharacterManager{
	    fonts,
	    fallback_buffer,
	    text_pipeline: TextPipeline::new(device.clone(), renderpass, msaa_flag),
	    scheduled_loads: RwLock::new(Vec::new())
	}
    }

    pub fn load_font(&mut self, font: FontName, path: &PathBuf) -> Result<(), BackendError>{
	let file = if let Ok(file) = std::fs::File::open(path){
	    file
	}else{
	    return Err(BackendError::FailedToLoadFont);
	};

	
	let mut reader = std::io::BufReader::new(file);

	let mut bytes: Vec<u8> = Vec::new();

	if let Err(_) = reader.read_to_end(&mut bytes){
	    return Err(BackendError::FailedToLoadFont);
	}
	
	let loaded_font = if let Ok(lf) = Font::from_bytes(bytes){
	    lf
	}else{
	    return Err(BackendError::FailedToLoadFont)
	};

	self.fonts.insert(font, FontBuffer::from_font(loaded_font));
	Ok(())
	
    }
    
    ///When called each pending buffer loading request is scheduled to the command buffer. Then a Pipeline barrier is attached to
    /// assure that each following draw command only operates on actually loaded buffers.
    pub fn update(&mut self, device: Arc<Device>, command_buffer: Arc<CommandBuffer>){
	let mut update_vec = Vec::with_capacity(self.scheduled_loads.read().unwrap().len());
	std::mem::swap(&mut update_vec, &mut self.scheduled_loads.write().expect("Failed to swap load calls"));

	for (font_name, ch) in update_vec{
	    if let Some(f) = self.fonts.get_mut(&font_name){
		f.load_glyphe(ch, device.clone(), command_buffer.clone());
	    }else{
		println!("DEBUG: Tried to load font character from not present font!");
	    }
	}

	//Now schedule buffer barrier
	command_buffer.cmd_pipeline_barrier(
            vk::PipelineStageFlags::TRANSFER,
            vk::PipelineStageFlags::TOP_OF_PIPE,
            vk::DependencyFlags::empty(),
            vec![],
            vec![],
            vec![]
        ).expect("Failed to add pipeline barrier while copying");
    }
    
    ///Tries to get a character buffer for the specified font and char. If none is found, the default one is returned (an visual square).
    /// and loading of the correct one is scheduled.
    fn get_buffer(&self, font: &FontName, ch: char) -> &CharacterBuffer{
	if let Some(fo) = self.fonts.get(font){
	    if let Some(c) = fo.buffer.get(&ch){
		&c
	    }else{
		self.scheduled_loads.write().expect("Could not schedule char load").push((font.clone(), ch));
		&self.fallback_buffer
	    }
	}else{
	    //Could not find the font which was searched for, try to fall back to the default font
	    if let Some(c) = self.fonts.get(&FontName::default()).expect("Could not get default font, should not happen").buffer.get(&ch){
		c
	    }else{
		//The default font does not have the char loaded as well therefore return the fallback character.
		//Also schedule load for the default and the called for font.
		self.scheduled_loads.write().expect("Could not schedule char load").push((FontName::default(), ch));
		self.scheduled_loads.write().expect("Could not schedule char load").push((font.clone(), ch));
		&self.fallback_buffer
	    }   
	}
    }
    
    pub fn draw(
	&self,
	command_buffer: Arc<CommandBuffer>,
	prims: Vec<(Area, Option<Area>, Style, f32, Character)>,
	screen_area: Area
    ){

	//Collects all draw calls sorted first according to the font, and then based on the character.
	// This way there have to be as less context switches as possible
	// (until i figure instancing out :D)
	let mut calls: HashMap<FontName, HashMap<char, Vec<(TextPush, vk::Rect2D)>>> = HashMap::new();

	for (drawing_area, scissors, style, level, character) in prims.into_iter(){

	    if character.ch == ' '{
		continue;
	    }
	    
	    let char_buffer = self.get_buffer(&character.font, character.ch);
	    let character_area = Area{
		from: drawing_area.from,
		to: (
		    drawing_area.from.x + (char_buffer.unit_extent.0 * character.size as f32),
		    drawing_area.from.y + (char_buffer.unit_extent.1 * character.size as f32)
		).into()
	    };

	    let final_area = drawing_area.union(character_area);

	    if final_area.has_zero_dim(){
		continue;
	    }

	    let scissors_area = if let Some(sc) = scissors{
		sc
	    }else{
		final_area
	    };
	    
	    //Create the draw call for this char
	    let draw_call = (
		TextPush{
		    transform: character_area.to_transform(&screen_area),
		    color: style.color,
		    power: style.power,
		    level,
		    pad1: [0.0; 2]
		},
		area_to_scissors(scissors_area, screen_area)
	    );

	    //Now check if there is allready a collection for the font name.
	    if let Some(font_call_collection) = calls.get_mut(&character.font){
		//create the draw call, if not already present, create a new call vec, otherwise just push
		if let Some(ch_draw_calls) = font_call_collection.get_mut(&character.ch){
		    //There are already calls for this char, insert
		    ch_draw_calls.push(draw_call);
		}else{
		    font_call_collection.insert(character.ch, vec![draw_call]);
		}
		
	    }else{
		//There is no collection for this font, therefore add our character call
		let mut font_hash = HashMap::new();
		font_hash.insert(
		    character.ch,
		    vec![draw_call]
		);
		
		calls.insert(
		    character.font.clone(),
		    font_hash
		);
	    }
	}

	//With the drawcalls sorted by font and character, issue a draw call for each of them
	for (font, font_calls) in calls.into_iter(){
	    for(ch, character_calls) in font_calls.into_iter(){
		let char_buffer = self.get_buffer(&font, ch);

		let index_buf = char_buffer.index_buffer.clone();
		let vertex_buffer = char_buffer.vertex_buffer.clone();
		let index_buffer_len = char_buffer.index_buffer_length;
		/*
		//Debug lower a little and draw rect behind
		for call in character_calls.iter_mut(){
		    call.0.level -= 0.1;
		}

		//Draw again with background rect
		self.surface_pipeline.draw(
		    (6, self.fallback_buffer.index_buffer.clone()),
		    self.fallback_buffer.vertex_buffer.clone(),
		    &character_calls,
		    command_buffer.clone()
		);

		for call in character_calls.iter_mut(){
		    call.0.level += 0.1;
		    call.0.color = [1.0, 0.0, 0.0, 1.0];
		}
		*/
		self.text_pipeline.draw(
		    (index_buffer_len, index_buf),
		    vertex_buffer,
		    &character_calls,
		    command_buffer.clone()
		);
	    }
	}
    }

    pub fn get_area_for_char(&self, ch: &Character) -> Area{
	
	//Try to get the char buffer and just read the unit scale.
	//If that fails, we schedule a load, but also read the 
	if let Some(fo) = self.fonts.get(&ch.font){

	    if ch.ch == ' '{
		let unit = fo.unit_length(ch.ch);
		return Area{
		    from: (0.0, 0.0).into(),
		    to: (unit.0 * ch.size as f32, unit.1 * ch.size as f32).into()
		}
	    }
	    
	    if let Some(c) = fo.buffer.get(&ch.ch){
		Area{
		    from: (0.0, 0.0).into(),
		    to: (
			(c.unit_extent.0 * ch.size as f32),
			(c.unit_extent.1 * ch.size as f32),
		    ).into()
		}
	    }else{
		//The Font is present, but we do not have the area.
		//Shedule a load for the character, also create a tmp glyphe and read the area the future loaded glyphe will have
		let unit_length = fo.unit_length(ch.ch);
		self.scheduled_loads.write().expect("Failed to Schedule font load!").push((ch.font.clone(), ch.ch));
		Area{
		    from: (0.0, 0.0).into(),
		    to: (
			(unit_length.0 * ch.size),
			(unit_length.1 * ch.size),
		    ).into()
		}
	    }
	}else{
	    //If the font is loaded, we cannot do anything, therefore return the normal unit scale.
	    println!("NEITH-MARP: Warning: Tried to get area of font that is not loaded, returning unit size!");
	    Area{
		from: (0.0, 0.0).into(),
		to: (ch.size, ch.size).into()
	    }
	}
    }
}
