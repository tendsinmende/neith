use marp::ash::vk::Rect2D;
use neith::math::Vector2;
use neith::primitive::PolyHandle;
use neith::widget::Style;
use neith::math::Area;
use crate::pipelines::surface::{SurfacePipeline, SurfacePush};
use crate::vulkan_prelude::*;
use std::collections::HashMap;
use std::sync::Arc;

use crate::command_builder::Vertex;

struct PolyBuffer{
    vertex_buffer: Arc<Buffer>,
    index_buffer: Arc<Buffer>,
    index_buffer_length: usize,
}

///Polygone primitive. Uses the `surface` shader to draw some polygone buffer
pub struct Poly{
    pipeline: SurfacePipeline,
    ///Holds currently to draw polys
    polys: HashMap<&'static str, PolyBuffer>,

    device: Arc<Device>,
    queue: Arc<Queue>,
}

impl Poly{
    pub fn new(
	device: Arc<Device>,
	queue: Arc<Queue>,
	render_pass: Arc<RenderPass>,
	msaa_flag: vk::SampleCountFlags
    ) -> Self{	
	let pipeline = SurfacePipeline::new(device.clone(), render_pass, msaa_flag);

	Poly{
	    pipeline,
	    polys: HashMap::new(),
	    device,
	    queue
	}
    }

    ///Registers a new polygone that can be drawn
    pub fn register_polygone(&mut self, name: &'static str, vertices: Vec<Vector2<f32>>, indices: Vec<u32>) -> PolyHandle{
	//Check if there is already a polygone with this name. If that's the case, return it.
	if self.polys.contains_key(name){
	    return PolyHandle::new(name);
	}
	
	let new_handle = PolyHandle::new(name);
	let index_buffer_length = indices.len();
	
	debug_assert!(indices.len() % 3 == 0, "index buffer length had not a multiple of 3 length");
	
        let (index_upload, index_buffer) = Buffer::from_data(
            self.device.clone(),
            self.queue.clone(),
            indices,
            BufferUsage {
                index_buffer: true,
                //storage_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly //we only want device access for this buffer.
        ).expect("Failed to create index buffer");

	index_upload.wait(std::u64::MAX).expect("Failed to wait for index upload!");
        //We go from 0.0 to 1.0, since we then can scale appropriate and afterwards move the rectangle to the right position
        let vertices = vertices.into_iter().map(|v| Vertex{pos: [v.x, v.y, 0.0, 1.0], uv: [0.0, 0.0]}).collect();
        let (vertex_upload, vertex_buffer) = Buffer::from_data(
            self.device.clone(),
            self.queue.clone(),
            vertices,
            BufferUsage {
                vertex_buffer: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly, //we only want device access for this buffer.
        ).expect("Failed to create index buffer");

        vertex_upload.wait(std::u64::MAX).expect("Failed to wait for vertex upload!");
	

	//Setup the poly buffer
	self.polys.insert(name, PolyBuffer{
	    index_buffer,
	    vertex_buffer,
	    index_buffer_length,
	});

	new_handle
    }

    ///Draws a rectangle at `location` with given extent
    ///Screen area is the area in pixels that is covered by the screen.
    pub fn draw_at(
	&self,
	screen_area: Area,
	calls: Vec<(Area, Option<Area>, Style, f32, PolyHandle)>,
    	command_buffer: Arc<CommandBuffer>,
    ){

	//collects calls under the same poly handle for bundeled draw call. Kinda ineffective atm.
	let mut call_bundles: HashMap<&'static str, Vec<(SurfacePush, Rect2D)>> = HashMap::new();

	for (area, scissors, style, level, poly) in calls.into_iter(){
	    //Create the transform for this call
	    let transform = area.to_transform(&screen_area);
	    
	    let push_buf = SurfacePush{
		transform,
		color: style.color,
		power: style.power,
		level,
		pad1: [0.0; 2]
	    };

	    let scissors_area = if let Some(sc) = scissors{
		sc
	    }else{
		area
	    };

	    let scissors_area = area_to_scissors(scissors_area, screen_area);
	    
	    //Now either push the call, or if the id was not yet used, create a push set
	    if call_bundles.contains_key(&poly.id){
		call_bundles.get_mut(&poly.id).unwrap().push((push_buf, scissors_area));
	    }else{
		call_bundles.insert(poly.id, vec![(push_buf, scissors_area)]);
	    }
	}

	//Draw each call collection based on the registered vertex/index buffer
	for (key, pushconst_scissors) in call_bundles.into_iter(){
	    self.pipeline.draw(
		(self.polys.get(key).unwrap().index_buffer_length, self.polys[key].index_buffer.clone()),
		self.polys[key].vertex_buffer.clone(),
		&pushconst_scissors,
		command_buffer.clone()
	    )
	}
	/*
	self.pipeline.draw(
	    (6, self.index_buffer.clone()),
	    self.vertex_buffer.clone(),
	    &push_scissors,
	    command_buffer
	);
	 */
    }
}



