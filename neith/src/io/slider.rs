use crate::Interface;
use crate::com::MutVal;
use crate::event::Event;
use crate::event::WindowEvent;
use crate::layout::Align;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use std::time::Instant;

use crate::io::{
    Value,
    DynamicValue
};
use crate::layout::Direction;

use super::Label;

///A slider that mutates a value of type `T` in some range.
pub struct Slider<T>{
    pub value: Value<T>,
    pub step_size: T,
    pub min: T,
    pub max: T,

    pub direction: Direction,
    
    pub preferred_size: Option<Vector2<Unit>>,

    ///Contains a possible label that is updated when the value changes.
    label_align: Option<Box<Align<Label>>>,
    postfix: Option<String>,

    ///If set an action that is executed on the inner value every time the slider changes.
    on_change: Option<Box<dyn FnMut(&mut Value<T>) + Send + 'static>>,
    
    area: Area,
}

impl<T: DynamicValue + Send + Sync> Slider<T>{
    pub fn new(initial_value: T, min: T, max: T, step_size: T) -> Self{
	Slider{
	    step_size,
	    min,
	    max,
	    value: Value::Own(initial_value),

	    direction: Direction::Horizontal,
	    
	    preferred_size: None,

	    label_align: None,
	    postfix: None,

	    on_change: None,
	    
	    area: Area::empty()
	}
    }

    pub fn with_direction(mut self, dir: Direction) -> Self{
	self.direction = dir;
	self
    }
    
    pub fn with_size(mut self, size: (Unit, Unit)) -> Self{
	self.preferred_size = Some(size.into());
	self
    }
    
    pub fn with_mut_val(mut self, mv: MutVal<T>) -> Self{
	self.value = Value::MutVal(mv);
	self.update_label();
	self
    }

    pub fn with_on_change<A>(mut self, action: A) -> Self where A: FnMut(&mut Value<T>) + Send + 'static{
	self.on_change = Some(Box::new(action));
	self
    }

    fn update_label(&mut self){

	
	let st = match &self.value{
	    Value::Own(t) => t.to_string(),
	    Value::MutVal(mt) => mt.get().to_string()
	};

	if let Some(label_align) = &mut self.label_align{
	    if let Some(pf) = &self.postfix{
		label_align.child.set_text(format!("{}{}", st, pf));
	    }else{
		label_align.child.set_text(format!("{}", st));	
	    }
	}
    }
    
    pub fn with_label(mut self, label: Label, postfix: Option<&str>) -> Self{
	let align = Box::new(Align::new(label));
	
	self.label_align = Some(align);
	self.postfix = if let Some(pf) = postfix{
	    Some(pf.to_owned())
	}else{
	    None
	};

	self.update_label();
	
	self
    }

    pub fn set_value(&mut self, new: T){
	match &mut self.value{
	    Value::MutVal(t) => *t.get_mut() = new,
	    Value::Own(t) => *t = new,
	}

	self.update_label();
    }
}

impl<T: DynamicValue + Send + Sync> Widget for Slider<T>{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, _location: &Vector2<Unit>){
	match event{
	    Event::WindowEvent(WindowEvent::CursorDrag{start, end}) => {
		//Calculate based on the current position of the cursor how many steps from min to max the value would have taken.
		let border_width = crate::get_config().border_highlight_width as Unit;
		let slider_area = self.area
		    .shrink_from((-border_width, -border_width).into())
		    .grow_to((-border_width, -border_width).into());

		if slider_area.is_in(&Vector2::from(*start)){
		    match self.direction{
			Direction::Horizontal => {
			    let percentage = (end.0 / slider_area.extent().x).max(0.0).min(1.0);
			    self.set_value(T::value_for_percentage(&self.min, &self.max, &self.step_size, percentage));
			},
			Direction::Vertical => {
			    let percentage = (end.1 / slider_area.extent().y).max(0.0).min(1.0);
			    self.set_value(T::value_for_percentage(&self.min, &self.max, &self.step_size, percentage));
			}
		    }

		    if let Some(ch_action) = &mut self.on_change{
			ch_action(&mut self.value);
		    }
		}
	    },
	    _ => {}
	}
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {

	self.area = if let Some(pref) = self.preferred_size{
	    (Area{
		from: (0.0, 0.0).into(),
		to: host_extent
	    }).union(Area{from: (0.0, 0.0).into(), to: pref})
	}else{
	    Area{
		from: (0.0, 0.0).into(),
		to: host_extent
	    }
	};

	let border_width = crate::get_config().border_highlight_width as Unit;
	let slider_area = self.area
	    .shrink_from((-border_width, -border_width).into())
	    .grow_to((-border_width, -border_width).into());

	let slider_percentage = match &self.value{
	    Value::MutVal(t) => t.get().percentage_for_value(&self.min, &self.max),
	    Value::Own(t) => t.percentage_for_value(&self.min, &self.max)
	};
	
	let active_area = Area{
	    from: slider_area.from,
	    to: match self.direction{
		Direction::Horizontal => (
		    slider_area.from.x + slider_area.extent().x * slider_percentage,
		    slider_area.to.y
		).into(),
		Direction::Vertical => (
		    slider_area.to.x,
		    slider_area.from.y + slider_area.extent().y * slider_percentage
		).into()
	    }
	};
	
	let mut calls = DrawCalls::new(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().highlight),
		level: level
	    },
	    Prim{
		prim: PrimType::Rect,
		drawing_area: slider_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level + 1
	    },
	    Prim{
		prim: PrimType::Rect,
		drawing_area: active_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background_selected),
		level: level + 2
	    },
	]);

	if let Some(l) = &mut self.label_align{	    
	    let mut text_calls = l.get_calls(slider_area.extent(), draw_time, interface, level + 3);
	    text_calls.offset((border_width, border_width).into());
	    calls.merge(text_calls);
	}

	calls
	
    }
}
