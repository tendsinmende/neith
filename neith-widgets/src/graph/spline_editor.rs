//! The Spline editor allows manipulating a spline within the x/y range of [0.0, 1.0]. The Spline can be sampled at any pointer.

use std::time::Instant;

use neith::{Interface, event::KeyCode};
use neith::config::Color;
use neith::primitive::Lines;
use neith::primitive::Prim;
use neith::primitive::PrimType;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;
use splines::Interpolation;
use splines::Key;
use splines::Spline;
use neith::event::Event;
use neith::math::Area;
use neith::event::KeyState;
use neith::event::MouseButton;
use neith::math::Unit;
use neith::math::Vector2;
use neith::event::WindowEvent;

use super::Graph;


const SAMPLING_WIDTH: f32 = 2.0;
const KEY_SIZE: f32 = 10.0;
const MOD: neith::event::KeyCode = neith::event::KeyCode::LShift;

pub struct SplineEditor{
    spline: Spline<f32, f32>,
    free_keys: Vec<Key<f32, f32>>,
    protected_keys: Vec<Key<f32, f32>>,

    cached_keys: Vec<(Color, [Vector2<Unit>; 5], Area)>,

    ///The region in which keys can be placed on x/y
    key_region: ((f32, f32), (f32, f32)),
    
    //Some if a key is being dragged
    dragging_key: Option<usize>,

    //true if pressing mod key
    is_mod: bool,
    
    area: Area,
}

impl SplineEditor{
    pub fn new(min: (f32, f32), max: (f32, f32)) -> Self{

	//Start with one key in the middl
	let first_key = Key::new((0.5 as f32).min(min.0).max(max.0), (0.5 as f32).min(min.1).max(max.1), Interpolation::CatmullRom);
	
	SplineEditor{
	    spline: Spline::from_vec(vec![]),
	    free_keys: vec![first_key],
	    protected_keys: vec![],
	    cached_keys: vec![],
	    key_region: (min, max),
	    dragging_key: None,
	    is_mod: false,
	    area: Area::empty(),
	}
    }

    ///adds unmovable keys, note that they are clamped to the min/max values of the initialization
    pub fn with_protected_keys(mut self, keys: Vec<(f32, f32)>) -> Self{
	self.protected_keys = keys.into_iter().map(|(kx,ky)| Key::new(
	    kx.min(self.key_region.1.0).max(self.key_region.0.0),
	    ky.min(self.key_region.1.1).max(self.key_region.0.1),
	    Interpolation::Cosine
	)).collect();
	self.update_spline();
	self
    }

    ///adds movable keys, note that they are clamped to the min/max values of the initialization
    pub fn with_free_keys(mut self, keys: Vec<(f32, f32)>) -> Self{
	self.free_keys = keys.into_iter().map(|(kx,ky)| Key::new(
	    kx.min(self.key_region.1.0).max(self.key_region.0.0),
	    ky.min(self.key_region.1.1).max(self.key_region.0.1),
	    Interpolation::Cosine
	)).collect();
	self.update_spline();
	self
    }

    fn update_spline(&mut self){
	let mut collected_keys = self.free_keys.clone();
	collected_keys.append(&mut self.protected_keys.clone());
	self.spline = Spline::from_vec(collected_keys);

	//update key areas
	self.cached_keys = self.get_ctrl_box_lines().into_iter().map(|(c, keys)| {
	    let area = Area{
		from: keys[0],
		to: keys[2]
	    };
	    (c, keys, area)
	}).collect();

    }

    fn key_region_extent(&self) -> (f32, f32){
	(
	    self.key_region.1.0 - self.key_region.0.0,
	    self.key_region.1.1 - self.key_region.0.1,
	)
    }
    
    #[inline]
    ///converts some location on the widget area to a location in the splines space
    fn area_loc_to_spline(&self, mut loc: Vector2<Unit>) -> Vector2<Unit>{

	let area_extent = self.area.extent();
	loc.x = loc.x.max(self.area.from.x).min(self.area.to.x);
	loc.y = loc.y.max(self.area.from.y).min(self.area.to.y);
	
	//Normalize x and y
	loc.x /= area_extent.x;
	loc.y = (area_extent.y - loc.y) / area_extent.y;
	
	//expand to spline area
	loc.x = self.key_region.0.0 + (self.key_region_extent().0 * loc.x);
	loc.y = self.key_region.0.1 + (self.key_region_extent().1 * loc.y);

	loc
    }
    
    #[inline]
    ///converts some location in the spline area to some location within the widgets area
    fn loc_spline_to_area(&self, loc: Vector2<Unit>) -> Vector2<Unit>{
	let area = self.area;
	let area_extent = area.extent();

	let norm_x = (loc.x - self.key_region.0.0) / self.key_region_extent().0;
	let norm_y = (loc.y - self.key_region.0.1) / self.key_region_extent().1;

	let p = Vector2::new(
	    area.from.x + area_extent.x * norm_x,
	    area.from.y + area_extent.y - area_extent.y * norm_y
	);
	p
    }

    ///Creates the control point boxes for each ctrl point.
    fn get_ctrl_box_lines(&self) -> Vec<(Color, [Vector2<Unit>; 5])>{
	let mut color_key_pairs = Vec::with_capacity(self.free_keys.len() + self.protected_keys.len());
	for fk in self.free_keys.iter(){

	    let pt = self.loc_spline_to_area(Vector2::new(fk.t, fk.value)); 
	    
	    color_key_pairs.push((
		Color::Float([0.8, 0.8, 0.2]),
		[
		    pt + Vector2::new(-0.5*KEY_SIZE, -0.5*KEY_SIZE),
		    pt + Vector2::new(0.5*KEY_SIZE, -0.5*KEY_SIZE),
		    pt + Vector2::new(0.5*KEY_SIZE, 0.5*KEY_SIZE),
		    pt + Vector2::new(-0.5*KEY_SIZE, 0.5*KEY_SIZE),
		    pt + Vector2::new(-0.5*KEY_SIZE, -0.5*KEY_SIZE),
		]
	    ))
	}

	for pk in self.protected_keys.iter(){
	    let pt = self.loc_spline_to_area(Vector2::new(pk.t, pk.value)); 
	    color_key_pairs.push((
		Color::Float([0.5, 0.2, 0.2]),
		[
		    pt + Vector2::new(-0.5*KEY_SIZE, -0.5*KEY_SIZE),
		    pt + Vector2::new(0.5*KEY_SIZE, -0.5*KEY_SIZE),
		    pt + Vector2::new(0.5*KEY_SIZE, 0.5*KEY_SIZE),
		    pt + Vector2::new(-0.5*KEY_SIZE, 0.5*KEY_SIZE),
		    pt + Vector2::new(-0.5*KEY_SIZE, -0.5*KEY_SIZE),
		]
	    ))
	}

	
	color_key_pairs
    }

    ///returns the y at some x. Returns None if out of range
    pub fn get_val_at(&self, x: f32) -> Option<f32>{
	self.spline.sample(x)
    }

    ///Creates the line that forms this spline.
    fn get_line_points(&self, area: Area) -> Vec<Vector2<Unit>>{
	let area_extent = area.extent();
	let num_samples = (area_extent.x / SAMPLING_WIDTH as Unit).ceil() as usize;

	let t_per_sample = (1.0 / num_samples as Unit) * self.key_region_extent().0;
	let mut t = self.key_region.0.0;
	let mut points = Vec::with_capacity(num_samples);

	for _i in 0..num_samples{
	    let sample_y = if let Some(y) = self.spline.sample(t){
		y
	    }else{
		0.0
	    };
	    
	    points.push(self.loc_spline_to_area(Vector2::new(t, sample_y)));
	    t += t_per_sample;
	}	
	points
    }
}

impl Graph for SplineEditor{}
impl Widget for SplineEditor{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	let mut key_changed = false;
	match event{
	    Event::WindowEvent(WindowEvent::Key(neith::event::Key{code: MOD, state})) => {
		match state{
		    KeyState::Pressed => self.is_mod = true,
		    KeyState::Released => self.is_mod = false,
		}
	    },
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Right, state: KeyState::Released}) => {
		//if right was released, check if we can delete a key here
		let mut deletion_index = None;
		for (idx, k) in self.cached_keys.iter().enumerate(){
		    if k.2.is_in(location) && idx < self.free_keys.len(){
			deletion_index = Some(idx);
			break;
		    }
		}

		if let Some(del_idx) = deletion_index{
		    self.free_keys.remove(del_idx);
		    key_changed = true;
		}
		
	    },
	    Event::WindowEvent(WindowEvent::CursorMoved(to)) => {
		if let Some(dragging_key) = self.dragging_key{

		    let new_key_loc = self.area_loc_to_spline(Vector2::new(to.0 as Unit, to.1 as Unit));
		    
		    self.free_keys[dragging_key].t = new_key_loc.x as f32;
		    self.free_keys[dragging_key].value = new_key_loc.y as f32;
		    key_changed = true;
		}
	    }
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state: KeyState::Released}) => {
		self.dragging_key = None;
	    }
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state: KeyState::Pressed}) => {
		for (idx, k) in self.cached_keys.iter().enumerate(){
		    if k.2.is_in(location){
			//since the cache list starts with free keys, check if the index is in the free key range.
			if idx < self.free_keys.len(){
			    self.dragging_key = Some(idx);
			    break;
			}
		    }
		}

		//if we did not start tracking something, add a key here if the mod key is pressed
		if self.is_mod && self.dragging_key.is_none(){

		    let key_loc = self.area_loc_to_spline(*location);
		    
		    self.free_keys.push(Key::new(
			key_loc.x.min(self.key_region.1.0).max(self.key_region.0.0),
			key_loc.y.min(self.key_region.1.1).max(self.key_region.0.1),
			Interpolation::Cosine
		    ));

		    key_changed = true;
		}
		
	    }
	    _ => {}
	}

	if key_changed{
	    self.update_spline();
	}
    }
    
    fn area(&self) -> Area {
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	_draw_time: Instant,
	_interface: &Interface,
	mut level: usize
    ) -> DrawCalls {
	let new_area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};
	
	if new_area != self.area{
	    self.area = new_area;
	    self.update_spline();
	}

	let mut prims = Vec::new();


	if self.area.from.x > self.area.to.x || self.area.from.y > self.area.to.y{
	    //Cannot draw diagram, therefore return already
	    return DrawCalls::new(prims);
	}

	
	level += 1;


	let located_points = self.get_line_points(self.area);
	let style = Style::from_color(Color::Float([0.8, 0.8, 0.2]));
	prims.push(
	    Prim{
		prim: PrimType::Lines(Lines{
		    points: located_points,
		    width: 2.0,
		}),
	        drawing_area: self.area,
	        scissors: None,
	        style,
	        level,
	    }
	);

	level += 1;

	//Draw SplinePoints
	for (c, lines, _sarea) in self.cached_keys.iter(){
	    prims.push(
		Prim{
		    prim: PrimType::Lines(Lines{
			points: lines.to_vec(),
			width: 1.5,
		    }),
	            drawing_area: self.area,
	            scissors: None,
	            style: Style::from_color(c.clone()),
	            level,
		}
	    )
	}
	DrawCalls::new(prims)
    }
}
