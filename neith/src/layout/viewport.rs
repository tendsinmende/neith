use crate::Interface;
use crate::event::Event;
use crate::event::KeyState;
use crate::event::MouseButton;
use crate::event::WindowEvent;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use std::time::Instant;

///A Viewport can contains a widget which exceeds the actual area it is contained in.
///This is made possible by adding appropriated scroll bars to the viewport to select the area which should be shown.
pub struct Viewport<T> where T: Widget + Send + 'static{
    pub child: T,
    
    //The offset of this viewport, relative to the childs (0,0)
    viewport_offset: Vector2<Unit>,

    //last know slider area if a slider was set (horizontal, vertical)
    slider: (Option<Area>, Option<Area>),

    //If the cursor is currently dragging one of the sliders. The usize signals if it is the
    // horizontal slider (0) or the vertical (1), the Unit is the offset from the start of the slider
    // at which the drag start occurred.
    is_dragging: Option<(u8, Unit)>,

    //Area given by parent in which can be drawn.
    area: Area
}

impl<T> Viewport<T> where T: Widget + Send + 'static{
    pub fn new(child: T) -> Self{
	Viewport{
	    child,
	    
	    viewport_offset: (0.0, 0.0).into(),
	    slider: (None, None),
	    
	    is_dragging: None,
	    area: Area::empty()
	}
    }
    
    //Returns the current correct slider areas based on the viewport offset
    fn set_slider_pos(&mut self){
	let child_extent = self.child.area().extent();
	let bar_width = crate::get_config().scroll_handle_width as Unit;

	let drawing_area = self.area;
	
	//Need a horizontal slider
	if self.area.extent().x < child_extent.x{	    
	    //The length of the bar is the percentage our "window" makes in the childs area.
	    // however, we also remove a the bar width from "whole" length, since a vertical bar might
	    // be drawn

	    // "max_width" * "percentage in child"
	    let bar_length = (drawing_area.extent().x - bar_width) * (drawing_area.extent().x / child_extent.x);
	    
	    //Find location of bar based on the current viewport offset.
	    //First find the extent in which the bar can move
	    let dynamic_extent = drawing_area.extent().x - bar_length - bar_width;
	    let child_dynamic_extent = child_extent.x - drawing_area.extent().x; //how much we can move wihtin the child

	    //sanitize offset
	    self.viewport_offset.x = self.viewport_offset.x.max(0.0).min(child_dynamic_extent);
	    
	    let bar_location_percentage = self.viewport_offset.x / child_dynamic_extent;

	    let barlocation = dynamic_extent * bar_location_percentage;

	    //now we know the bars location if it would have a length of bar_length
	    self.slider.0 = Some(Area{
		from: (barlocation, drawing_area.extent().y - bar_width).into(),
		to: ((barlocation + bar_length), drawing_area.extent().y).into()
	    });
	    
	}else{
	    //reset offset
	    self.viewport_offset.x = 0.0;
	    self.slider.0 = None;
	}

	if self.area.extent().y < child_extent.y{

	    // "max_width" * "percentage in child"
	    let bar_length = (drawing_area.extent().y - bar_width) * (drawing_area.extent().y / child_extent.y);
	    
	    //Find location of bar based on the current viewport offset.
	    //First find the extent in which the bar can move
	    let dynamic_extent = drawing_area.extent().y - bar_length - bar_width;
	    let child_dynamic_extent = child_extent.y - drawing_area.extent().y; //how much we can move wihtin the child

	    //sanitize offset
	    self.viewport_offset.y = self.viewport_offset.y.max(0.0).min(child_dynamic_extent);
	    
	    let bar_location_percentage = self.viewport_offset.y / child_dynamic_extent;

	    let barlocation = dynamic_extent * bar_location_percentage;

	    //now we know the bars location if it would have a length of bar_length
	    self.slider.1 = Some(Area{
		from: (drawing_area.extent().x - bar_width, barlocation).into(),
		to: (drawing_area.extent().x, (barlocation + bar_length)).into()
	    });
	    
	}else{
	    //reset offset
	    self.viewport_offset.y = 0.0;
	    self.slider.1 = None;
	}
    }

    fn get_drawing_area(&self) -> Area{
	let bar_width = crate::get_config().scroll_handle_width as Unit;
	let child_extent = self.child.area().extent();
	let mut drawing_area = self.area;
	if self.area.extent().x < child_extent.x{
	    //If we need a horizontal slider, the area we can draw to gets `bar_width` smaller.
	    drawing_area.to.y -= bar_width;
	}
	if self.area.extent().y < child_extent.y{
	    //If we need a horizontal slider, the area we can draw to gets `bar_width` smaller.
	    drawing_area.to.x -= bar_width;
	}
	drawing_area
    }
}

impl<T> Widget for Viewport<T> where T: Widget + Send + 'static{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	//If one of the sliders is turned on, check if there is a draw event that is overlapping, if thats the case,
	//update the slider position as well as the viewport offset accordingly
	
	match event{	    
	    Event::WindowEvent(WindowEvent::CursorDrag{start: _, end}) => {
		
		//If is dragging, change viewport offset accordingly.
		if let Some((slider_id, offset)) = self.is_dragging{		    
		    match slider_id{
			0 => {
			    //This should be the new start of the slider
			    let mut new_x = end.0 - offset;

			    let drawing_area = self.get_drawing_area();
			    let mut max_x = drawing_area.extent().x;
			    
			    //However, this might exceed the slider area, therefore normalize
			    if let Some(hor) = self.slider.0{

				//remove the slider width from max_x as well, since we want to calculate the new offset betweeen virtual and physical
				//viewport
				max_x -= hor.extent().x;
				new_x = new_x.min(max_x);
				//Now find where this new_x would be when it was scaled by the percentage of the virtual viewport,
				//compared to its "physical" size
				let new_offset = (new_x * (self.child.area().extent().x / drawing_area.extent().x)).max(0.0);
				
				self.viewport_offset.x = new_offset;
				self.set_slider_pos();
			    }else{
				//In this case the inner area changed in a way that we should not drag anymore, therefore reset offset and
				//remove slider
				self.viewport_offset.x = 0.0;
				self.is_dragging = None;
			    }
			},
			1 =>{
			    //This should be the new start of the slider
			    let mut new_y = end.1 - offset;

			    let drawing_area = self.get_drawing_area();
			    let mut max_y = drawing_area.extent().y;
			    
			    //However, this might exceed the slider area, therefore normalize
			    if let Some(vert) = self.slider.1{

				//remove the slider width from max_x as well, since we want to calculate the new offset betweeen virtual and physical
				//viewport
				max_y -= vert.extent().y;
				new_y = new_y.min(max_y);
				
				//Now find where this new_y would be when it was scaled by the percentage of the virtual viewport,
				//compared to its "physical" size
				let new_offset = (new_y * (self.child.area().extent().y / drawing_area.extent().y)).max(0.0);

				self.viewport_offset.y = new_offset; 
				self.set_slider_pos();
				
			    }else{
				//In this case the inner area changed in a way that we should not drag anymore, therefore reset offset and
				//remove slider
				self.viewport_offset.y = 0.0;
				self.is_dragging = None;
			    }
			},
			_ => {} //Currently only 2d viewport :D
		    }
		}
	    },
	    
	    //If the left mouse button is release, we always stop dragging the sliders
	    Event::WindowEvent(WindowEvent::MouseClick{button, state}) => {
		match (button, state){
		    (MouseButton::Left, KeyState::Released) => {
			self.is_dragging = None;
		    },
		    (MouseButton::Left, KeyState::Pressed) => {
			//Start dragging
			if let Some(hor) = self.slider.0{
			    let offset = location.x - hor.from.x;
			    if hor.is_in(&(*location).into()){
				self.is_dragging = Some((0, offset));
			    }
			}

			if let Some(vert) = self.slider.1{
			    let offset = location.y - vert.from.y;
			    if vert.is_in(&(*location).into()){
				self.is_dragging = Some((1, offset));
			    }
			}
		    }
		    _ => {},
		}
	    },

	    Event::WindowEvent(WindowEvent::Scroll((mut x, mut y))) => {
		
		//Check if we have an actually 2d signal, if so, probagate to the correct offset.
		//However, most mouses only have one wheel, therefore we map the y motion to horizontal if no y slider is there
		if self.area.is_in(location){

		    let min = self.child.area();

		    x *= -1.0 * crate::get_config().scroll_speed;
		    y *= -1.0 * crate::get_config().scroll_speed;

		    
		    if let Some(_vert) = self.slider.1{
			self.viewport_offset.y = (y + self.viewport_offset.y).max(0.0).min(min.extent().y - self.get_drawing_area().extent().y);
			self.set_slider_pos();
		    }else if let Some(_hor) = self.slider.0{
			//Use x scroll if there is a value, otherwise use the y value, since it was not used for a vertical bar.
			if x == 0.0{
			    self.viewport_offset.x = (y + self.viewport_offset.x).max(0.0).min(min.extent().x - self.get_drawing_area().extent().x);
			}else{
			    self.viewport_offset.x = (x + self.viewport_offset.x).max(0.0).min(min.extent().x - self.get_drawing_area().extent().x);
			}
			self.set_slider_pos();
		    }
		}
		
	    }
	    Event::WindowEvent(WindowEvent::CursorLeft) => self.is_dragging = None,
	    _ => {},
	}

	//If we are currently not dragging, pass events down, however, we have to offset the event location into the coordinate
	//system of the child
	if self.is_dragging.is_none(){
	    let location = *location + self.viewport_offset;
	    //Some events have an inner "location" of some sorts, those are changed here
	    let event = event.offset(self.viewport_offset);
	    
	    self.child.on_event(&event, &(location * 1.0));
	}
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls{

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	let bar_width = crate::get_config().scroll_handle_width as Unit;
	
	let child_extent = Vector2::new(
	    if self.slider.1.is_some() {host_extent.x - bar_width}else{host_extent.x},
	    if self.slider.0.is_some() {host_extent.y - bar_width}else{host_extent.y},
	);

	let child_level = if self.slider.0.is_some() || self.slider.1.is_some(){
	    level + 2
	}else{
	    level + 1 
	};
	
	//First of all draw the sliders, depending on the feedback, setup new slider
	let mut child_primitives = self.child.get_calls(child_extent, draw_time, interface, child_level);

	let offset = self.viewport_offset * Vector2{x: -1.0, y: -1.0};
	
	child_primitives.offset(offset);
	child_primitives.clip_to(self.get_drawing_area().to);
	
	//Recreate slider
	self.set_slider_pos();
	
	//First of all draw the background as well as, if there are any the sliders
	let mut primitives = Vec::new();
	let mut inner_level = level;
	
	
	primitives.push(Prim{
	    prim: PrimType::Rect,
	    drawing_area: self.area,
	    scissors: None,
	    style: Style::from_color(crate::get_styling().background),
	    level: inner_level
	});
	
	if self.slider.0.is_some() || self.slider.1.is_some(){
	    inner_level += 1;
	}

	let selection_index = if let Some((idx, _)) = self.is_dragging{
	    idx
	}else{
	    3
	};
	
	if let Some(hor) = self.slider.0{
	    primitives.push(
		Prim{
		    prim: PrimType::Rect,
		    drawing_area: hor,
		    scissors: None,
		    style: Style::from_color(
			if selection_index == 0 {
			    crate::get_styling().background_sec_selected
			}else{
			    crate::get_styling().background_sec
			}
		    ),
		    level: inner_level
		}
	    );
	}
	
	if let Some(vert) = self.slider.1{
	    primitives.push(
		Prim{
		    prim: PrimType::Rect,
		    drawing_area: vert,
		    scissors: None,
		    style: Style::from_color(
			if selection_index == 1 {
			    crate::get_styling().background_sec_selected
			}else{
			    crate::get_styling().background_sec
			}
		    ),
		    level: inner_level
		}
	    );
	}	
	

	child_primitives.add(primitives);
	child_primitives
    }
}
