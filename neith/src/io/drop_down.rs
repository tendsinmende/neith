

use std::{time::Instant, fmt::Display};
use log::info;

use crate::base::Text;
use crate::layout::Align;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;
use crate::{
    event::{WindowEvent, Event, MouseButton, KeyState},
    math::{Unit, Vector2, Area},
    Interface,
    base::list_array::ListArray,
    layout::Direction
};


///A Drop down menu that can be used as a menu of a set list of elements.
pub struct DropDown<E: Display>{
    elements: Vec<E>,
    selected: usize,
    on_change: Option<Box<dyn Fn(&[E], &E) + Send>>,
    area: Area,

    ///The element render when the list is not expanded
    current_element: Box<Align<Text>>,
    
    ///The widget used as overlay when the list is expanded.
    list: ListArray,

    is_expanded: Option<usize>,
    next_change_expand_state: bool,
    drop_down_height: Unit,
}

impl<E> DropDown<E> where E: Display{
    pub fn new() -> Self{
	DropDown{
	    elements: vec![],
	    selected: 0,
	    on_change: None,
	    area: Area::empty(),
	    current_element: Box::new(Align::new(Text::new().with_text("none"))),
	    list: ListArray::new(Direction::Vertical, vec![])
		.with_element_size(crate::get_config().text_size as Unit + 2.0),
	    is_expanded: None,
	    next_change_expand_state: false,
	    drop_down_height: crate::get_config().default_overlay_size as Unit,
	}
    }

    pub fn with_elements(mut self, elements: Vec<E>) -> Self{
	self.elements = elements;
	self
    }

    pub fn with_selection(mut self, selection: usize) -> Self{
	self.set_selection(selection);
	self
    }

    ///Adds a function that is called when the selection state changes. The function get provided the current list of elements,
    /// as well as the currently selected element.
    pub fn with_on_selection_changed<F>(mut self, function: F) -> Self where F: Fn(&[E], &E) + Send + 'static{
	self.on_change = Some(Box::new(function));
	self
    }

    pub fn get_elements(&self) -> &[E]{
	&self.elements
    }

    pub fn get_elements_mut(&mut self) -> &mut Vec<E>{
	&mut self.elements
    }

    pub fn get_selected(&self) -> Option<&E>{
	self.elements.get(self.selected)
    }

    pub fn get_selected_mut(&mut self) -> Option<&mut E>{
	self.elements.get_mut(self.selected)
    }

    pub fn get_selected_idx(&self) -> usize{
	self.selected
    }
    
    ///Sets the current selected element, does nothing if the element does not exist (for instance if `sel` exceeds the current element list).
    pub fn set_selection(&mut self, sel: usize){
	if sel > self.elements.len(){
	    info!("Dropdown: Selection > len");
	    
	    return;
	}

	self.selected = sel;
	self.current_element.child = Box::new(
	    Text::new()
		.with_text(&self.elements[self.selected].to_string())
		.with_single_line()
	);
    }

    fn update_wlist(&mut self){
	self.list.list = self.elements.iter().map(|e|{
	    let wid = Box::new(crate::base::Text::new()
					   .with_text(&e.to_string())
					   .with_single_line()
	    );

	    let cast: Box<dyn Widget + Send> = wid;
	    cast
	}).collect::<Vec<_>>();
    }
}

impl<E> Widget for DropDown<E> where E: Display{
    
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	
	let border = crate::get_config().border_slim as Unit;
	let list_local_click = *location - Vector2::new(border, border);	
	match event{
	    Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state: KeyState::Pressed}) => {
		if let Some(_overlay_id) = self.is_expanded{
		    //Is expanded, check if we have to select an item, or close the dropdown overlay
		    let location_info = self.list.get_location_item(list_local_click);
		    if let Some(idx) = location_info{			
			self.set_selection(idx);
			if let Some(f) = &self.on_change{
			    f(&self.elements, &self.elements[self.selected]);
			}
			//Anyways reset to none expanded
			self.next_change_expand_state = true;
		    }else{
			self.next_change_expand_state = true;
		    }
		}else{
		    //Is not expanded, check if we should expand.
		    if self.area.is_in(location){
			self.next_change_expand_state = true;
		    }
		}
	    }
	    _ => {}
	}

	self.list.on_event(
	    &event.offset(Vector2::new(border, border)),
	    &list_local_click
	)
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};
	
	//Check out the expand state. If it changed, register/deregister the overlay
	if self.next_change_expand_state{
	    if let Some(old_id) = self.is_expanded.take(){
		interface.remove_overlay(old_id);
	    }else{
		self.is_expanded = Some(interface.register_overlay());
	    }

	    self.next_change_expand_state = false;
	}
	
	let border = crate::get_config().border_slim as Unit;
	let shrink = Vector2::new(-border, -border);
	let box_area = self.area.shrink_from(shrink).grow_to(shrink);

	let trigger_size = (50.0 as Unit).min(box_area.extent().x * 0.25);
	
	let trigger_area = Area{
	    from: (box_area.to.x - trigger_size, box_area.from.y).into(),
	    to: box_area.to
	};
	
	let box_area = Area{
	    from: box_area.from,
	    to: (box_area.to.x - trigger_size - border, box_area.to.y).into()
	};
	
	let mut calls = DrawCalls::new(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background_sec),
		level: level
	    },
	    Prim{
		prim: PrimType::Rect,
		drawing_area: box_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level + 1
	    },
	    Prim{
		prim: PrimType::Rect,
		drawing_area: trigger_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level + 1
	    }
	]);

	
	
	if let Some(id) = self.is_expanded{
	    //If the list is currently expanded, draw it over the drop dowm
	    self.update_wlist();

	    let mut overlay_calls = self.list.get_calls(
		(box_area.to.x, self.drop_down_height).into(),
		draw_time,
		interface,
		0
	    );

	    overlay_calls.offset(shrink * -1.0);
	    
	    calls.add_overlay_calls(id, overlay_calls);
	}else{
	    let mut title_calls = self.current_element.get_calls(
		box_area.extent(),
		draw_time,
		interface,
		level + 2
	    );

	    title_calls.offset(shrink * -1.0);
	    calls.merge(title_calls)
	}
	calls
    }
}
