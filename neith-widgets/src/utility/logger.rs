use std::sync::Arc;
use std::sync::Mutex;
use std::sync::RwLock;
use std::time::Instant;

use log::Level;
use log::LevelFilter;
use log::Metadata;
use log::Record;
use neith::Interface;
use neith::base::ArcWidget;
use neith::base::Text;
use neith::com::MutVal;
use neith::com::Recv;
use neith::com::Sender;
use neith::config::Color;
use neith::event::Event;
use neith::io::DropDown;
use neith::io::Label;
use neith::layout::Align;
use neith::layout::Direction;
use neith::layout::List;
use neith::layout::Split;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith::widget::DrawCalls;
use neith::widget::Style;
use neith::widget::Widget;

struct InnerLogger{
    log_sender: Sender<(Level, String)>
}

impl log::Log for InnerLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
	true
    }

    fn log(&self, record: &Record) {
	self.log_sender.send((record.level(), format!("{} - {}", record.level(), record.args())));
    }

    fn flush(&self) {}
}

///Displays all log messages is able to dynamically change the granularity of the logs.
pub struct Logger{
    area: Area,
    layout: Split,
}

impl Logger{
    pub fn new() -> Self{

	let log_sender: Sender<(Level, String)> = Sender::new();
	let log_receiver = log_sender.new_receiver();
	let inner_logger = Box::new(InnerLogger{
	    log_sender
	});
	
	log::set_boxed_logger(inner_logger).expect("Failed to set logger");
	log::set_max_level(LevelFilter::Trace);

	let log_level = MutVal::new(LevelFilter::Trace);
	let llcp = log_level.clone();
	
	let list = List::new(Direction::Vertical)
	    .with_on_update(move |list| {
		//Receive all messages and push them onto the stack.
		let msgs = log_receiver.recv();

		for (level, msg) in msgs{

		    if level > *log_level.get(){
			println!("Ignoring!");
			continue
		    }
		    
		    let color = match level{
			Level::Error => Color::Float([1.0, 0.5, 0.5]),
			Level::Warn => Color::Float([1.0, 1.0, 0.5]),
			Level::Info => Color::Float([0.5, 0.5, 1.0]),
			Level::Debug => Color::Float([0.5, 1.0, 0.5]),
			Level::Trace => Color::Float([0.8, 0.8, 0.8]),
		    };

		    let text = Align::new(Label::new("").with_text(
			Text::new()
			    .with_single_line()
			    .with_text(&msg)
			    .with_style(Style::from_color(color))
			    .with_size(25.0)
		    )).with_horizontal_alignment(neith::layout::HorizontalAlign::Left);
		    list.list.push(Box::new(text));
		}
	    }).with_element_size(28.0);
	
	let layout = Split::new()
	    .with_one(
		list
	    ).with_two(
		DropDown::new()
		    .with_elements(
			vec![
			    LevelFilter::Off,
			    LevelFilter::Error,
			    LevelFilter::Warn,
			    LevelFilter::Info,
			    LevelFilter::Debug,
			    LevelFilter::Trace
			]
		    ).with_on_selection_changed(move |_list, new_level| *llcp.get_mut() = *new_level)
		    .with_selection(5)
	    ).with_split_type(neith::layout::split::SplitType::Absolute(35.0))
	    .with_inv_reference();

	
	Logger{
	    area: Area::empty(),
	    layout
	}
    }
}

impl Widget for Logger{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	self.layout.on_event(event, location)
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {	
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};
	self.layout.get_calls(host_extent, draw_time, interface, level)
    }
}
