//! # Utility
//! Contains utility nodes that have a special purpose.
//! This contains but is not limited to:
//!
//! - Log (log output based on the [log](https://crates.io/crates/log) crate)

mod logger;
pub use logger::Logger;
