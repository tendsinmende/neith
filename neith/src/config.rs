
use ron::de::from_reader;
use ron::ser::{to_string_pretty, PrettyConfig};
use serde::Deserialize;
use serde::Serialize;
use std::fs::{File, OpenOptions};
use std::io::{Error, Write};
use core::any::TypeId;
use crate::log::warn;

///Defines a color for a style.
#[derive(Serialize, Deserialize, Clone)]
pub enum Color{
    Hex(String),
    Uint([u8;3]),
    Float([f32;3]),
}

fn hex_to_u8(c: char) -> u8{
    if let Ok(num) = format!("{}", c).parse::<u8>(){
	num
    }else{
	match c{
	    'a' | 'A' => 10,
	    'b' | 'B' => 11,
	    'c' | 'C' => 12,
	    'd' | 'D' => 13,
	    'e' | 'E' => 14,
	    'f' | 'F' => 15,
	    _ => panic!("Could not parse char {} to u32, must be 0-1 or a,b,c,d,e,f"),
	}
    }
}

///Converts a hexadecimal number of two character eg. ff or 0e, to a u32 in range 0-255
fn double_hex_to_u8(c1: char, c2: char) -> u8{
    hex_to_u8(c1) * 16 + hex_to_u8(c2)
}

fn hex_to_col(hex: String) -> [f32; 4]{
    assert!(hex.len() == 6, "Hex color string({}) was not 6 elements long", hex);
    let mut iter = hex.chars();
    let one = double_hex_to_u8(iter.next().unwrap(), iter.next().unwrap());
    let two = double_hex_to_u8(iter.next().unwrap(), iter.next().unwrap());
    let three = double_hex_to_u8(iter.next().unwrap(), iter.next().unwrap());

    [one as f32 / 255.0, two as f32 / 255.0, three as f32 / 255.0, 1.0]
} 

impl Color{
    pub fn to_glsl(&self) -> [f32; 4]{
	match self{
	    Color::Hex(val) => hex_to_col(val.to_string()),
	    Color::Uint(val) => [
		(val[0] as f32).min(255.0).max(0.0) / 255.0,
		(val[1] as f32).min(255.0).max(0.0) / 255.0,
		(val[2] as f32).min(255.0).max(0.0) / 255.0,
		1.0
	    ],
	    Color::Float(val) => [
		val[0].min(1.0).max(0.0),
		val[1].min(1.0).max(0.0),
		val[2].min(1.0).max(0.0),
		1.0
	    ]
	}
    }

    pub fn darken(&mut self, ammount: f32) -> Self{
	let mut glsl = self.to_glsl();
	glsl[0] = (glsl[0] - ammount).max(0.0).min(1.0);
	glsl[1] = (glsl[1] - ammount).max(0.0).min(1.0);
	glsl[2] = (glsl[2] - ammount).max(0.0).min(1.0);
	
	Color::Float([glsl[0], glsl[1], glsl[2]])
    }

    pub fn lighten(&mut self, ammount: f32) -> Self{
	let mut glsl = self.to_glsl();
	glsl[0] = (glsl[0] + ammount).max(0.0).min(1.0);
	glsl[1] = (glsl[1] + ammount).max(0.0).min(1.0);
	glsl[2] = (glsl[2] + ammount).max(0.0).min(1.0);
	
	Color::Float([glsl[0], glsl[1], glsl[2]])
    }

    pub fn from_type_id(id: &TypeId) -> Self{
	//Cast type into bytes and use them to build a color.
	let slice = ty_as_u8_slice(id);

	
	Color::Uint([
	    slice[0 % slice.len()],
	    slice[1 % slice.len()],
	    slice[2 % slice.len()]
	])
    }
}

fn ty_as_u8_slice(p: &TypeId) -> &[u8] {
    unsafe{
	std::slice::from_raw_parts(
            (p as *const TypeId) as *const u8,
            ::std::mem::size_of::<TypeId>(),
	)
    }    
}

///Color definitions for the theme.
#[derive(Serialize, Deserialize, Clone)]
pub struct Styling{
    ///Main color used for all backgrounds
    pub background: Color,
    ///Used for background that signals selection, for instance when hovering over a button with the cursor.
    pub background_selected: Color,
    ///Used for secondary background areas, for instance the scroll-handles, or the split border areas
    pub background_sec: Color,
    pub background_sec_selected: Color,

    ///Used for normal highlights within a ui. For instance when separating two sides of a split widget, or when
    ///drawing a border around something.
    pub highlight: Color, 
    
    ///Main color used for characters in a text
    pub text: Color,
    ///Color used for selection in a text
    pub text_selected: Color,
    ///Cursor color in a text edit
    pub text_cursor: Color,
    
    ///Used for highlighting stuff where an error occurred.
    pub error: Color,
    ///Used for warnings.
    pub warning: Color
}

impl Default for Styling{
    fn default() -> Self{
	Styling{
	    background: Color::Float([0.155, 0.15, 0.15]),
	    background_selected: Color::Float([0.25, 0.2, 0.2]),
	    
	    background_sec: Color::Float([0.355, 0.35, 0.35]),
	    background_sec_selected: Color::Float([0.455, 0.45, 0.45]),

	    highlight: Color::Float([0.805, 0.8, 0.8]),

	    text: Color::Float([0.805, 0.8, 0.8]),
	    text_selected: Color::Float([0.2, 0.2, 0.7]),
	    text_cursor: Color::Float([1.0, 1.0, 1.0]),

	    error: Color::Float([0.9, 0.2, 0.2]),
	    warning: Color::Float([0.8, 0.8, 0.2]),
	}
    }
}

///Contains all configureable value. The config can be reloaded by notifying the interface via
/// `config_changed()`.
#[derive(Serialize, Deserialize, Clone)]
pub struct Config {

    ///The relative path to the file where changes should be written to.
    pub path: String,
    
    ///Default styling information for the widgets within the toolkit.
    pub styling: Styling,
    
    ///Width of borders between widgets (in pixel)
    pub border_width: u32,
    ///Slim border, like the one used for "Frame"
    pub border_slim: u32,
    ///Width of highlights within borders, if highlights are drawn
    pub border_highlight_width: u32,
    
    ///Width of the scroll handles in viewports (in pixel)
    pub scroll_handle_width: u32,

    pub scroll_speed: f32,

    ///The smallest area a viewport can be contained in.
    pub viewport_min_area: (f32, f32),

    pub default_button_size: (f32, f32),
    pub default_list_element_size: f32,
    pub default_overlay_size: f32,
    
    ///Default text size if no other is set
    pub text_size: f32,
    ///Width of the cursor in pixel, when the font size would be 1px. So usually something like 0.05
    pub text_cursor_size: f32,
	
}

impl Default for Config {
    fn default() -> Self {
        Config{

	    path: "neith.conf".to_owned(),
	    
	    styling: Styling::default(),

	    border_width: 8,
	    border_slim: 4,
	    border_highlight_width: 2,
	    
	    scroll_handle_width: 8,
	    scroll_speed: 10.0,
	    viewport_min_area: (10.0, 10.0),
	    default_button_size: (150.0, 75.0),
	    default_list_element_size: 30.0,
	    default_overlay_size: 200.0,
	    
	    text_size: 20.0,
	    text_cursor_size: 0.05
        }
    }
}

impl Config {
    ///Saves the Config to the path given in `self.path`
    pub fn save(&self) -> Result<(), Error> {
        let pretty = PrettyConfig {
            depth_limit: 10,
            separate_tuple_members: true,
            enumerate_arrays: false,
            ..PrettyConfig::default()
        };

        let final_string = to_string_pretty(self, pretty).expect("Failed to encode config!");

        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .open(self.path.clone())?;
        write!(file, "{}", final_string)?;
        Ok(())
    }

    ///Loads a config from some file
    pub fn load(path: &str) -> Self {
        let file = match File::open(path) {
            Ok(f) => f,
            Err(e) => {
                warn!(
                    "Could not find file at {} with er: {}, loading defaults!",
                    path, e
                );
                let mut default = Config::default();
                default.path = String::from(path);
                return default;
            }
        };
        let config: Config = match from_reader(file) {
            Ok(c) => c,
            Err(e) => {
                warn!(
                    "Failed to parse config at {} with err: {}, loading defaults!",
                    path, e
                );
                Config::default()
            }
        };
        config
    }
}
