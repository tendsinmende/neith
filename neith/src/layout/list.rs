use crate::Interface;
use crate::base::list_array::ListArray;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::widget::DrawCalls;
use crate::widget::Widget;

use std::time::Instant;

use super::Direction;


///A standard vertical list with embedded viewport
pub struct List{
    ///The inner viewport that contains the list-array
    viewport: Box<crate::layout::Viewport<ListArray>>,
    on_update: Option<Box<dyn Fn(&mut ListArray) + Send + 'static>>,
    area: Area,
}

impl List{
    pub fn new(dir: Direction) -> Self{

	let la = crate::base::list_array::ListArray::new(dir, vec![]);

	let viewport = Box::new(crate::layout::Viewport::new(la));
	
	List{
	    viewport,
	    on_update: None,
	    area: Area::empty(),
	}
    }

    pub fn with_element_size(mut self, size: Unit) -> Self{
	self.viewport.child.element_size = size;
	self
    }

    pub fn with_items(mut self, mut items: Vec<Box<dyn Widget + Send + 'static>>) -> Self{
	self.viewport.child.list.append(&mut items);
	self
    }

    pub fn push<I>(mut self, i: I) -> Self where I: Widget + Send + 'static{
	self.viewport.child.list.push(Box::new(i));
	self
    }
    
    pub fn add<I>(&mut self, i: I) where I: Widget + Send + Sync + 'static{
	self.viewport.child.list.push(Box::new(i));
    }

    ///Executes something on the internal list of widgets
    pub fn on_list<F>(&mut self, mut f: F) where F: FnMut(&mut Vec<Box<dyn Widget + Send + 'static>>){
	f(&mut self.viewport.child.list);
    }

    ///Gets executed everytime the list updates
    pub fn with_on_update<F>(mut self, fnc: F) -> Self where F: Fn(&mut ListArray) + Send + 'static{
	self.on_update = Some(Box::new(fnc));
	self
    }
}


impl Widget for List{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	if self.area.is_in(location){
	    self.viewport.on_event(event, location);
	}
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	if let Some(on_upd) = &self.on_update{
	    (on_upd)(&mut self.viewport.child);
	}
	
	self.viewport.get_calls(
	    host_extent,
	    draw_time,
	    interface,
	    level
	)
    }
}
