
use crate::Interface;
use crate::event::Event;
use crate::layout::Direction;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use std::time::Instant;

///A Simple widget that takes a list of widgets and arranges them vertically. You'll probably won't
/// use that except if you are implementing some custom widget.
pub struct ListArray{
    pub direction: Direction,
    pub list: Vec<Box<dyn Widget + Send>>,
    pub area: Area,
    ///The max width for horizontal/ height for vertical list elements.
    pub element_size: Unit,
    pub last_selected: Option<usize>,
}

impl ListArray{

    pub fn new(direction: Direction, list: Vec<Box<dyn Widget + Send>>) -> Self{
	ListArray{
	    direction,
	    list,
	    area: Area::empty(),
	    element_size: crate::get_config().default_list_element_size as Unit,
	    last_selected: None,
	}
    }

    pub fn on_list<F>(&mut self, mut f: F) where F: FnMut(&mut Vec<Box<dyn Widget + Send>>){
	f(&mut self.list)
    }

    pub fn with_element_size(mut self, new_size: Unit) -> Self{
	self.element_size = new_size; 
	self
    }
    
    ///If there is an item at this location, its index in the list is returned now
    pub fn get_location_item(&self, location: Vector2<Unit>) -> Option<usize>{

	if !self.area.is_in(&location) {
	    return None;
	}
	
	let border = crate::get_config().border_slim as Unit;		
	let idx = match self.direction{
	    Direction::Vertical => location.y / (self.element_size + border),
	    Direction::Horizontal => location.x / (self.element_size + border)
	};

	let idx = idx.floor() as usize;
	if idx < self.list.len(){
	    Some(idx)
	}else{
	    None
	}
    }

    ///Height for vertical list, width for horizontal list. Includes all eye candy
    pub fn extent(&self) -> Unit{
	self.list.len() as Unit * self.element_size + ((self.list.len()+1) as Unit * crate::get_config().border_slim as Unit)
    }
}

impl Widget for ListArray{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	let border_width = crate::get_config().border_slim as Unit;
	let mut event_offset: Vector2<Unit> = match self.direction{
	    Direction::Horizontal => (border_width, 0.0).into(),
	    Direction::Vertical => (0.0, border_width).into(),
	};

	let mut selection_state = None;
	
	for (idx, l) in self.list.iter_mut().enumerate(){
	    let inner_event = event.offset(event_offset * -1.0);
	    let inner_location = *location - event_offset;
	    l.on_event(&inner_event, &inner_location);
	    
	    //Offset for next widget
	    match self.direction{
		Direction::Vertical => event_offset.y += self.element_size + border_width,
		Direction::Horizontal => event_offset.x += self.element_size + border_width,
	    }
	}

	self.last_selected = selection_state;
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {
	//Collect all draw calls, but move each item beneath the one before.

	let mut child_calls = DrawCalls::new(vec![]);
	let border_width = crate::get_config().border_slim as Unit;
	let mut offset = Vector2::new(border_width, border_width);

	let ini_offset = offset;
	
	for (idx, l) in self.list.iter_mut().enumerate(){
	    let element_extent = match self.direction{
		Direction::Vertical => Vector2::new(host_extent.x - 2.0 * border_width, self.element_size),
		Direction::Horizontal => Vector2::new(self.element_size, host_extent.y - 2.0 * border_width),
	    };

	    
	    let mut sub_calls = l.get_calls(
		element_extent,
		draw_time,
		interface,
		level + 2
	    );
	    sub_calls.clip_to(element_extent);
	    //First move, then update for next call
	    sub_calls.offset(offset);

	    let background_area = Area{
		from: offset,
		to: offset + element_extent
	    };
	    
	    //Add background for this widget
	    child_calls.add(vec![Prim{
		prim: PrimType::Rect,
		drawing_area: background_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level + 1
	    }]);
	    
	    match self.direction{
		Direction::Vertical => offset.y += element_extent.y + border_width,
		Direction::Horizontal => offset.x += element_extent.x + border_width,
	    }
	    
	    //Now merge new calls into old ones
	    child_calls.merge(sub_calls);
	}

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: child_calls.extent + ini_offset
	};

	let ligth_area = Area{
	    from: (0.0, 0.0).into(),
	    to: (self.area.extent().x.max(host_extent.x), self.area.extent().y.max(host_extent.y)).into()
	};
	
	//Add a draw call for the background in the border color
	let mut calls = DrawCalls::new(vec![
	    Prim{
		prim: PrimType::Rect,
		drawing_area: ligth_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background_sec),
		level: level
	    }
	]);
	
	calls.merge(child_calls);
	calls
    }
}
