use std::ops::{
    Add,
    Sub,
    Mul,
    Div
};

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct Vector2<T>{
    pub x: T,
    pub y: T
}

impl<T> Vector2<T>{
    pub fn new(x: T, y: T) -> Self{
	Vector2{x,y}
    }
}

impl<T: std::fmt::Display> std::fmt::Display for Vector2<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl<T> Add for Vector2<T> where T: Add{
    type Output = Vector2<<T as Add>::Output>;
    
    fn add(self, other: Self) -> Self::Output{
	Vector2{
	    x: self.x + other.x,
	    y: self.y + other.y
	}
    }
}

impl<T> Sub for Vector2<T> where T: Sub{
    type Output = Vector2<<T as Sub>::Output>;
    fn sub(self, other: Self) -> Self::Output{
	Vector2{
	    x: self.x - other.x,
	    y: self.y - other.y
	}
    }
}

impl<T> Mul for Vector2<T> where T: Mul{
    type Output = Vector2<<T as Mul>::Output>;
    fn mul(self, other: Self) -> Self::Output{
	Vector2{
	    x: self.x * other.x,
	    y: self.y * other.y
	}
    }
}

impl<T> Mul<T> for Vector2<T> where T: Mul + Copy{
    type Output = Vector2<<T as Mul>::Output>;
    fn mul(self, other: T) -> Self::Output{
	Vector2{
	    x: self.x * other,
	    y: self.y * other
	}
    }
}

impl<T> Div for Vector2<T> where T: Div{
    type Output = Vector2<<T as Div>::Output>;
    fn div(self, other: Self) -> Self::Output{
	Vector2{
	    x: self.x / other.x,
	    y: self.y / other.y
	}
    }
}

impl<T> From<[T;2]> for Vector2<T> where T: Clone{
    fn from(inp: [T;2]) -> Vector2<T>{
	Vector2{
	    x: inp[0].clone(),
	    y: inp[1].clone()
	}
    }
}

impl<T> From<(T,T)> for Vector2<T>{
    fn from(inp: (T,T)) -> Vector2<T>{
	Vector2{
	    x: inp.0,
	    y: inp.1
	}
    }
}

///One unit is the width/height if one pixel. However, since sometimes sub pixel areas are needed, the underlying type is a float.
pub type Unit = f32;

#[derive(Copy, Clone, PartialEq)]
pub struct Area{
    pub from: Vector2<Unit>,
    pub to: Vector2<Unit>
}

impl std::fmt::Display for Area {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Area: [{},{}] - [{},{}]", self.from.x.round(), self.from.y.round(), self.to.x.round(), self.to.y.round())
    }
}


impl Area{

    pub fn center(&self) -> Vector2<Unit>{
	self.from + self.extent() / (2.0, 2.0).into()
    }
    
    pub fn is_in(&self, point: &Vector2<Unit>) -> bool{
	(point.x > self.from.x) && (point.x <= self.to.x) && (point.y > self.from.y) && (point.y <= self.to.y)
    }
    
    pub fn extent(&self) -> Vector2<Unit>{
        self.to - self.from        
    }

    ///Adds `addition` to the `to` parameter.
    pub fn grow_to(mut self, addition: Vector2<Unit>) -> Self{
	self.to = self.to + addition;
	self
    }

    ///Reduces the `from` parameter.
    pub fn shrink_from(mut self, reduction: Vector2<Unit>) -> Self{
	self.from = self.from - reduction;
	self
    }

    pub fn offset(&mut self, offset: Vector2<Unit>){
	self.from = self.from + offset;
	self.to = self.to + offset
    }
    
    ///Creates a new empty area starting at [0,0] with an extent of [0,0]
    pub fn empty() -> Self{
	Area{
	    from: (0.0, 0.0).into(),
	    to: (0.0, 0.0).into()
	}
    }

    ///Creates the unit area from [0,0] to [1.0, 1.0]
    pub fn unit() -> Self{
	Area{
	    from: (0.0, 0.0).into(),
	    to: (1.0, 1.0).into()
	}
    }

    //If at least one dimension has an extent of <= 0
    pub fn has_zero_dim(&self) -> bool{
	(self.from.x >= self.to.x) || (self.from.y >= self.to.y)
    }

    ///Converts the Area to a 2d transform matrix, which transforms a screen covering rectangle to a `area` covering rectangle
    pub fn to_transform(&self, screen: &Area) -> [[f32; 4]; 4] {

	let extent = self.extent();
	let screen_extent = screen.extent();
	//First find the scaling factor for x and y, then add the translation from the pos attribute
        let sx = extent.x / screen_extent.x * 2.0;
        let sy = extent.y / screen_extent.y * 2.0;
        //The translation in vulkan is selfs translation / screen_width * 2 since
        //vulkans coords are from -1 till 1
        let tx = (self.from.x / screen_extent.x * 2.0) - 1.0;
        let ty = (self.from.y / screen_extent.y * 2.0) - 1.0;

        //Build the transform matrix, glsl seams to be column/row
        [
            [sx as f32, 0.0      , 0.0, 0.0],
            [0.0      , sy as f32, 0.0, 0.0],
            [0.0      , 0.0      , 1.0, 0.0],
            [tx as f32, ty as f32, 0.0, 1.0],
        ]
    }

    ///Swaps the from and to coordinates in a way that from is always smaller then to on all coords.
    pub fn normalize(&mut self){
	if self.from.x > self.to.x{
	    std::mem::swap(&mut self.from.x, &mut self.to.x);
	}

	if self.from.y > self.to.y{
	    std::mem::swap(&mut self.from.y, &mut self.to.y);
	}
    }
    
    pub fn union(&self, other: Area) -> Area{

	let new_from: Vector2<_> = (
	    self.from.x.max(other.from.x),
	    self.from.y.max(other.from.y),
	).into();
	
	let mut new_to: Vector2<_> = (
	    self.to.x.min(other.to.x),
	    self.to.y.min(other.to.y),
	).into();

	
	//If one of the axis overlab, we got a zero dimension 
	if new_from.x > new_to.x || new_from.y > new_to.y{
	    new_to.x = new_from.x;
	}
	
	
	Area{
	    from: new_from,
	    to: new_to
	}
    }
}

///Identity 4x4 matrix
pub const IDENTITY: [[f32;4]; 4] = [
    [1.0, 0.0, 0.0, 0.0],
    [0.0, 1.0, 0.0, 0.0],
    [0.0, 0.0, 1.0, 0.0],
    [0.0, 0.0, 0.0, 1.0]
];
