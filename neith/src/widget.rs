use std::any::Any;
use std::time::Instant;
use std::{collections::HashMap, sync::{RwLock, Arc}};

use crate::Interface;
use crate::config::Color;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;

///Style description of most primitives.
#[derive(Clone, Copy)]
pub struct Style{
    ///color at which this is drawn in rgba [0.0-1.0]. So in traditional 8bit colors maps 0 -> 0, 1.0 -> 255
    pub color: [f32;4],
    /// glowing power of the style. When > 0.0 some bloom might be applied.
    pub power: f32,
}

impl Style{
    pub fn from_color(col: Color) -> Self{
	Style{
	    color: col.to_glsl(),
	    power: 0.0
	}
    }

    pub fn with_power(mut self, power: f32) -> Self{
	self.power = power;
	self
    }
}

///A set of drawcalls with the most top-left element always starting at (0,0).
#[derive(Clone)]
pub struct DrawCalls{
    pub primitives: Vec<Prim>,
    pub extent: Vector2<Unit>,
    ///Contains the primitives that are contained within a overlay. They are keyed by the
    /// key used in the `Interface`. The get transformed in the same way like the normal draw calls, but drawn on top of all
    /// "normal" widgets.
    pub overlay_prims: HashMap<usize, Vec<Prim>>,
}

impl DrawCalls{
    pub fn new(primitives: Vec<Prim>) -> Self{

	let mut extent: Vector2<Unit> = Vector2{x: 0.0, y: 0.0};
	for p in primitives.iter(){
	    extent.x = extent.x.max(p.area().to.x);
	    extent.y = extent.y.max(p.area().to.y);
	}
	
	DrawCalls{
	    primitives,
	    extent,
	    overlay_prims: HashMap::new()
	}
    }    
    pub fn merge(&mut self, mut other: DrawCalls){
	self.extent = Vector2{
	    x: self.extent.x.max(other.extent.x),
	    y: self.extent.y.max(other.extent.y),
	};
	//Merge normal primitives
	self.primitives.append(&mut other.primitives);
	//merge overlay primitves
	for (key, mut prims) in other.overlay_prims.into_iter(){
	    if let Some(other_prims) = self.overlay_prims.get_mut(&key){
		other_prims.append(&mut prims);
	    }else{
		self.overlay_prims.insert(key, prims);
	    }
	}
    }

    pub fn add(&mut self, mut prims: Vec<Prim>){

	self.primitives.append(&mut prims);
	
	for p in prims{
	    self.extent.x = self.extent.x.max(p.area().to.x);
	    self.extent.y = self.extent.y.max(p.area().to.y);
	}
    }

    pub fn add_overlay_calls(&mut self, id: usize, prims: DrawCalls){

	let mut prims = prims.primitives;
	
	if let Some(other_prims) = self.overlay_prims.get_mut(&id){
	    other_prims.append(&mut prims);
	}else{
	    self.overlay_prims.insert(id, prims);
	}

	//Make sure that the overlay calls start at level 0. Otherwise the overhead for the levels becomes too big for many overlays
	if let Some(calls) = self.overlay_prims.get_mut(&id){
	    let mut min_level = core::usize::MAX;

	    for call in calls.iter(){
		min_level = min_level.min(call.level);
	    }

	    //Check if we have to change the offset
	    if min_level > 0{
		let offset = min_level;
		for call in calls.iter_mut(){
		    call.level -= offset;
		}
	    }
	}
    }
    
    pub fn offset(&mut self, offset: Vector2<Unit>){
	let mut max: Vector2<Unit> = (0.0, 0.0).into();
	//Offset primitive and correct max value
	for p in self.primitives.iter_mut(){
	    p.offset(offset);
	    max.x = p.area().to.x.max(max.x);
	    max.y = p.area().to.y.max(max.y);
	}

	//Also offset overlay prims
	for (_k, prims) in self.overlay_prims.iter_mut(){
	    for p in prims.iter_mut(){
		p.offset(offset);
		max.x = p.area().to.x.max(max.x);
		max.y = p.area().to.y.max(max.y);	
	    }
	}

	self.extent = max;
    }

    pub fn area(&self) -> Area{
	Area{
	    from: (0.0, 0.0).into(),
	    to: self.extent
	}
    }

    ///Clips away every primitive that lies completely out of an area starting at 0.0,0.0 extending until `extent`
    ///
    /// Set own extent to `extent` and clips have in primitives
    pub fn clip_to(&mut self, extent: Vector2<Unit>){
	let host_area = Area{
	    from: (0.0, 0.0).into(),
	    to: extent
	};

	let mut swap = Vec::new();
	std::mem::swap(&mut swap, &mut self.primitives);
	
	self.primitives = swap.into_iter().filter_map(|mut p| if p.area().union(host_area).has_zero_dim(){
	    None
	}else{
	    let sc = if let Some(old_scissors) = p.scissors{
		old_scissors.union(host_area)
	    }else{
		host_area
	    };

	    if !sc.has_zero_dim() && sc.to.x <= host_area.to.x && sc.to.y <= host_area.to.y{
		p.scissors = Some(sc);
		Some(p)	
	    }else{
		None
	    }
	}).collect();
	//Overwrite with new max
	self.extent = extent;
    }

    ///Takes this draw call and scales every primitive by "scale".
    ///Scales from the (0.0, 0.0) point global to the primitive.
    pub fn scale(mut self, scale: Unit) -> Self{
	self.extent = self.extent * scale;
	//Update primitives
	for p in self.primitives.iter_mut(){
	    match &mut p.prim{
		PrimType::Polygon(_pgone) => {}, //nothing to scale, is scaled anyways to the area its drawn in.
		PrimType::Lines(li) => {
		    for pt in li.points.iter_mut(){
			*pt = *pt * scale;
		    }
		    li.width = li.width * scale;
		},
		PrimType::Rect => {}, //Nothing to modify
		PrimType::Character(ch) => ch.size = ch.size * scale,
	    }
	    
	    //Update calls meta data
	    p.drawing_area.from = p.drawing_area.from * scale;
	    p.drawing_area.to = p.drawing_area.to * scale;

	    if let Some(sc) = &mut p.scissors{
		sc.from = sc.from * scale;
		sc.to = sc.to * scale;
	    }
	}

	//Scale overlay prims as well
	for (_k, prims) in self.overlay_prims.iter_mut(){
	    for p in prims.iter_mut(){
		match &mut p.prim{
		    PrimType::Polygon(_pgone) => {}, //nothing to scale, is scaled anyways to the area its drawn in.
		    PrimType::Lines(li) => {
			for pt in li.points.iter_mut(){
			    *pt = *pt * scale;
			}
			li.width = li.width * scale;
		    },
		    PrimType::Rect => {}, //Nothing to modify
		    PrimType::Character(ch) => ch.size = ch.size * scale,
		}
		
		//Update calls meta data
		p.drawing_area.from = p.drawing_area.from * scale;
		p.drawing_area.to = p.drawing_area.to * scale;

		if let Some(sc) = &mut p.scissors{
		    sc.from = sc.from * scale;
		    sc.to = sc.to * scale;
		}
	    }
	}
	
	self
    }

    //Returns the highest level in this draw calls
    pub fn max_level(&self) -> usize{
	let mut max = 0;
	for c in &self.primitives{
	    max = max.max(c.level);
	}
	max
    }
}

pub trait Widget{
    ///Broadcasts event into tree. An Event might have a `location`. In that case the event should only be given
    /// To children that are within the location.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>);
    
    ///Returns the last known current occupied area
    fn area(&self) -> Area;
    
    ///Collects the primitives that have to be draw, provides the time at which they will be drawn.
    /// `draw_time` can be used to implement time dependent effects.
    ///
    /// `level` is the depth within the widget tree at which `self` resides.
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls;
}

/*
impl<W> Widget for Box<W> where W: Widget + Send + 'static{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>) {
	self.on_event(event, location)
    }
    fn area(&self) -> Area {
	(*self).area()
    }

    fn get_calls(&mut self, host_extent: Vector2<Unit>, draw_time: Instant, interface: &Interface, level: usize) -> DrawCalls {
	(*self).get_calls(host_extent, draw_time, interface, level)
    }
}

*/
///Utility trait which makes it possible to try and downcast any widget to a concrete type.
pub trait WidgetCast{
    ///Tries to convert `self` to `T`. Returns None if the widget is not of type `T`
    fn to_widget<T>(self) -> Option<Arc<RwLock<T>>> where T: Send + Sync + 'static;
}

impl<W> WidgetCast for Arc<RwLock<W>>
where W: Widget + Send + Sync + 'static{
    fn to_widget<T>(self) -> Option<Arc<RwLock<T>>>
    where T: Send + Sync + 'static{
	let any_self: Arc<dyn Any + Send + Sync + 'static> = self;
	if let Ok(cast) = any_self.downcast::<RwLock<T>>(){
	    Some(cast)
	}else{
	    None
	}
    }
}
