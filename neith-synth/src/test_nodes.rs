use neith::{Interface, io::{Edit, Label, Slider}};
use neith::com::MutVal;
use neith::config::Color;
use neith::event::Event;
use neith::event::Key;
use neith::event::KeyCode;
use neith::event::KeyState;
use neith::event::WindowEvent;
use neith::io::TextEditable;
use neith::layout::HorizontalAlign;
use neith::layout::VerticalAlign;
use neith::math::Area;
use neith::math::Unit;
use neith::math::Vector2;
use neith_widgets::node::ConnectionError;
use neith::widget::DrawCalls;
use neith::widget::Widget;
use neith_widgets::graph::line_graph::LineGraph;
use std::sync::{Arc, RwLock};
use std::any::TypeId;
use std::time::Instant;

use log::{error, info};

pub struct TestNode{
    #[allow(dead_code)]
    shared_value: MutVal<String>,
}

type AbstNode = Arc<RwLock<TestNode>>;

#[derive(Clone)]
struct NodeWrapper{
}

impl NodeWrapper{
    fn new() -> Self{
	NodeWrapper{
	}
    }
}

impl neith_widgets::node::NodeContent for NodeWrapper{
    type Node = NodeWrapper;
    fn inputs(&self) -> Vec<(TypeId, String)> {
	vec![
	    (TypeId::of::<i32>(), "Blub".to_string()),
	    (TypeId::of::<f32>(), "Blubber".to_string()),
	    (TypeId::of::<String>(), "Bubble".to_string()),
	    (TypeId::of::<String>(), "Bust".to_string())
	]   
    }
    fn outputs(&self) -> Vec<(TypeId, String)> {
	vec![
	    (TypeId::of::<f32>(), "Foo".to_string()),
	    (TypeId::of::<String>(), "Baz".to_string()),
	    (TypeId::of::<TypeId>(), "Bar".to_string()),
	    (TypeId::of::<()>(), "Spam".to_string()),
	    (TypeId::of::<String>(), "Spam".to_string())

	]	
    }
    fn try_in_connect(&mut self, input_index: usize, _source: &Self::Node, source_out_index: usize) -> Result<(), ConnectionError> {
	error!("Got IN connection at {} for out: {}", input_index, source_out_index);
	Ok(())
    }
    fn try_out_connect(&mut self, out_index: usize, _target: &Self::Node, target_in_index: usize) -> Result<(), ConnectionError> {
	info!("Got OUT connection at {} for in: {}", out_index, target_in_index);
	Ok(())
    }

    fn in_disconnected(&mut self, in_index: usize) -> Result<(), ConnectionError>{
	info!("Got IN disconnect at: {}", in_index);
	Ok(())
    }
    fn out_disconnected(&mut self, out_index: usize) -> Result<(), ConnectionError>{
	info!("Got OUT disconnect at: {}", out_index);
	Ok(())
    }

    ///Can be implemented if the node allows a shared (or non shared but displayable) default value on a in port.
    fn in_default_value(&mut self, idx: usize) -> Option<Box<dyn Widget + Send + 'static>>{
	match idx{
	    0 => Some(Box::new(Slider::new(0, 0, 10, 1).with_label(Label::new("Teddy").with_size(20.0), Some(" post")))),
	    1 => Some(Box::new(Slider::new(0.0 , 0.0 , 10.0 , 1.0 as f32))),
	    2 => Some(Box::new(Edit::new("teddy".to_string()))),
	    3 => Some(Box::new(Edit::new("Fancy default value".to_string()))),
	    _ => None
	}
    }

    ///Can be implemented if the node allows a shared (or non shared but displayable) default value on a in port.
    fn out_default_value(&mut self, idx: usize) -> Option<Box<dyn Widget + Send + 'static>>{
	match idx{
	    0 => Some(Box::new(Slider::new(0.0 , 0.0 , 10.0 , 1.0 as f32))),
	    1 => Some(Box::new(Edit::new(String::from("Default output string")))),
	    2 => None,
	    3 => None,
	    4 => Some(Box::new(Edit::new(String::from("Default output string")))),
	    _ => None
	}
    }

    
    fn get_title(&self) -> Option<String> {
	Some(String::from("Mr. Green Genes"))
    }
    fn get_interface(&self) -> Option<Box<dyn Widget + Send + 'static>> {
	//Build interface
	Some(
	    Box::new(neith::layout::Split::new()
				 .with_one(

				     neith::layout::Split::new()
					 .with_one(
					     LineGraph::new()
						 .with_data(
						     (0..200)
							 .map(|p| (p as f32, (p as f32 * 0.1).sin()))
							 .collect::<Vec<(f32,f32)>>()
						 )
						 .with_interpolation()
					 ).with_two(
					     neith::io::Slider::new(1.0, 0.25, 10.0, 0.5)
						 .with_label(neith::io::Label::new("Unset"), Some(" stuff"))
					 )	     
				 ).with_two(
				     neith::layout::Align::new(
					 neith::io::Button::new("Button Label")
					     .with_action(||println!("Node button pressed!"))
				     ).with_child_size((200.0, 75.0).into())
					 .with_vertical_alignment(VerticalAlign::Center)
					 .with_horizontal_alignment(HorizontalAlign::Center)
				 )
	    ))
    }

    fn frame_color(&self) -> Option<Color> {
	Some(Color::Float([0.2, 0.2, 1.0]))
    }
}


pub struct WrapCanvas{
    all_nodes: Vec<NodeWrapper>,
    area: Area,
    inner_canvas: neith_widgets::node::NodeCanvas<NodeWrapper>
}

impl WrapCanvas{
    pub fn new() -> Self{
	WrapCanvas{
	    area: Area::empty(),
	    all_nodes: vec![],
	    inner_canvas: neith_widgets::node::NodeCanvas::new(),
	}
    }
}

impl Widget for WrapCanvas{
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	if !self.area.is_in(location){
	    return;
	}
	
	match event{
	    Event::WindowEvent(WindowEvent::Key(Key{code: KeyCode::A, state: KeyState::Pressed})) =>{
		let shared_node = NodeWrapper::new();

		self.all_nodes.push(shared_node.clone());

		//Now also add to graph
		self.inner_canvas.add_node_at(shared_node, *location, (400.0, 200.0).into());
	    },
	    _ => {}
	}

	self.inner_canvas.on_event(event, location)
    }
    
    fn area(&self) -> Area{
	self.area
    }

    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls{
	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	self.inner_canvas.get_calls(
	    host_extent,
	    draw_time,
	    interface,
	    level
	)
    }
}
