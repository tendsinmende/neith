
use std::collections::{HashSet, HashMap, hash_set::Iter};

use super::NodeId;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Edge{
    //Node at which the line starts
    pub start_node: NodeId,
    ///The output idx of the start node
    pub start_node_port: usize,

    ///Node at which the line ends
    pub end_node: NodeId,
    ///Input port of end node
    pub end_node_port: usize,
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
struct Port{
    node: NodeId,
    port: usize
}

pub struct EdgeContainer{
    edges: HashSet<Edge>,
    from_in: HashMap<Port, Port>,
    from_out: HashMap<Port, Port>,
}


impl EdgeContainer{
    pub fn new() -> Self{
	EdgeContainer{
	    edges: HashSet::new(),
	    from_in: HashMap::new(),
	    from_out: HashMap::new()
	}
    }

    pub fn in_port_in_use(&self, node: &NodeId, port: usize) -> bool{
	let in_port = Port{node: *node, port};
	self.from_in.contains_key(&in_port)
    }
    
    pub fn out_port_in_use(&self, node: &NodeId, port: usize) -> bool{
	let out_port = Port{node: *node, port};
	self.from_out.contains_key(&out_port)
    }
    
    pub fn get_edges(&self) -> Iter<Edge>{
	self.edges.iter()
    }
    
    pub fn add_edge(&mut self, edge: Edge){
	let out_port = Port{node: edge.start_node, port: edge.start_node_port};
	let in_port = Port{node: edge.end_node, port: edge.end_node_port};

	self.from_in.insert(in_port, out_port);
	self.from_out.insert(out_port, in_port);

	self.edges.insert(edge);
    }

    ///If there was a connection, this remove the edge and returns the other side of this edge that was targeted by this in_idx
    pub fn remove_connection_from_in(&mut self, node: NodeId, in_idx: usize) -> Option<(NodeId, usize)>{
	let out_port = *self.from_in.get(&Port{node, port: in_idx})?;
	//Found some out port, therefore remove this edge entirely
	let former_in = self.from_out.remove(&out_port)?;
	let former_out = self.from_in.remove(&former_in)?;
	self.edges.remove(&Edge{
	    start_node: former_out.node,
	    start_node_port: former_out.port,
	    end_node: former_in.node,
	    end_node_port: former_in.port,
	});

	Some((out_port.node, out_port.port))
    }

    ///If there was a connection, this remove the edge and returns the other side of this edge that was targeted by this out_idx
    pub fn remove_connection_from_out(&mut self, node: NodeId, out_idx: usize) -> Option<(NodeId, usize)>{
	let in_port = *self.from_out.get(&Port{node, port: out_idx})?;

	//Found some out port, therefore remove this edge entirely
	let former_out = self.from_in.remove(&in_port)?;
	let former_in = self.from_out.remove(&former_out)?;
	self.edges.remove(&Edge{
	    start_node: former_out.node,
	    start_node_port: former_out.port,
	    end_node: former_in.node,
	    end_node_port: former_in.port,
	});
	

	Some((in_port.node, in_port.port))
    }
}
