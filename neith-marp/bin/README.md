# Binary content
In here is all the binary content. Currently this includes the shaders source and binary code as well as the used standart font.

## Shader
- Surface: Shader used to draw most surfaces. This can be lines, characters and normal rectangles or polygones.

## Fonts
- LeagueMono-Regular from [here](https://www.theleagueofmoveabletype.com/league-mono)
