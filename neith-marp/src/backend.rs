use std::path::PathBuf;
use std::sync::Arc;
use crate::*;
use crate::vulkan_prelude::*;


use crate::msw::winit::dpi::LogicalSize;

use neith::Backend;
use neith;
use neith::event::CursorIcon;
use neith::math::Area;
use neith::math::Vector2;
use neith::primitive::Character;
use neith::primitive::FontName;
use neith::primitive::PolyHandle;
use neith::primitive::Prim;

#[derive(Clone, Copy, Debug)]
pub enum BackendError{
    FailedToLoadFont,
}

///Holds all frame related information.
pub(crate) struct FrameInfo{
    pub present_finished: Arc<Semaphore>,
    pub render_complete: Arc<Semaphore>,
    pub render_fence: Option<Arc<Fence<Vec<SubmitInfo>>>>,
}

impl FrameInfo{
    pub fn new(device: Arc<Device>) -> Self{
	FrameInfo{
	    present_finished: Semaphore::new(device.clone()).expect("Failed to create present_finished semaphore"),
	    render_complete: Semaphore::new(device.clone()).expect("Failed to create render_complete semaphore"),
	    render_fence: None,
	}
    }
}

///A Rendering backend which draws via vulkan to a window.
/// The backend supports all if neith standard widgets as well as some other widgets that can be found in
/// `widgets` within this crate.
pub struct BackendProvider{
    pub window: Window,
    pub(crate) device: Arc<Device>,
    pub(crate) graphics_queue: Arc<Queue>,

    pub(crate) swapchain: Arc<Swapchain>,
    
    pub(crate) command_builder: command_builder::CommandBuilder,

    ///Tracks the last window extent
    pub(crate) window_extent: (u32,u32),

    ///Tracks the frame index that was in use last
    pub(crate) last_frame_index: usize,

    pub(crate) frame_count: usize,

    pub(crate) frame_infos: Vec<FrameInfo>,

    pub(crate) framebuffer_dirty: bool,
}

impl BackendProvider{
    pub fn show_window(&self){
	self.window.set_inner_size(LogicalSize::<u32>::from((680 as u32, 420 as u32)));
	self.window.set_visible(true);
    }

    pub(crate) fn resize(&mut self){

	for d in self.frame_infos.iter(){
	    if let Some(ref fen) = d.render_fence{
		fen.wait(std::u64::MAX).expect("Could not wait for fence");
	    }
	}

	
	self.device.wait_idle().expect("Could not wait for device!");


	//Drop old semaphores and stuff
	self.frame_infos = (0..self.swapchain.image_count()).into_iter()
	    .map(|_i| FrameInfo::new(self.device.clone()))
	    .collect();
	
	let swapchain_extent = vk::Extent2D::builder()
            .width(self.window_extent.0)
            .height(self.window_extent.1)
            .build();

        self.swapchain.recreate(swapchain_extent);
        let submit_fence = self.swapchain.images_to_present_layout(self.graphics_queue.clone());
        submit_fence.wait(std::u64::MAX).expect("Failed to wait for layout transition");
    }

    pub fn load_font(&mut self, font: FontName, path: &PathBuf) -> Result<(), BackendError>{
	self.command_builder.interface_pass.character.load_font(font, path)
    }
}

impl Backend for BackendProvider{
    fn draw_primitives(&mut self, prims: Vec<Prim>) {	
	let new_extent = self.window.inner_size();
	//Check if the framebuffer changed since the last time we rendered something, if thats the case, mark as dirty.
	if (self.window_extent.0 != new_extent.width) || (self.window_extent.1 != new_extent.height){
	    self.window_extent = (new_extent.width, new_extent.height);
	    self.framebuffer_dirty = true;

	    return;
	}

	//If it did not change since the last time, but the framebuffer is dirty, recreate all screen dependent systems
	if self.framebuffer_dirty{
	    //Resize inner swapchain
	    self.resize();
	    //Recreate command builder and all attached resources
	    self.command_builder.resize(self.frame_count as u32, self.window_extent);

	    self.framebuffer_dirty = false;
	}
	
	//Update frame index which is used
	let frame_index = (self.last_frame_index + 1) % self.frame_count;
	self.last_frame_index = frame_index;	

	//Wait for last frame on this index
	if let Some(old_fence) = self.frame_infos[frame_index].render_fence.take(){
	    old_fence.wait(std::u64::MAX).expect("Failed to wait for old drawing fence!");
	}
	
	//Check which image we are going to use
        let submit_index = match self.swapchain.acquire_next_image(
            std::u64::MAX,
            self.frame_infos[frame_index].present_finished.clone(), //Signals when the image at index is actually available.
        ){
            Ok(idx) => idx,
            Err(_) => {
                println!("neith-window: WARNING: could not get image index... dropping frame");
                return;
            }
        };

	let final_command_buffer = self.command_builder.render(prims, frame_index, self.swapchain.get_images()[frame_index].clone());
	
	let render_compleat_fence = self.graphics_queue.queue_submit(
            vec![
                SubmitInfo::new(
                    vec![(self.frame_infos[frame_index].present_finished.clone(), vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)],
                    vec![final_command_buffer],
                    vec![self.frame_infos[frame_index].render_complete.clone()]
                )
            ]
        ).expect("Failed to submit work to graphics queue");
	
        //Override inner fence so we can wait for this frame if the slot is used again
	self.frame_infos[frame_index].render_fence = Some(render_compleat_fence);
	
        match self.swapchain.queue_present(
            self.graphics_queue.clone(),
            vec![self.frame_infos[frame_index].render_complete.clone()],
            submit_index
        ){
            Ok(_present_resource) => {
		
	    },
            Err(er) => {
                println!("Could not present new frame: {}\ndropping frame!", er);
            }
        }
    }
    
    fn get_rendering_area(&self) -> Area {
	let new_dims = self.window.inner_size();
	Area{
	    from: (0.0, 0.0).into(),
	    to: (new_dims.width as f32, new_dims.height as f32).into()
	}
    }
    
    fn get_character_area(
	&self, character: &Character
    ) -> Area {
	self.command_builder.interface_pass.character.get_area_for_char(character)
    }
    
    fn set_cursor(&mut self, cursor: CursorIcon) {
        self.window.set_cursor_icon(to_winit_cursor_icon(cursor));
    }

    fn register_polygone(&self, name: &'static str, vertices: Vec<Vector2<f32>>, indices: Vec<u32>) -> PolyHandle {
	self.command_builder.interface_pass.polys.write().unwrap().register_polygone(name, vertices, indices)
    }
}
