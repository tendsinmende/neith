use neith::math::Area;

use crate::vulkan_prelude::*;
use crate::msw::winit;

pub fn area_to_scissors(area: Area, screen_area: Area) -> vk::Rect2D{

    let common_area = area.union(screen_area);
    
    let extent = common_area.extent();
    
    vk::Rect2D {
        offset: vk::Offset2D {
            x: common_area.from.x as i32,
            y: common_area.from.y as i32,
        },
        extent: vk::Extent2D {
            width: extent.x as u32,
            height: extent.y as u32,
        },
    }
}

///Converts a neith cursor to a winit cursor. (TODO: implement as macro?)
pub fn to_winit_cursor_icon(cursor: crate::neith::event::CursorIcon) -> winit::window::CursorIcon{
    match cursor{
	crate::neith::event::CursorIcon::Default => winit::window::CursorIcon::Default,
	crate::neith::event::CursorIcon::Crosshair => winit::window::CursorIcon::Crosshair, 
	crate::neith::event::CursorIcon::Hand => winit::window::CursorIcon::Hand,
	crate::neith::event::CursorIcon::Arrow => winit::window::CursorIcon::Arrow,
	crate::neith::event::CursorIcon::Move => winit::window::CursorIcon::Move,
	crate::neith::event::CursorIcon::Text => winit::window::CursorIcon::Text,
	crate::neith::event::CursorIcon::Wait => winit::window::CursorIcon::Wait,
	crate::neith::event::CursorIcon::Help => winit::window::CursorIcon::Help,
	crate::neith::event::CursorIcon::Progress => winit::window::CursorIcon::Progress,
	crate::neith::event::CursorIcon::NotAllowed => winit::window::CursorIcon::NotAllowed,
	crate::neith::event::CursorIcon::ContextMenu => winit::window::CursorIcon::ContextMenu,
	crate::neith::event::CursorIcon::Cell => winit::window::CursorIcon::Cell,
	crate::neith::event::CursorIcon::VerticalText => winit::window::CursorIcon::VerticalText,
	crate::neith::event::CursorIcon::Alias => winit::window::CursorIcon::Alias,
	crate::neith::event::CursorIcon::Copy => winit::window::CursorIcon::Copy,
	crate::neith::event::CursorIcon::NoDrop => winit::window::CursorIcon::NoDrop,
	crate::neith::event::CursorIcon::Grab => winit::window::CursorIcon::Grab,
	crate::neith::event::CursorIcon::Grabbing => winit::window::CursorIcon::Grabbing,
	crate::neith::event::CursorIcon::AllScroll => winit::window::CursorIcon::AllScroll,
	crate::neith::event::CursorIcon::ZoomIn => winit::window::CursorIcon::ZoomIn,
	crate::neith::event::CursorIcon::ZoomOut => winit::window::CursorIcon::ZoomOut,
	crate::neith::event::CursorIcon::EResize => winit::window::CursorIcon::EResize,
	crate::neith::event::CursorIcon::NResize => winit::window::CursorIcon::NResize,
	crate::neith::event::CursorIcon::NeResize => winit::window::CursorIcon::NeResize,
	crate::neith::event::CursorIcon::NwResize => winit::window::CursorIcon::NwResize,
	crate::neith::event::CursorIcon::SResize => winit::window::CursorIcon::SResize,
	crate::neith::event::CursorIcon::SeResize => winit::window::CursorIcon::SeResize,
	crate::neith::event::CursorIcon::SwResize => winit::window::CursorIcon::SwResize,
	crate::neith::event::CursorIcon::WResize => winit::window::CursorIcon::WResize,
	crate::neith::event::CursorIcon::EwResize => winit::window::CursorIcon::EwResize,
	crate::neith::event::CursorIcon::NsResize => winit::window::CursorIcon::NsResize,
	crate::neith::event::CursorIcon::NeswResize => winit::window::CursorIcon::NeswResize,
	crate::neith::event::CursorIcon::NwseResize => winit::window::CursorIcon::NwseResize,
	crate::neith::event::CursorIcon::ColResize => winit::window::CursorIcon::ColResize,
	crate::neith::event::CursorIcon::RowResize => winit::window::CursorIcon::RowResize,
    }
}
