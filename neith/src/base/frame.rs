use std::time::Instant;

use crate::Interface;
use crate::event::Event;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

pub struct Frame{
    area: Area,
    border_width: Option<Unit>,
    pub child: Box<dyn Widget + Send>,
}

impl Frame{
    pub fn new<W>(child: W) -> Self where W: Widget + Send + 'static{
	Frame{
	    area: Area::empty(),
	    border_width: None,
	    child: Box::new(child)
	}
    }

    pub fn with_border_width(mut self, width: Unit) -> Self{
	self.border_width = Some(width);
	self
    }

    fn border_width(&self) -> Unit{
	self.border_width.unwrap_or(crate::get_config().border_slim as Unit)
    }

    pub fn set_border_width(&mut self, width: Unit){
	self.border_width = Some(width)
    }
}

impl Widget for Frame{

    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){
	let offset = Vector2::new(self.border_width(), self.border_width());
	self.child.on_event(&event.offset(offset * -1.0), &(*location - offset));
    }

    fn area(&self) -> Area{
	self.area
    }
    
    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls {

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent
	};

	let border_width = self.border_width();
	let child_area = Area{
	    from: Vector2::new(border_width, border_width),
	    to: host_extent - Vector2::new(border_width, border_width),
	};

	let mut calls = DrawCalls::new(vec![
	    //Frame color
	    Prim{
		prim: PrimType::Rect,
		drawing_area: self.area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background_sec),
		level: level
	    },
	    Prim{
		prim: PrimType::Rect,
		drawing_area: child_area,
		scissors: None,
		style: Style::from_color(crate::get_styling().background),
		level: level
	    },
	]);
	let mut child_calls = self.child.get_calls(
	    child_area.extent(),
	    draw_time,
	    interface,
	    level + 1
	);

	child_calls.offset(Vector2::new(2.0 * border_width, 2.0 * border_width));
	child_calls.clip_to(child_area.extent());
	calls.merge(child_calls);
	calls
    }
}
