use crate::Interface;
use crate::event::Event;
use crate::event::KeyState;
use crate::event::MouseButton;
use crate::event::WindowEvent;
use crate::math::Area;
use crate::math::Unit;
use crate::math::Vector2;
use crate::primitive::Prim;
use crate::primitive::PrimType;
use crate::widget::DrawCalls;
use crate::widget::Style;
use crate::widget::Widget;

use crate::base::Void;

use std::time::Instant;

use crate::layout::Direction;

#[derive(Debug)]
pub enum SplitType{
    ///Number of pixel from left/top. Or, if the parent split is in inverse mode, from right/bottom
    Absolute(Unit),
    ///Percentage from left/top at which the split is. Or, if the parent split is in inverse mode, from right/bottom.
    ///
    /// Needs to be in the interval [0.0, 1.0], Values outside are clipped.
    Relative(f32),
}

impl SplitType{
    ///Returns the location of the split on a 1D interval.
    pub fn loc_on_interval(&self, interval: Unit, inv: bool) -> Unit{
        match self{
            SplitType::Absolute(t) => if inv { (interval - t).max(0.0).min(interval) } else { t.min(interval).max(0.0) },
            SplitType::Relative(perc) => if inv { interval - (interval as f32 * perc.max(0.0).min(1.0)) } else { interval as f32 * perc.max(0.0).min(1.0) },
        }        
    }

    pub fn set_location(&mut self, extent: Unit, location: Unit, inv: bool){
	match self{
	    SplitType::Absolute(t) => if inv { *t = extent - location.min(extent).max(0.0) } else { *t = location.min(extent).max(0.0) },
	    SplitType::Relative(rel) => if inv { *rel = 1.0 -  (location.max(0.0).min(extent) / extent) } else { *rel = location.max(0.0).min(extent) / extent},
	}
    }
}

pub struct Split{
    pub one: Box<dyn Widget + Send>,
    pub two: Box<dyn Widget + Send>,

    pub ty: SplitType,
    pub dir: Direction,

    ///Width of the inner border aka the horizontal/vertical split line
    pub border_width: Option<Unit>,
    ///Width of the highlight within the border. If it is 0.0, none is rendered. 
    pub border_highlight: Option<Unit>,
    ///if the splits location can be moved
    pub moveable: bool,
    is_dragging: bool,

    ///True if the split should use a reference reference for the split.
    ///Usually SplitType::Absolute(50.0) means "50px from left/top" when true, this means "from right/bottom"
    is_inv: bool,
    
    area: Area 
}

impl Split{

    pub fn new() -> Self{
	Split{
	    one: Box::new(Void::new()),
	    two: Box::new(Void::new()),
	    ty: SplitType::Relative(0.5),
	    dir: Direction::Horizontal,

	    border_width: None,
	    border_highlight: None,
	    moveable: false,
	    is_dragging: false,

	    is_inv: false,
	    
	    area: Area::empty(),
	}
    }

    pub fn with_split_type(mut self, ty: SplitType) -> Self{
	self.ty = ty;
	self
    }

    pub fn with_direction(mut self, dir: Direction) -> Self{
	self.dir = dir;
	self
    }
    
    pub fn with_border(mut self, border_width: Unit) -> Self{
	self.border_width = Some(border_width);
	self
    }

    pub fn with_highligh_width(mut self, highlight: Unit) -> Self{
	self.border_highlight = Some(highlight);
	self
    }

    pub fn with_moveable(mut self, is_moveable: bool) -> Self{
	self.moveable = is_moveable;
	self
    }

    pub fn with_one<W>(mut self, new: W) -> Self
    where W: Widget + Send + 'static{
	let _created = self.set_one(new);
	self
    }

    pub fn with_two<W>(mut self, new: W) -> Self
    where W: Widget + Send + 'static{
	let _created = self.set_two(new);
	self
    }

    pub fn with_one_boxed(mut self, new: Box<dyn Widget + Send + 'static>) -> Self{
	self.one = new;
	self
    }

    pub fn with_two_boxed(mut self, new: Box<dyn Widget + Send + 'static>) -> Self{
	self.two = new;
	self
    }

    ///True if the split should use a referce reference for the split.
    ///Usually SplitType::Absolute(50.0) means "50px from left/top" when true, this means "from right/bottom"
    pub fn with_inv_reference(mut self) -> Self{
	self.is_inv = true;
	self
    }
    
    ///Sets the first child of the split, returns the old one
    pub fn set_one<W>(&mut self, new: W) -> Box<dyn Widget + Send + 'static>
    where W: Widget + std::marker::Send + 'static
    {
	let mut tmp: Box<dyn Widget + Send + 'static> = Box::new(new);
	std::mem::swap(&mut self.one, &mut tmp);
	tmp
    }

    pub fn set_two<W>(&mut self, new: W) -> Box<dyn Widget + Send + 'static>
    where W: Widget + std::marker::Send + 'static
    {
	let mut tmp: Box<dyn Widget + Send + 'static> = Box::new(new);
	std::mem::swap(&mut self.two, &mut tmp);
	tmp
    }
    
    fn get_two_offset(&self) -> Vector2<Unit>{
	let border_width: Unit = self.border_width.unwrap_or(crate::get_config().border_width as f32);
	match self.dir{
            Direction::Vertical => {
                //Where on the x axis the split is
                let split_loc = self.area.from.x + self.ty.loc_on_interval(self.area.extent().x, self.is_inv);

		Vector2{
		    x: split_loc + border_width / 2.0,
		    y: 0.0
		}
            },
            Direction::Horizontal => {
                //Where on the x axis the split is
                let split_loc = self.area.from.y + self.ty.loc_on_interval(self.area.extent().y, self.is_inv);

		Vector2{
		    x: 0.0,
		    y: split_loc + border_width / 2.0
		}
            }
        }
    }

    ///generates the area in which the split handle is displayed.
    fn split_handle_area(&self) -> Area{
	let border_width: Unit = self.border_width.unwrap_or(crate::get_config().border_width as f32);
	
	match self.dir{
	    Direction::Vertical => {
		let split_loc = self.area.from.x + self.ty.loc_on_interval(self.area.extent().x, self.is_inv);
		Area{
		    from: (split_loc - border_width, 0.0).into(),
		    to: (split_loc + border_width, self.area.to.y).into()
		}
		
	    },
	    Direction::Horizontal => {
		let split_loc = self.area.from.y + self.ty.loc_on_interval(self.area.extent().y, self.is_inv);
		Area{
		    from: (0.0, split_loc - border_width).into(),
		    to: (self.area.to.x, split_loc + border_width).into()
		}
	    }
	}
    }
}

impl Widget for Split{
    ///Broadcasts event into tree.
    fn on_event(&mut self, event: &Event, location: &Vector2<Unit>){

	//If we area moveable, check if we have to move
	if self.moveable{
	    match event{
		Event::WindowEvent(WindowEvent::MouseClick{button: MouseButton::Left, state}) => {
		    match state{
			KeyState::Pressed => {
			    //If press occurred in split area, start dragging
			    if self.split_handle_area().is_in(location){
				self.is_dragging = true;
			    }
			},
			KeyState::Released => {
			    //Reset dragging
			    self.is_dragging = false;
			}
		    }
		},
		Event::WindowEvent(WindowEvent::CursorDrag{start: _, end}) => {
		    //if is dragging, set location to it
		    if self.is_dragging{
			match self.dir{
			    Direction::Vertical => {
				self.ty.set_location(self.area.extent().x, end.0, self.is_inv);
			    },
			    Direction::Horizontal => {
				self.ty.set_location(self.area.extent().y, end.1, self.is_inv);
			    }
			}
		    }
		}
		_ => {}
	    }
	}
	
	self.one.on_event(event, location);

	//Offset event for lower children
	let offset = self.get_two_offset();
	let e = event.offset(Vector2::new(-1.0, -1.0) * offset);
	
	self.two.on_event(&e, &(*location - offset));
    }
    

    ///Returns the area this widget currently covers
    fn area(&self) -> Area{
        self.area
    }

    fn get_calls(
	&mut self,
	host_extent: Vector2<Unit>,
	draw_time: Instant,
	interface: &Interface,
	level: usize
    ) -> DrawCalls{

	self.area = Area{
	    from: (0.0, 0.0).into(),
	    to: host_extent,
	};
	
	//Draw the border, and if needed its highlight
	let mut inner_level = level;
	let mut primitives = vec![];
	primitives.push(Prim{
	    prim: PrimType::Rect,
	    drawing_area: self.area,
	    scissors: None,
	    style: Style::from_color(crate::get_styling().background),
	    level: inner_level
	});
	inner_level += 1;

	
	let border_width: Unit = self.border_width.unwrap_or(crate::get_config().border_width as f32);
	let highlight_width: Unit = self.border_highlight.unwrap_or(crate::get_config().border_highlight_width as f32);


	let one_extent: Vector2<Unit>;
	let two_extent: Vector2<Unit>; 
	let two_offset: Vector2<Unit>;
	match self.dir{
            Direction::Vertical => {
                //Where on the x axis the split is
                let split_loc = self.area.from.x + self.ty.loc_on_interval(self.area.extent().x, self.is_inv);

		
		//The extents of the two sub areas
		one_extent = Vector2{
		    x: split_loc - border_width / 2.0,
		    y: host_extent.y,
		};

		two_offset = Vector2{
		    x: split_loc + border_width / 2.0,
		    y: 0.0
		};

		two_extent = host_extent - two_offset;
		//Start and end of the middle bar
		let one_loc = split_loc - border_width / 2.0;
		let two_loc = split_loc + border_width / 2.0;

		let outer_area = Area{
		    from: (one_loc, self.area.from.y).into(),
		    to: (two_loc, self.area.to.y).into()
		};

		//The Background
		primitives.push(
		    Prim{
			prim: PrimType::Rect,
			drawing_area: outer_area,
			scissors: None,
			style: Style::from_color(crate::get_styling().background_sec),
			level: inner_level
		    }
		);

		inner_level += 1;

		//If given, draw highlight
		if highlight_width > 0.0{
		    let highlight_area = Area{
			from: ((split_loc - highlight_width / 2.0).floor(), self.area.from.y.floor()).into(),
			to: ((split_loc + highlight_width / 2.0).ceil(), self.area.to.y.ceil()).into()
		    };

		    primitives.push(
			Prim{
			    prim: PrimType::Rect,
			    drawing_area: highlight_area,
			    scissors: None,
			    style: Style::from_color(crate::get_styling().highlight),
			    level: inner_level
			}
		    );

		    inner_level += 1;
		}
            },
            Direction::Horizontal => {
                //Where on the x axis the split is
                let split_loc = self.area.from.y + self.ty.loc_on_interval(self.area.extent().y, self.is_inv);
		
		let one_loc = split_loc - border_width / 2.0;
		let two_loc = split_loc + border_width / 2.0;
		
		let outer_area = Area{
		    from: (self.area.from.x, one_loc).into(),
		    to: (self.area.to.x, two_loc).into()
		};
		
		//The extents of the two sub areas
		one_extent = Vector2{
		    x: host_extent.x,
		    y: split_loc - border_width / 2.0,
		};

		two_offset = Vector2{
		    x: 0.0,
		    y: split_loc + border_width / 2.0
		};

		two_extent = host_extent - two_offset;
		
		//The Background
		primitives.push(
		    Prim{
			prim: PrimType::Rect,
			drawing_area: outer_area,
			scissors: None,
			style: Style::from_color(crate::get_styling().background_sec),
			level: inner_level
		    }
		);

		inner_level += 1;

		//If given, draw highlight
		if highlight_width > 0.0{
		    let highlight_area = Area{
			from: (self.area.from.x.floor(), (split_loc - highlight_width / 2.0).floor()).into(),
			to: (self.area.to.x.ceil(), (split_loc + highlight_width / 2.0).ceil()).into()
		    };

		    primitives.push(
			Prim{
			    prim: PrimType::Rect,
			    drawing_area: highlight_area,
			    scissors: None,
			    style: Style::from_color(crate::get_styling().highlight),
			    level: inner_level
			}
		    );

		    inner_level += 1;
		}
            }
        }
	
	let mut one = self.one.get_calls(one_extent, draw_time, interface, inner_level);
	one.clip_to(one_extent);
	let mut two = self.two.get_calls(two_extent, draw_time, interface, inner_level);
	two.clip_to(two_extent);
	
	two.offset(two_offset);

	one.merge(two);
	one.add(primitives);

	one

    }
}
