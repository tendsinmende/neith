extern crate neith;
#[macro_use]
extern crate marp;
///winit bindings for marp.
pub extern crate marp_surface_winit as msw;
///Winit crate used internally to load up windows. Can be used to get informations about the backend's
/// inner `window` field.
pub use msw::winit;


extern crate rusttype;
extern crate lyon;

use msw::winit::{
    event_loop::{
	EventLoop,
	ControlFlow
    },
    event::{
	Event,
	WindowEvent,
	DeviceEvent,
	MouseButton,
	ElementState,
	VirtualKeyCode,
	MouseScrollDelta
    },
    window::{
	Theme,
	Window,
	WindowBuilder
    },
};

///Contains everything needed to draw any possible character from the currently used set of fonts.
pub(crate) mod character;
///contains everything to draw a normal rectangle
pub(crate) mod rectangle;
///Used to build the command buffer which is submitted and shown on the backends surface.
pub(crate) mod command_builder;
///Everything to setup the commandbuffer for displaying all supplied primitives of the interface
pub(crate) mod interface_pass;
///Everything to attach post progress stages to the rendered interface. This contains bloom and drop shadows.
pub(crate) mod post_progress_pass;

pub(crate) mod pipelines;

///Line drawing related rendering code
pub(crate) mod line;
///Polygon related drawing code
pub(crate) mod polygon;

pub mod util;

pub(crate) mod vulkan_prelude{
    pub use crate::util::*;
    pub use crate::marp::{
	device::{
	    Device,
	    PhysicalDevice,
	    Queue,
	    QueueFamily,
	    queue::SubmitInfo,
	},
	framebuffer::{
	    RenderPass,
	    Framebuffer
	},
	image::{
	    Image,
	    ImageInfo,
	    MipLevel,
	    ImageType,
	    ImageUsage,
	    AbstractImage
	},
	buffer::{
	    UploadStrategy,
	    SharingMode,
	    Buffer,
	    BufferUsage
	},
	memory::MemoryUsage,
	swapchain::Swapchain,
	command_buffer::{
	    CommandPool,
	    CommandBuffer,
	    CommandBufferPool
	},
	pipeline::{
	    Pipeline,
	    PipelineLayout,
	    GraphicsPipeline,
	    ComputePipeline,
	    ViewportMode,
	    ViewportState,
	    PipelineState,
	    ColorBlendAttachmentState,
	    ColorBlendState,
	    DepthStencilState,
	    StencilTestState,
	    MultisampleState,
	    RasterizationState,
	    InputAssemblyState,
	    VertexInputState,
	},
	shader::{
	    Stage,
	    ShaderModule,
	    AbstractShaderModule
	},
	descriptor::{
	    *
	},
	sync::{
	    Semaphore,Fence
	},
	util::*,
	ash::vk,
    };
}
pub mod backend;
pub use backend::BackendProvider;

pub mod input;
pub use input::{
    InputProvider,
    InputReceiver
};

///A Convenience function that create an interface for you and starts a `main` function on another thread, since
/// the input has to be captured on the main thread.
///
///However, you'll have to implement the update logic yourself. Either you choose the normal updating by executing `update()` every n milliseconds, or you do something
/// fancy via async signals or tasks.
///
/// # Example for polling
/// ```
/// fn main(){
///     neith_marp::new_interface(None, None, |mut interface|{
///	    while !interface.update(){
///	        std::thread::sleep(std::time::Duration::from_millis(16));
///	    }
///     });
/// }
/// ```
pub fn new_interface<F: 'static>(window_builder: Option<WindowBuilder>, config_path: Option<&'static str>, main_func: F) where F: std::marker::Send + Fn(neith::Interface){
    let input = InputProvider::new();
    let backend = input.new_backend(window_builder).expect("Failed to create vulkan backend!"); //TODO configure window builder
    
    let input_receiver = input.get_input_receiver();
    //Create the interface and throw it into a new thread
    let _handle = std::thread::spawn(move ||{
	let interface = neith::Interface::new(backend, input_receiver, config_path);

	main_func(interface);
    });


    //Start input loop
    input.run()
}
