use marp::swapchain::surface::Surface;
use std::sync::Arc;
use crate::*;
use crate::marp::*;
use crate::marp::ash;
use msw::WinitSurface;
use msw::winit;
#[cfg(unix)]
use winit::platform::unix::EventLoopWindowTargetExtUnix;
use ash::vk;

use crate::neith::com::{Recv, Sender};
use crate::command_builder::CommandBuilder;
use crate::backend::FrameInfo;

pub enum InputProviderEvent{
    Shutdown,
    ExternEvent(neith::event::Event),
}

pub struct InputProvider{
    eventloop: EventLoop<()>,
    ///Used to send the events from the event loop somewhere
    sender: Sender<neith::event::Event>,
    recv: Recv<InputProviderEvent>,
}

impl InputProvider{
    pub fn new() -> Self{
	InputProvider{
	    eventloop: EventLoop::new(),
	    sender: Sender::new(),
	    recv: Recv::new(),
	}
    }

	#[cfg(unix)]
    fn is_on_wayland(&self) -> bool {
		self.eventloop.is_wayland()
    }
	
	
	#[cfg(windows)]
    fn is_on_wayland(&self) -> bool {
		false
	}
	
    ///Creates a new backend that shows it output in a window. The window can be constructed from a `window_builder`, otherwise default values are used.
    pub fn new_backend(&self, window_builder: Option<WindowBuilder>) -> Result<BackendProvider, ()>{

	let window = if let Some(wb) = window_builder{
	    wb.build(&self.eventloop).expect("Could not construct window from provided builder!")
	}else{
	    Window::new(&self.eventloop).expect("Failed to build default window!")
	}; 

	let mut app_info = miscellaneous::AppInfo::default("MyApp".to_string());
        app_info.set_api_version(miscellaneous::Version::new(1,1,0));

	
        //Currently disabling wayland since we wanna use renderdoc.
        let mut extensions = miscellaneous::InstanceExtensions::presentable();
	
	extensions.wayland_surface = self.is_on_wayland();
	//Since wayland support breaks Renderdoc, disable wayland when not in wayland
	
        let layer = if cfg!(feature = "debug-layer"){
	    Some(miscellaneous::InstanceLayer::debug_layers())
	}else{
	    None
	};
	
	
        let window_extent = window.inner_size();
        let swapchain_extent = vk::Extent2D::builder()
            .width(window_extent.width as u32)
            .height(window_extent.height as u32)
            .build();
        
        //The vulkan instance we'll be using
        let instance = match instance::Instance::new(
            Some(app_info),
            Some(extensions),
            layer,
            None
        ){
            Ok(inst) => inst,
            Err(er) => {
                println!("failed to create instance: {}\ndoes your gfx card and driver support vulkan?", er);
                return Err(());
            },
        };

        //Create a surface for the window
        let surface: Arc<dyn Surface + Send + Sync> = WinitSurface::new(
            instance.clone(),
            &window,
            &self.eventloop
        ).expect("Failed to create window");


        //Create a physical device 
        let p_devices = device::physical_device::PhysicalDevice::find_physical_device(instance.clone())
            .expect("failed to find physical devices");
        
        //find the physical device that supports presentation on out surface.
        let (p_device, index) = p_devices.into_iter().filter_map(|pdev|{
            match pdev.find_present_queue_family(&surface){
                Ok(queue_idx) => {
                    println!("neith-window: DEBUG: Found suitable physical device");
                    Some((pdev, queue_idx))
                },
                Err(er) => {
                    println!("neith-window: WARNING: Could not find presentable surface for physical device: {:?}", er);
                    None
                }
            }
        }).nth(0).expect("failed to find suitable device for presenting!");



        //Create one queue for the 
        let queue_create_info = vec![(*p_device.get_queue_family_by_index(index).expect("Failed to get graphics queue family"), 1.0 as f32)];
        let needed_device_ext = vec![
            miscellaneous::DeviceExtension::new("VK_KHR_swapchain".to_string(), 1),
        ];
        let (device, mut queues) = device::device::Device::new(
            instance.clone(),
            p_device.clone(),
            queue_create_info,
            Some(&needed_device_ext),
            Some(*p_device.get_features())
        ).expect("failed to find Device and its queues");
        println!("neith-window: DEBUG: found {}, present queues", queues.len());
        let queue = queues.pop().expect("Could not get our present queue!");


        //Find a depth and color format that us unorm, otherwise the colors might look off
        let swapchain_format = surface
            .get_supported_formats(device.get_physical_device())
            .expect("Failed to get surface formats").into_iter().fold(None, |b, f|{
		println!("Found format: {:?}", f.format);
                if let None = b{
		    if f.format == vk::Format::B8G8R8A8_UNORM{
			Some(f)
		    }else{
			None
		    }
                }else{
                    b
                }
            }).expect("Could not find b8g8r8a8-unorm format");

	println!("Swapchain supported formats:");
        for i in surface.get_supported_formats(device.get_physical_device()).expect("No formats!"){
            println!("    {:#?}, cs: {:#?}", i.format, i.color_space);
        }
        
        //Create swapchain
        let swapchain = swapchain::Swapchain::new(
            device.clone(),
            surface.clone(),
            swapchain_extent,
            Some(swapchain_format),
            Some(2),
            Some(ash::vk::PresentModeKHR::FIFO_RELAXED),
            Some(image::ImageUsage{
                color_attachment: true,
                transfer_dst: true,
                .. Default::default()
            })
        ).expect("Failed to create swapchain!");

	let mut backend_provider = BackendProvider{
	    frame_infos: (0..swapchain.image_count()).into_iter().map(|_i| FrameInfo::new(device.clone())).collect(),
	    
	    command_builder: CommandBuilder::new(
		device.clone(),
		queue.clone(),
		(window.inner_size().width, window.inner_size().height),
		swapchain.image_count() as usize,
		swapchain_format.format
	    ),

	    last_frame_index: 0,
	    frame_count: swapchain.image_count() as usize,

	    window_extent: (window.inner_size().width, window.inner_size().height),
	    
	    window,
	    device,
	    graphics_queue: queue,
	    swapchain,

	    framebuffer_dirty: false,
	};

	//For some reason the reported swapchain extent is not always correct, therefore resize
	backend_provider.resize();
	
	Ok(backend_provider)
    }

    ///Returns a new sender that can be used to send controlling events to the loop.
    pub fn get_input_provider_event_sender(&self) -> Sender<InputProviderEvent>{
	self.recv.new_sender()
    }

    pub fn get_input_receiver(&self) -> InputReceiver{
	InputReceiver{
	    recv: self.sender.new_receiver(),
	    controll_sender: self.recv.new_sender()
	}
    }

    ///Runs the input loop until it receives an `Shutdown` Event. This either happens if you call `should_end()` on
    /// a from this created `InputReceiver`, or if you took a sender via `get_input_provider_event_sender()` and send the
    /// event `InputProviderEvent::Shutdown`. 
    ///
    ///#Safety
    ///This must be executed on the main thread of the application. Otherwise this function will panic.
    pub fn run(self){

	let ev_loop = self.eventloop;
	let sender = self.sender;
	let recv = self.recv;

	let mut last_draw = std::time::Instant::now();
	
	ev_loop.run(move |event, _, control_flow|{	    
	    //Only poll when needed
	    //*control_flow = ControlFlow::WaitUntil(std::time::Instant::now() + std::time::Duration::from_millis(16));
	    *control_flow = ControlFlow::Poll;
	    //Check the control information from outside the input provider
	    for controll_event in recv.recv(){
		match controll_event{
		    InputProviderEvent::Shutdown => {
			//Should end the loop based on the acquired event
			*control_flow = ControlFlow::Exit
		    },
		    InputProviderEvent::ExternEvent(e) => {
			println!("Got External event");
			sender.send(e);
		    },
		}
	    }
	    
	    //Check the event loop, if the OS request a redraw, send an additional redraw event
	    match event{
		Event::WindowEvent {
		    event: WindowEvent::CloseRequested,
		    ..
		} => {
		    sender.send(neith::event::Event::MetaEvent(neith::event::MetaEvent::CloseRequest));
		    *control_flow = ControlFlow::Exit
		},
		Event::MainEventsCleared => {
		},
		Event::RedrawRequested(_) => {
		    last_draw = std::time::Instant::now();
		    
		    sender.send(neith::event::Event::MetaEvent(neith::event::MetaEvent::Redraw))
		},
		_=> ()
	    }
	    
	    if last_draw.elapsed() > std::time::Duration::from_millis(8){
		last_draw = std::time::Instant::now();
		sender.send(neith::event::Event::MetaEvent(neith::event::MetaEvent::Redraw));
	    }
	    
	    if let Some(neith_event) = to_neith_event(event){
		sender.send(neith_event);
	    }
	})
    }
}

///Receives some input from the connected `InputProvider`. Can also send an `End` signal to the InputProvide in case that it should
/// stop the input polling and return.
///
/// The InputReceiver is created from a InputProvider via `input_provider.get_input_receiver()`.
pub struct InputReceiver{
    recv: Recv<neith::event::Event>,
    controll_sender: Sender<InputProviderEvent>,
}

impl neith::Input for InputReceiver{
    fn get_events(&self) -> Vec<neith::event::Event> {
	self.recv.recv()
    }

    fn should_end(&self){
	self.controll_sender.send(InputProviderEvent::Shutdown);
    }
    fn add_event(&self, event: neith::event::Event) {
	self.controll_sender.send(InputProviderEvent::ExternEvent(event));
    }
}

fn to_neith_event<'a>(event: Event<'a, ()>) -> Option<neith::event::Event>{
    match event{
	Event::WindowEvent{
	    window_id: _,
	    event
	} => {
	    match event{
		WindowEvent::CloseRequested => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::CloseRequest)),
		WindowEvent::Destroyed => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::Destroyed)),
		WindowEvent::Resized(new_size) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::Resized(neith::math::Area{
		    from: (0.0, 0.0).into(),
		    to: (new_size.width as f32, new_size.height as f32).into()
		}))),
		WindowEvent::Moved(new_pos) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::Moved((new_pos.x, new_pos.y).into()))),
		WindowEvent::DroppedFile(path) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::DroppedFile(path))),
		WindowEvent::HoveredFile(path) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::HoveredFile(path))),
		WindowEvent::HoveredFileCancelled => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::HoveredFileCancelled)),
		WindowEvent::ReceivedCharacter(c) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::ReceivedCharacter(c))),
		WindowEvent::Focused(b) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::Focused(b))),
		WindowEvent::ModifiersChanged(mods) => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::ModifiersChanged{
		    shift: mods.shift(),
		    ctrl: mods.ctrl(),
		    alt: mods.alt(),
		    super_key: mods.logo()
		})),
		WindowEvent::CursorMoved{device_id: _, position, modifiers: _} => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::CursorMoved((position.x, position.y)))),
		WindowEvent::CursorEntered{device_id: _} => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::CursorEntered)),
		WindowEvent::CursorLeft{device_id: _} => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::CursorLeft)),
		WindowEvent::MouseInput{device_id: _, state, button, modifiers: _} => Some(neith::event::Event::WindowEvent(neith::event::WindowEvent::MouseClick{
		    button: match button{
			MouseButton::Left => neith::event::MouseButton::Left,
			MouseButton::Right => neith::event::MouseButton::Right,
			MouseButton::Middle => neith::event::MouseButton::Middle,
			MouseButton::Other(r) => neith::event::MouseButton::Other(r),
		    },
		    state: match state{
			ElementState::Pressed => neith::event::KeyState::Pressed,
			ElementState::Released => neith::event::KeyState::Released,
		    }
		})),
		WindowEvent::MouseWheel{device_id: _, delta, phase: _, modifiers: _} => {
		    Some(
			neith::event::Event::WindowEvent(
			    neith::event::WindowEvent::Scroll(match delta{
				MouseScrollDelta::LineDelta(x,y) => (x,y),
				MouseScrollDelta::PixelDelta(pos) => (pos.x as f32, pos.y as f32)
			    })
			)
		    )
		}
		WindowEvent::ThemeChanged(th) => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::ThemeChanged(match th{
		    Theme::Dark => neith::event::Theme::Dark,
		    Theme::Light => neith::event::Theme::Light,
		}))),
		WindowEvent::KeyboardInput{device_id: _, input, is_synthetic: _} => if let Some(_key) = input.virtual_keycode{
		    Some(
			neith::event::Event::WindowEvent(neith::event::WindowEvent::Key(neith::event::Key{
			    code: match input.virtual_keycode{
				Some(code) => neith_key_code(&code),
				None => return None,
			    },
			    state: match input.state{
				ElementState::Pressed => neith::event::KeyState::Pressed,
				ElementState::Released => neith::event::KeyState::Released,
			    },
			}))
		    )
		}else{
		    None
		}
		//TODO Add touch input ?
		_ => None
	    }
	},
	Event::DeviceEvent{
	    device_id: _,
	    event
	} => {
	    Some(neith::event::Event::DeviceEvent(
		match event{
		    DeviceEvent::MouseMotion{delta} => neith::event::DeviceEvent::MouseMoved(delta),
		    DeviceEvent::MouseWheel{delta} => {			
			neith::event::DeviceEvent::MouseWheel(match delta{
			    MouseScrollDelta::LineDelta(x,y) => (x,y),
			    MouseScrollDelta::PixelDelta(pos) => (pos.x as f32, pos.y as f32)
			})
		    },
		    //NOTE: Currently ignoring all other device events, since we are more considering the Window events for UI
		    _ => return None,
		}
	    ))
	},
	Event::Suspended => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::Suspended)),
	Event::Resumed => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::Resumed)),
	Event::RedrawRequested(_) => Some(neith::event::Event::MetaEvent(neith::event::MetaEvent::Redraw)),
	_ => return None, //All other events are not handled
    }
}


fn neith_key_code(code: &VirtualKeyCode) -> neith::event::KeyCode{
    match code{
	VirtualKeyCode::Key1 => neith::event::KeyCode::Key1,
	VirtualKeyCode::Key2 => neith::event::KeyCode::Key2,
	VirtualKeyCode::Key3 => neith::event::KeyCode::Key3,
	VirtualKeyCode::Key4 => neith::event::KeyCode::Key4,
	VirtualKeyCode::Key5 => neith::event::KeyCode::Key5,
	VirtualKeyCode::Key6 => neith::event::KeyCode::Key6,
	VirtualKeyCode::Key7 => neith::event::KeyCode::Key7,
	VirtualKeyCode::Key8 => neith::event::KeyCode::Key8,
	VirtualKeyCode::Key9 => neith::event::KeyCode::Key9,
	VirtualKeyCode::Key0 => neith::event::KeyCode::Key0,
	VirtualKeyCode::A => neith::event::KeyCode::A,
	VirtualKeyCode::B => neith::event::KeyCode::B,
	VirtualKeyCode::C => neith::event::KeyCode::C,
	VirtualKeyCode::D => neith::event::KeyCode::D,
	VirtualKeyCode::E => neith::event::KeyCode::E,
	VirtualKeyCode::F => neith::event::KeyCode::F,
	VirtualKeyCode::G => neith::event::KeyCode::G,
	VirtualKeyCode::H => neith::event::KeyCode::H,
	VirtualKeyCode::I => neith::event::KeyCode::I,
	VirtualKeyCode::J => neith::event::KeyCode::J,
	VirtualKeyCode::K => neith::event::KeyCode::K,
	VirtualKeyCode::L => neith::event::KeyCode::L,
	VirtualKeyCode::M => neith::event::KeyCode::M,
	VirtualKeyCode::N => neith::event::KeyCode::N,
	VirtualKeyCode::O => neith::event::KeyCode::O,
	VirtualKeyCode::P => neith::event::KeyCode::P,
	VirtualKeyCode::Q => neith::event::KeyCode::Q,
	VirtualKeyCode::R => neith::event::KeyCode::R,
	VirtualKeyCode::S => neith::event::KeyCode::S,
	VirtualKeyCode::T => neith::event::KeyCode::T,
	VirtualKeyCode::U => neith::event::KeyCode::U,
	VirtualKeyCode::V => neith::event::KeyCode::V,
	VirtualKeyCode::W => neith::event::KeyCode::W,
	VirtualKeyCode::X => neith::event::KeyCode::X,
	VirtualKeyCode::Y => neith::event::KeyCode::Y,
	VirtualKeyCode::Z => neith::event::KeyCode::Z,
	VirtualKeyCode::Escape => neith::event::KeyCode::Escape,
	VirtualKeyCode::F1 => neith::event::KeyCode::F1,
	VirtualKeyCode::F2 => neith::event::KeyCode::F2,
	VirtualKeyCode::F3 => neith::event::KeyCode::F3,
	VirtualKeyCode::F4 => neith::event::KeyCode::F4,
	VirtualKeyCode::F5 => neith::event::KeyCode::F5,
	VirtualKeyCode::F6 => neith::event::KeyCode::F6,
	VirtualKeyCode::F7 => neith::event::KeyCode::F7,
	VirtualKeyCode::F8 => neith::event::KeyCode::F8,
	VirtualKeyCode::F9 => neith::event::KeyCode::F9,
	VirtualKeyCode::F10 => neith::event::KeyCode::F10,
	VirtualKeyCode::F11 => neith::event::KeyCode::F11,
	VirtualKeyCode::F12 => neith::event::KeyCode::F12,
	VirtualKeyCode::F13 => neith::event::KeyCode::F13,
	VirtualKeyCode::F14 => neith::event::KeyCode::F14,
	VirtualKeyCode::F15 => neith::event::KeyCode::F15,
	VirtualKeyCode::F16 => neith::event::KeyCode::F16,
	VirtualKeyCode::F17 => neith::event::KeyCode::F17,
	VirtualKeyCode::F18 => neith::event::KeyCode::F18,
	VirtualKeyCode::F19 => neith::event::KeyCode::F19,
	VirtualKeyCode::F20 => neith::event::KeyCode::F20,
	VirtualKeyCode::F21 => neith::event::KeyCode::F21,
	VirtualKeyCode::F22 => neith::event::KeyCode::F22,
	VirtualKeyCode::F23 => neith::event::KeyCode::F23,
	VirtualKeyCode::F24 => neith::event::KeyCode::F24,
	VirtualKeyCode::Snapshot => neith::event::KeyCode::Snapshot,
	VirtualKeyCode::Scroll => neith::event::KeyCode::Scroll,
	VirtualKeyCode::Pause => neith::event::KeyCode::Pause,
	VirtualKeyCode::Insert => neith::event::KeyCode::Insert,
	VirtualKeyCode::Home => neith::event::KeyCode::Home,
	VirtualKeyCode::Delete => neith::event::KeyCode::Delete,
	VirtualKeyCode::End => neith::event::KeyCode::End,
	VirtualKeyCode::PageDown => neith::event::KeyCode::PageDown,
	VirtualKeyCode::PageUp => neith::event::KeyCode::PageUp,
	VirtualKeyCode::Left => neith::event::KeyCode::Left,
	VirtualKeyCode::Up => neith::event::KeyCode::Up,
	VirtualKeyCode::Right => neith::event::KeyCode::Right,
	VirtualKeyCode::Down => neith::event::KeyCode::Down,
	VirtualKeyCode::Back => neith::event::KeyCode::Back,
	VirtualKeyCode::Return => neith::event::KeyCode::Return,
	VirtualKeyCode::Space => neith::event::KeyCode::Space,
	VirtualKeyCode::Compose => neith::event::KeyCode::Compose,
	VirtualKeyCode::Caret => neith::event::KeyCode::Caret,
	VirtualKeyCode::Numlock => neith::event::KeyCode::Numlock,
	VirtualKeyCode::Numpad0 => neith::event::KeyCode::Numpad0,
	VirtualKeyCode::Numpad1 => neith::event::KeyCode::Numpad1,
	VirtualKeyCode::Numpad2 => neith::event::KeyCode::Numpad2,
	VirtualKeyCode::Numpad3 => neith::event::KeyCode::Numpad3,
	VirtualKeyCode::Numpad4 => neith::event::KeyCode::Numpad4,
	VirtualKeyCode::Numpad5 => neith::event::KeyCode::Numpad5,
	VirtualKeyCode::Numpad6 => neith::event::KeyCode::Numpad6,
	VirtualKeyCode::Numpad7 => neith::event::KeyCode::Numpad7,
	VirtualKeyCode::Numpad8 => neith::event::KeyCode::Numpad8,
	VirtualKeyCode::Numpad9 => neith::event::KeyCode::Numpad9,
	VirtualKeyCode::AbntC1 => neith::event::KeyCode::AbntC1,
	VirtualKeyCode::AbntC2 => neith::event::KeyCode::AbntC2,
	VirtualKeyCode::Apostrophe => neith::event::KeyCode::Apostrophe,
	VirtualKeyCode::Apps => neith::event::KeyCode::Apps,
	VirtualKeyCode::At => neith::event::KeyCode::At,
	VirtualKeyCode::Ax => neith::event::KeyCode::Ax,
	VirtualKeyCode::Backslash => neith::event::KeyCode::Backslash,
	VirtualKeyCode::Calculator => neith::event::KeyCode::Calculator,
	VirtualKeyCode::Capital => neith::event::KeyCode::Capital,
	VirtualKeyCode::Colon => neith::event::KeyCode::Colon,
	VirtualKeyCode::Comma => neith::event::KeyCode::Comma,
	VirtualKeyCode::Convert => neith::event::KeyCode::Convert,
	VirtualKeyCode::Equals => neith::event::KeyCode::Equals,
	VirtualKeyCode::Grave => neith::event::KeyCode::Grave,
	VirtualKeyCode::Kana => neith::event::KeyCode::Kana,
	VirtualKeyCode::Kanji => neith::event::KeyCode::Kanji,
	VirtualKeyCode::LAlt => neith::event::KeyCode::LAlt,
	VirtualKeyCode::LBracket => neith::event::KeyCode::LBracket,
	VirtualKeyCode::LControl => neith::event::KeyCode::LControl,
	VirtualKeyCode::LShift => neith::event::KeyCode::LShift,
	VirtualKeyCode::LWin => neith::event::KeyCode::LWin,
	VirtualKeyCode::Mail => neith::event::KeyCode::Mail,
	VirtualKeyCode::MediaSelect => neith::event::KeyCode::MediaSelect,
	VirtualKeyCode::MediaStop => neith::event::KeyCode::MediaStop,
	VirtualKeyCode::Minus => neith::event::KeyCode::Minus,
	VirtualKeyCode::Mute => neith::event::KeyCode::Mute,
	VirtualKeyCode::MyComputer => neith::event::KeyCode::MyComputer,
	VirtualKeyCode::NavigateForward => neith::event::KeyCode::NavigateForward,
	VirtualKeyCode::NavigateBackward => neith::event::KeyCode::NavigateBackward,
	VirtualKeyCode::NextTrack => neith::event::KeyCode::NextTrack,
	VirtualKeyCode::NoConvert => neith::event::KeyCode::NoConvert,
	VirtualKeyCode::NumpadComma => neith::event::KeyCode::NumpadComma,
	VirtualKeyCode::NumpadEnter => neith::event::KeyCode::NumpadEnter,
	VirtualKeyCode::NumpadEquals => neith::event::KeyCode::NumpadEquals,
	VirtualKeyCode::OEM102 => neith::event::KeyCode::OEM102,
	VirtualKeyCode::Period => neith::event::KeyCode::Period,
	VirtualKeyCode::PlayPause => neith::event::KeyCode::PlayPause,
	VirtualKeyCode::Power => neith::event::KeyCode::Power,
	VirtualKeyCode::PrevTrack => neith::event::KeyCode::PrevTrack,
	VirtualKeyCode::RAlt => neith::event::KeyCode::RAlt,
	VirtualKeyCode::RBracket => neith::event::KeyCode::RBracket,
	VirtualKeyCode::RControl => neith::event::KeyCode::RControl,
	VirtualKeyCode::RShift => neith::event::KeyCode::RShift,
	VirtualKeyCode::RWin => neith::event::KeyCode::RWin,
	VirtualKeyCode::Semicolon => neith::event::KeyCode::Semicolon,
	VirtualKeyCode::Slash => neith::event::KeyCode::Slash,
	VirtualKeyCode::Sleep => neith::event::KeyCode::Sleep,
	VirtualKeyCode::Stop => neith::event::KeyCode::Stop,	VirtualKeyCode::Sysrq => neith::event::KeyCode::Sysrq,
	VirtualKeyCode::Tab => neith::event::KeyCode::Tab,
	VirtualKeyCode::Underline => neith::event::KeyCode::Underline,
	VirtualKeyCode::Unlabeled => neith::event::KeyCode::Unlabeled,
	VirtualKeyCode::VolumeDown => neith::event::KeyCode::VolumeDown,
	VirtualKeyCode::VolumeUp => neith::event::KeyCode::VolumeUp,
	VirtualKeyCode::Wake => neith::event::KeyCode::Wake,
	VirtualKeyCode::WebBack => neith::event::KeyCode::WebBack,
	VirtualKeyCode::WebFavorites => neith::event::KeyCode::WebFavorites,
	VirtualKeyCode::WebForward => neith::event::KeyCode::WebForward,
	VirtualKeyCode::WebHome => neith::event::KeyCode::WebHome,
	VirtualKeyCode::WebRefresh => neith::event::KeyCode::WebRefresh,
	VirtualKeyCode::WebSearch => neith::event::KeyCode::WebSearch,
	VirtualKeyCode::WebStop => neith::event::KeyCode::WebStop,
	VirtualKeyCode::Yen => neith::event::KeyCode::Yen,
	VirtualKeyCode::Copy => neith::event::KeyCode::Copy,
	VirtualKeyCode::Paste => neith::event::KeyCode::Paste,
	VirtualKeyCode::Cut => neith::event::KeyCode::Cut,

        VirtualKeyCode::NumpadAdd => neith::event::KeyCode::Add,
	VirtualKeyCode::NumpadDecimal => neith::event::KeyCode::Decimal,
	VirtualKeyCode::NumpadDivide => neith::event::KeyCode::Divide,
	VirtualKeyCode::NumpadSubtract => neith::event::KeyCode::Subtract,
	VirtualKeyCode::NumpadMultiply => neith::event::KeyCode::Multiply,

        VirtualKeyCode::Asterisk => neith::event::KeyCode::Unlabeled,
        VirtualKeyCode::Plus => neith::event::KeyCode::Unlabeled,
        
    }
}
